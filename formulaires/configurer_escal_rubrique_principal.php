<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_rubrique_principal_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configrubrique-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:page_rubrique').'</h3>'
				),

// les annonces
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_rub_annonce',
				'label' => '<:escal:annonces:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annoncerub',
						'label' => '<:escal:annonce_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annoncedefilrub',
						'label' => '<:escal:annonce_defil_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				)
			),// fin du fieldset
// la rubrique
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_rubrique_la',
				'label' => '<:escal:rubrique_la:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'fluxrssrub',
						'label' => '<:escal:rubrique_rss:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'fluxrsssousrub',
						'label' => '<:escal:rubrique_rss2:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogorub',
						'label' => '<:escal:rubrique_taille_logo:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'daterub',
						'label' => '<:escal:rubrique_date:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteurrub',
						'label' => '<:escal:affichage_auteur_articles:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nombreart',
						'label' => '<:escal:nombre_articles_pagination:>',
						'defaut' => '5',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'nbrecolrub',
						'label' => '<:escal:nombre_colonnes:>',
						'defaut' => '1',
						'data' => array(
							'1' => '<:escal:choix_une:>',
							'2' => '<:escal:choix_deux:>',
							'3' => '<:escal:choix_trois:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrecaracttexterub',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'paginhaut',
						'label' => '<:escal:rubrique_pagin_haut:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'paginbas',
						'label' => '<:escal:rubrique_pagin_bas:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset

				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}