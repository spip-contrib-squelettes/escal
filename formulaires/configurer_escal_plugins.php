<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_plugins_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configplugin-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:cfg_page_plugins').'</h3>'
				),
// article_pdf
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_articlepdf_plugin',
				'label' => '<:escal:articlepdf_plugin:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_articlepdf',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article187" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_articlepdf',
						'texte' => '<:escal:articlepdf_explication:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'rubpdf',
						'label' => '<:escal:articlepdf_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// galleria
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_galleria',
				'label' => '<:escal:galleria_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_galleria',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article177" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_galleria',
						'texte' => '<:escal:galleria_explication:>',
						)
					),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'galleriafond',
						'label' => '<:escal:galleria_couleur_gallerie:>',
						'explication' => 'par défaut : #000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'galleriaencarts',
						'label' => '<:escal:galleria_couleur_encart:>',
						'explication' => 'par défaut : #336699',
						'defaut' => '#336699',
						)
					),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'galleriacoulpolice',
						'label' => '<:escal:galleria_couleur_texte:>',
						'explication' => 'par défaut : #ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'galleriatailletitre',
						'label' => '<:escal:galleria_caracteres:>',
						'defaut' => '14',
						)
					),
/*	
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_metadonnees',
						'texte' => '<:escal:metadonnees_explication:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'metadonnees',
						'defaut' => 'metadonnees',
						'data' => array(
							'metadonnees' => '<:escal:metadonnees:>',
							'titre' => '<:escal:titre:>',
							'meta_et_titre' => '<:escal:metadonnees_titre:>',
							)
						)
					),
*/						
				)
			),// fin du fieldset
// licence
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_licence',
				'label' => '<:intitule_licence:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_licence',
						'texte' => '<:escal:licence_explication:>',
						)
					),
				)
			),// fin du fieldset
// liens sociaux
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_liens_sociaux',
				'label' => '<:escal:liens_sociaux_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_liens_sociaux',
						'texte' => '<:escal:liens_sociaux_explication:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'sociaux_espace',
						'label' => '<:escal:liens_sociaux_espace:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'sociaux_pied',
						'label' => '<:escal:liens_sociaux_pied:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'sociaux_texte',
						'label' => '<:escal:liens_sociaux_texte:>',
						)
					),
				)
			),// fin du fieldset
// mentions légales
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_mentions',
				'label' => '<:escal:mentions:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_mentions',
						'texte' => '<:escal:mentions_explication:>',
						)
					),
				)
			),// fin du fieldset
// Modèles facebook
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_facebook',
				'label' => '<:escal:facebook_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_facebook',
						'texte' => '<:escal:facebook_explication:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'boutonfacebook',
						'label' => '<:escal:facebook_bouton:>',
						'defaut' => 'button_count',
						'data' => array(
							'button_count' => '<img src="'.find_in_path('prive/themes/spip/images/buttoncount.png').'" alt="" />',
							'box_count' => '<img src="'.find_in_path('prive/themes/spip/images/boxcount.png').'" alt="" />',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'sendfacebook',
						'label' => '<:escal:facebook_partager:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// qr code
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_qrcode',
				'label' => '<:escal:qrcode_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_qrcode',
						'texte' => '<:escal:qrcode_explication:>',
						)
					),
				)
			),// fin du fieldset
// rainette
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_rainette',
				'label' => '<:escal:rainette:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_rainette',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article192&lang=fr" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_rainette',
						'texte' => '<:escal:rainette_explication:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'service',
						'label' => '<:escal:rainette_service:>',
						'defaut' => 'weatherbit',
						'data' => array(
							'accuweather' => 'AccuWeather',
							'meteoconcept' => 'Meteo Concept',
							'openmeteo' => 'Open-Meteo',
							'owm' => 'Open Weather Map',
							'weatherapi' => 'WeatherAPI',
							'weatherbit' => 'Weatherbit.io',
							'wwo' => 'World Weather Online',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'ville',
						'label' => '<:escal:rainette_code:>',
						)
					),
				)
			),// fin du fieldset
/*
// shoutbox
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_shoutbox',
				'label' => '<:escal:shoutbox_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_shoutbox',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article317&lang=fr" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_shoutbox',
						'texte' => '<:escal:shoutbox_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrmessageshoutbox',
						'label' => '<:escal:shoutbox_nombre:>',
						'defaut' => '50',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'hauteurshoutbox',
						'label' => '<:escal:shoutbox_hauteur:>',
						'defaut' => '200',
						)
					),
				)
			),// fin du fieldset
			*/
/*
// signalement
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_signalement',
				'label' => '<:escal:signalement_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_signalement',
						'texte' => '<:escal:signalement_explication:>',
						)
					),
				)
			),// fin du fieldset
// socialtags
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_socialtags',
				'label' => '<:escal:socialtags_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_socialtags',
						'texte' => '<:escal:socialtags_explication:>',
						)
					),
				)
			),// fin du fieldset
*/
// spip400
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_spip400',
				'label' => '<:escal:spip400_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_spip400',
						'texte' => '<:escal:spip400_explication:>',
						)
					),
				)
			),// fin du fieldset
// spipdf
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_spipdf',
				'label' => '<:escal:spipdf_plugin:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_spipdf',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article231" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_spipdf',
						'texte' => '<:escal:spipdf_explication:>',
						)
					),
				)
			),// fin du fieldset


				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),
		);
	return $saisies;
}