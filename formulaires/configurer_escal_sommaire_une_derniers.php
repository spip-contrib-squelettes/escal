<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_sommaire_une_derniers_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configune-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('public:derniers_articles').'</h3>'
				),

// article en exergue
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_exclus arts2',
								'texte' => '<:escal:onglets_derniers_arts_explication4:>',
								'alerte_role' => 'alert',
								)
							),
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_exergue',
				'label' => '<:escal:onglets_derniers_exergue:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoartexergue',
						'label' => '<:escal:article_logo:>',
						'defaut' => '100',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'surtitreune',
						'label' => '<:escal:affichage_surtitre:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'soustitreune',
						'label' => '<:escal:affichage_soustitre:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteurune',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'datepubune',
						'label' => '<:escal:affichage_date_pub:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'datemodifune',
						'label' => '<:escal:affichage_date_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'rubriqueune',
						'label' => '<:escal:affichage_rubrique:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifune',
						'label' => '<:escal:affichage_descriptif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'chapeauune',
						'label' => '<:escal:affichage_chapeau:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'texteune',
						'label' => '<:escal:affichage_debut:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailletexteune',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '300',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'imageune',
						'label' => '<:escal:affichage_image:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoimgexergue',
						'label' => '<:escal:articles_largeur_image:>',
						'defaut' => '150',
						)
					),
				)
			),// fin du fieldset
// autres articles
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_autres',
				'label' => '<:escal:onglets_derniers_autres:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nombrealaune',
						'label' => '<:escal:onglets_nombre_articles_exergue:>',
						'defaut' => '11',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'nbrecol',
						'label' => '<:escal:nombre_colonnes:>',
						'defaut' => '2',
						'data' => array(
							'1' => '<:escal:choix_une:>',
							'2' => '<:escal:choix_deux:>',
							'3' => '<:escal:choix_trois:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'ordrealaune',
						'label' => '<:escal:affichage_ordre:>',
						'defaut' => 'date',
						'data' => array(
							'date' => '<:escal:affichage_ordre_dateinv:>',
							'date_modif' => '<:escal:affichage_ordre_datemodif:>',
							'hasard' => '<:escal:affichage_ordre_hasard:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoartune',
						'label' => '<:escal:articles_logo:>',
						'defaut' => '40',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrecaracttitre',
						'label' => '<:escal:titre_coupe:>',
						'defaut' => '30',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'dateuneautres',
						'label' => '<:escal:affichage_date_pub_ou_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteuruneautres',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichrubrique',
						'label' => '<:escal:affichage_rubrique:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichdescriptif',
						'label' => '<:escal:affichage_descriptif:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichchapo',
						'label' => '<:escal:affichage_chapeau:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichtextederniersart',
						'label' => '<:escal:affichage_debut:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrecaracttexte',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichcomm',
						'label' => '<:escal:affichage_nombre_comments:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset


				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}
