<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_fonds_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configfonds-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:cfg_page_fonds').'</h3>'
				),

// infos
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_fond_infos',
				'label' => '<:escal:infos:>',
				'onglet' => 'oui',
						'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_couleurs',
						'texte' => '<:escal:fonds_couleurs:>',
						)
					),
				)
			),// fin du fieldset

// Le fond du site
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_fonds_site',
				'label' => '<:escal:fonds_site:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleurfond',
						'label' => '<:escal:fonds_site_couleur:>',
						'explication' => '<:escal:fonds_site_couleur_defaut:>',
						'defaut' => '#DFDFDF',
						)
					),
				array(
					'saisie' => 'escal_fond',
					'options' => array(
						'nom' => 'imagefond',
						'option_intro' => '<:escal:fonds_site_aucune_image:>',
						'conteneur_class' => 'pleine_largeur'
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_site_explication2',
						'texte' => '<:escal:fonds_site_explication2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'imagefond2',
						'conteneur_class' => 'pleine_largeur'
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_site_explication3',
						'texte' => '<:escal:fonds_site_explication3:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'imagefondunique',
						'label' => '<:escal:fonds_site_imagefondunique:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset Le fond du site
// Les fonds et les textes des blocs
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_fonds_noisettes',
				'label' => '<:escal:fonds_noisettes:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
		// bandeau
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_bandeau',
						'titre' => '<:escal:fonds_noisettes_bandeau:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur9',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#336699',
						'defaut' => '#336699',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte01',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_bandeau_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur9',$defaut='#336699').'; color:'
						.lire_config($chemin='escal/config/texte01',$defaut='#ffffff').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_bandeau_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// contenu des pages
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_contenu',
						'titre' => '<:escal:fonds_noisettes_contenu:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleurpage',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#ffffff<br>',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'textpage',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_contenu_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleurpage',$defaut='#ffffff').'; color:'
						.lire_config($chemin='escal/config/textpage',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_contenu_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// pied de page
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_pied',
						'titre' => '<:escal:fonds_noisettes_pied:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur11',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#BBCCDD',
						'defaut' => '#BBCCDD',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text11',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_pied_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur11',$defaut='#BBCCDD').'; color:'
						.lire_config($chemin='escal/config/text11',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_pied_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// menu horizontal, liens survolés du menu vertical déroulant à droite,
		// encart des entetes pour les rubriques et les articles, plan, forum
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_menuh',
						'titre' => '<:escal:fonds_noisettes_menuh:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur3',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#336699',
						'defaut' => '#336699',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text3',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_menuh_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur3',$defaut='#336699').'; color:'
						.lire_config($chemin='escal/config/text3',$defaut='#ffffff').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_menuh_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// annonce, surlignage recherche
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_annonce',
						'titre' => '<:escal:fonds_noisettes_annonce:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur10',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#EC7942',
						'defaut' => '#EC7942',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text10',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_annonce_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur10',$defaut='#EC7942').'; color:'
						.lire_config($chemin='escal/config/text10',$defaut='#ffffff').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_annonce_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// onglets à la une si actifs
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_onglets_actifs',
						'titre' => '<:escal:fonds_noisettes_onglets_actifs:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleurune',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#336699',
						'defaut' => '#336699',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'textune',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'type' => 'color',
						'nom' => 'lienune',
						'label' => '<:escal:fonds_noisettes_lien:>',
						'explication' => '<:escal:par_defaut:>#ecc442',
						'defaut' => '#ecc442',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'type' => 'color',
						'nom' => 'lienunesurvol',
						'label' => '<:escal:fonds_noisettes_lien_survol:>',
						'explication' => '<:escal:par_defaut:>#ff7f50',
						'defaut' => '#ff7f50',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_onglets_actifs_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleurune',$defaut='#336699').'; color:'
						.lire_config($chemin='escal/config/textune',$defaut='#ffffff').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_onglets_actifs_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// onglets à la une si inactifs
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_onglets_inactifs',
						'titre' => '<:escal:fonds_noisettes_onglets_inactifs:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleuruneinact',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#82ADE2',
						'defaut' => '#82ADE2',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'textuneinact',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_onglets_inactifs_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleuruneinact',$defaut='#82ADE2').'; color:'
						.lire_config($chemin='escal/config/textuneinact',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_onglets_inactifs_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// derniers articles en une
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_derniers_articles',
						'titre' => '<:escal:fonds_noisettes_derniers_articles:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleuruneart',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#DAE6F6',
						'defaut' => '#DAE6F6',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'textuneart',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_derniers_articles_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleuruneart',$defaut='#DAE6F6').'; color:'
						.lire_config($chemin='escal/config/textuneart',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_derniers_articles_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// titre et bords blocs latéraux, menu vertical 2, entete tableaux, événements, ..
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_titres',
						'titre' => '<:escal:fonds_noisettes_titres:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur5',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#82ADE2',
						'defaut' => '#82ADE2',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text5',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_titres_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur5',$defaut='#82ADE2').'; color:'
						.lire_config($chemin='escal/config/text5',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_titres_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// texte des blocs latéraux
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_textes',
						'titre' => '<:escal:fonds_noisettes_textes:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur8',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#DAE6F6',
						'defaut' => '#DAE6F6',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text8',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_textes_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur8',$defaut='#DAE6F6').'; color:'
						.lire_config($chemin='escal/config/text8',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_textes_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// articles, forum, agenda
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_articles',
						'titre' => '<:escal:fonds_noisettes_articles:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur2',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text2',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_articles_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur2',$defaut='#ffffff').'; color:'
						.lire_config($chemin='escal/config/text2',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_articles_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// identification, recherche, formulaire contact, tableaux lignes paires, portfolio, post-scriptum, forum, ...
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_boutons',
						'titre' => '<:escal:fonds_noisettes_boutons:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur6',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#EFEFEF',
						'defaut' => '#EFEFEF',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text6',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_boutons_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur6',$defaut='#EFEFEF').'; color:'
						.lire_config($chemin='escal/config/text6',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_boutons_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// rubrique active du menu horizontal, tableaux lignes impaires, prévisualisation des forum ...
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_rubriques',
						'titre' => '<:escal:fonds_noisettes_rubriques:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur7',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#BBCCDD',
						'defaut' => '#BBCCDD',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text7',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_rubriques_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur7',$defaut='#BBCCDD').'; color:'
						.lire_config($chemin='escal/config/text7',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_rubriques_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// survol des entrées du menu horizontal et des articles du bloc à la une
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_survol',
						'titre' => '<:escal:fonds_noisettes_survol:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur4',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#224466',
						'defaut' => '#224466',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text4',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_survol_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur4',$defaut='#224466').'; color:'
						.lire_config($chemin='escal/config/text4',$defaut='#ffffff').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_survol_hr',
						'conteneur_class' => 'apercu-hr',
						'texte' => '<hr>',
						)
					),
		// forum site
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_forum',
						'titre' => '<:escal:page_forum:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleur1',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#DFDFDF',
						'defaut' => '#DFDFDF',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'text1',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#000000',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_noisettes_forum_2',
						'conteneur_class' => 'droite apercu',
						'texte' => '<div class="apercu_fond" style="background-color:'
						.lire_config($chemin='escal/config/couleur1',$defaut='#DFDFDF').'; color:'
						.lire_config($chemin='escal/config/text1',$defaut='#000000').'"><:escal:fonds_noisettes_apercu:></div>',
						)
					),
				)
			),// fin du fieldset Les fonds et les textes des blocs
// autres textes
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_fonds_textes',
				'label' => '<:escal:fonds_textes:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_fonds_textes',
						'texte' => '<:escal:fonds_textes_explication:>',
						)
					),
		// liens
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte1',
						'type' => 'color',
						'label' => '<:escal:fonds_textes_liens:>',
						'explication' => ''._T('escal:par_defaut').'#000099<br>
						<span class="apercu_texte" style="background-color:white;color:'.lire_config($chemin='escal/config/texte1',$defaut='#000099').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:grey;color:'.lire_config($chemin='escal/config/texte1',$defaut='#000099').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:black;color:'.lire_config($chemin='escal/config/texte1',$defaut='#000099').'">'._T('escal:fonds_textes_texte').'</span>
						',
						'defaut' => '#000099',
						)
					),
		// liens visités
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte2',
						'type' => 'color',
						'label' => '<:escal:fonds_textes_liens_vus:>',
						'explication' => ''._T('escal:par_defaut').'#000099<br>
						<span class="apercu_texte" style="background-color:white;color:'.lire_config($chemin='escal/config/texte2',$defaut='#000099').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:grey;color:'.lire_config($chemin='escal/config/texte2',$defaut='#000099').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:black;color:'.lire_config($chemin='escal/config/texte2',$defaut='#000099').'">'._T('escal:fonds_textes_texte').'</span>
						',
						'defaut' => '#000099',
						)
					),
		// liens survolés
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte3',
						'type' => 'color',
						'label' => '<:escal:fonds_textes_liens_survol:>',
						'explication' => ''._T('escal:par_defaut').'#FF8000<br>
						<span class="apercu_texte" style="background-color:white;color:'.lire_config($chemin='escal/config/texte3',$defaut='#FF8000').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:grey;color:'.lire_config($chemin='escal/config/texte3',$defaut='#FF8000').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:black;color:'.lire_config($chemin='escal/config/texte3',$defaut='#FF8000').'">'._T('escal:fonds_textes_texte').'</span>
						',
						'defaut' => '#FF8000',
						)
					),
		// zones de saisie de SPIP
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte6',
						'type' => 'color',
						'label' => '<:escal:fonds_textes_couleur:>',
						'explication' => ''._T('escal:par_defaut').'#336699<br>
						<span class="apercu_texte" style="background-color:white;color:'.lire_config($chemin='escal/config/texte6',$defaut='#336699').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:grey;color:'.lire_config($chemin='escal/config/texte6',$defaut='#336699').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:black;color:'.lire_config($chemin='escal/config/texte6',$defaut='#336699').'">'._T('escal:fonds_textes_texte').'</span>
						',
						'defaut' => '#336699',
						)
					),
		// forum du site
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte4',
						'type' => 'color',
						'label' => '<:escal:page_forum:>',
						'explication' => ''._T('escal:par_defaut').'#336699<br>
						<span class="apercu_texte" style="background-color:white;color:'.lire_config($chemin='escal/config/texte4',$defaut='#336699').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:grey;color:'.lire_config($chemin='escal/config/texte4',$defaut='#336699').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:black;color:'.lire_config($chemin='escal/config/texte4',$defaut='#336699').'">'._T('escal:fonds_textes_texte').'</span>
						',
						'defaut' => '#336699',
						)
					),
		// alertes de la page contact
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'texte5',
						'type' => 'color',
						'label' => '<:escal:fonds_textes_alerte:>',
						'explication' => ''._T('escal:par_defaut').'#FF0000<br>
						<span class="apercu_texte" style="background-color:white;color:'.lire_config($chemin='escal/config/texte5',$defaut='#FF0000').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:grey;color:'.lire_config($chemin='escal/config/texte5',$defaut='#FF0000').'">'._T('escal:fonds_textes_texte').'</span>
						<span class="apercu_texte" style="background-color:black;color:'.lire_config($chemin='escal/config/texte5',$defaut='#FF0000').'">'._T('escal:fonds_textes_texte').'</span>
						',
						'defaut' => '#FF0000',
						)
					),



				)
			),// fin du fieldset
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),
		);
	return $saisies;
}