<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_sommaire_blocs_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configcorps-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:bloc_choix').'</h3>'
				),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_choix_blocs',
						'texte' => '<:escal:doc_choix_blocs:>',
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'blocune1',
						'label' => '<:escal:bloc_un:>',
						'defaut' => 'a_la_une',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:aucun:>',
							'annonce' => '<:escal:annonce:>',
							'annonce_defilant' => '<:escal:annonce_defil:>',
							'a_la_une' => '<:escal:une:>',
							)
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'blocune2',
						'label' => '<:escal:bloc_deux:>',
						'defaut' => 'rien',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:aucun:>',
							'annonce' => '<:escal:annonce:>',
							'annonce_defilant' => '<:escal:annonce_defil:>',
							'a_la_une' => '<:escal:une:>',
							)
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'blocune3',
						'label' => '<:escal:bloc_trois:>',
						'defaut' => 'rien',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:aucun:>',
							'annonce' => '<:escal:annonce:>',
							'annonce_defilant' => '<:escal:annonce_defil:>',
							'a_la_une' => '<:escal:une:>',
							)
						)
					),



				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}