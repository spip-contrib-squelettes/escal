<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_sommaire_une_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configune-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:une_bloc').'</h3>'
				),
// choix des onglets
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_choix',
				'label' => '<:escal:onglets_choix:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'onglet1',
						'label' => '<:escal:onglets_un:>',
						'defaut' => 'derniersarticles',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'derniersarticles' => '<:public:derniers_articles:>',
							'derniersarticlesbis' => '<:escal:derniers_articles_bis:>',
							'derniersarticlester' => '<:escal:derniers_articles_ter:>',
							'articlesmotcle' => '<:escal:onglets_arts_mot:>',
							'articlesmotcle2' => '<:escal:onglets_arts_mot:> 2',
							'articlesmotcle3' => '<:escal:onglets_arts_mot:> 3',
							'plansite' => '<:public:plan_site:>',
							'articleaccueil' => '<:escal:onglets_art_accueil:>',
							'articlearchive' => '<:escal:onglets_art_archive:>',
							'rubrique' => '<:public:rubrique:>',
							'rubrique2' => '<:escal:onglets_rub2:>',
							'rubrique3' => '<:escal:onglets_rub3:>',
							'rubrique4' => '<:escal:onglets_rub4:>',
							'rubrique5' => '<:escal:onglets_rub5:>',
							'mon_article' => '<:escal:onglets_monart:>',
							'mon_article2' => '<:escal:onglets_monart2:>',
							'mon_article3' => '<:escal:onglets_monart3:>',
							'mon_article4' => '<:escal:onglets_monart4:>',
							'mon_article5' => '<:escal:onglets_monart5:>',
							'sites_accueil' => '<:public:sur_web:>',
							)
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'onglet2',
						'label' => '<:escal:onglets_deux:>',
						'defaut' => 'plansite',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:onglets_rien:>',
							'derniersarticles' => '<:public:derniers_articles:>',
							'derniersarticlesbis' => '<:escal:derniers_articles_bis:>',
							'derniersarticlester' => '<:escal:derniers_articles_ter:>',
							'articlesmotcle' => '<:escal:onglets_arts_mot:>',
							'articlesmotcle2' => '<:escal:onglets_arts_mot:> 2',
							'articlesmotcle3' => '<:escal:onglets_arts_mot:> 3',
							'plansite' => '<:public:plan_site:>',
							'articleaccueil' => '<:escal:onglets_art_accueil:>',
							'articlearchive' => '<:escal:onglets_art_archive:>',
							'rubrique' => '<:public:rubrique:>',
							'rubrique2' => '<:escal:onglets_rub2:>',
							'rubrique3' => '<:escal:onglets_rub3:>',
							'rubrique4' => '<:escal:onglets_rub4:>',
							'rubrique5' => '<:escal:onglets_rub5:>',
							'mon_article' => '<:escal:onglets_monart:>',
							'mon_article2' => '<:escal:onglets_monart2:>',
							'mon_article3' => '<:escal:onglets_monart3:>',
							'mon_article4' => '<:escal:onglets_monart4:>',
							'mon_article5' => '<:escal:onglets_monart5:>',
							'sites_accueil' => '<:public:sur_web:>',
							)
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'onglet3',
						'label' => '<:escal:onglets_trois:>',
						'defaut' => 'rien',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:onglets_rien:>',
							'derniersarticles' => '<:public:derniers_articles:>',
							'derniersarticlesbis' => '<:escal:derniers_articles_bis:>',
							'derniersarticlester' => '<:escal:derniers_articles_ter:>',
							'articlesmotcle' => '<:escal:onglets_arts_mot:>',
							'articlesmotcle2' => '<:escal:onglets_arts_mot:> 2',
							'articlesmotcle3' => '<:escal:onglets_arts_mot:> 3',
							'plansite' => '<:public:plan_site:>',
							'articleaccueil' => '<:escal:onglets_art_accueil:>',
							'articlearchive' => '<:escal:onglets_art_archive:>',
							'rubrique' => '<:public:rubrique:>',
							'rubrique2' => '<:escal:onglets_rub2:>',
							'rubrique3' => '<:escal:onglets_rub3:>',
							'rubrique4' => '<:escal:onglets_rub4:>',
							'rubrique5' => '<:escal:onglets_rub5:>',
							'mon_article' => '<:escal:onglets_monart:>',
							'mon_article2' => '<:escal:onglets_monart2:>',
							'mon_article3' => '<:escal:onglets_monart3:>',
							'mon_article4' => '<:escal:onglets_monart4:>',
							'mon_article5' => '<:escal:onglets_monart5:>',
							'sites_accueil' => '<:public:sur_web:>',
							)
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'onglet4',
						'label' => '<:escal:onglets_quatre:>',
						'defaut' => 'rien',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:onglets_rien:>',
							'derniersarticles' => '<:public:derniers_articles:>',
							'derniersarticlesbis' => '<:escal:derniers_articles_bis:>',
							'derniersarticlester' => '<:escal:derniers_articles_ter:>',
							'articlesmotcle' => '<:escal:onglets_arts_mot:>',
							'articlesmotcle2' => '<:escal:onglets_arts_mot:> 2',
							'articlesmotcle3' => '<:escal:onglets_arts_mot:> 3',
							'plansite' => '<:public:plan_site:>',
							'articleaccueil' => '<:escal:onglets_art_accueil:>',
							'articlearchive' => '<:escal:onglets_art_archive:>',
							'rubrique' => '<:public:rubrique:>',
							'rubrique2' => '<:escal:onglets_rub2:>',
							'rubrique3' => '<:escal:onglets_rub3:>',
							'rubrique4' => '<:escal:onglets_rub4:>',
							'rubrique5' => '<:escal:onglets_rub5:>',
							'mon_article' => '<:escal:onglets_monart:>',
							'mon_article2' => '<:escal:onglets_monart2:>',
							'mon_article3' => '<:escal:onglets_monart3:>',
							'mon_article4' => '<:escal:onglets_monart4:>',
							'mon_article5' => '<:escal:onglets_monart5:>',
							'sites_accueil' => '<:public:sur_web:>',
							)
						)
					),
				array(
					'saisie' => 'selection',
					'options' => array(
						'nom' => 'onglet5',
						'label' => '<:escal:onglets_cinq:>',
						'defaut' => 'rien',
						'cacher_option_intro' => 'oui',
						'data' => array(
							'rien' => '<:escal:onglets_rien:>',
							'derniersarticles' => '<:public:derniers_articles:>',
							'derniersarticlesbis' => '<:escal:derniers_articles_bis:>',
							'derniersarticlester' => '<:escal:derniers_articles_ter:>',
							'articlesmotcle' => '<:escal:onglets_arts_mot:>',
							'articlesmotcle2' => '<:escal:onglets_arts_mot:> 2',
							'articlesmotcle3' => '<:escal:onglets_arts_mot:> 3',
							'plansite' => '<:public:plan_site:>',
							'articleaccueil' => '<:escal:onglets_art_accueil:>',
							'articlearchive' => '<:escal:onglets_art_archive:>',
							'rubrique' => '<:public:rubrique:>',
							'rubrique2' => '<:escal:onglets_rub2:>',
							'rubrique3' => '<:escal:onglets_rub3:>',
							'rubrique4' => '<:escal:onglets_rub4:>',
							'rubrique5' => '<:escal:onglets_rub5:>',
							'mon_article' => '<:escal:onglets_monart:>',
							'mon_article2' => '<:escal:onglets_monart2:>',
							'mon_article3' => '<:escal:onglets_monart3:>',
							'mon_article4' => '<:escal:onglets_monart4:>',
							'mon_article5' => '<:escal:onglets_monart5:>',
							'sites_accueil' => '<:public:sur_web:>',
							)
						)
					),
				)
			),// fin du fieldset choix des onglets


// paramétrage des onglets
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_parametrage',
				'label' => '<:escal:onglets_parametrage:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'ancreonglet',
						'label' => '<:escal:onglets_placer:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
		// derniers articles
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglet_derniers_articles',
						'label' => '<:public:derniers_articles:>',
						'onglet' => 'oui',
						'onglet_vertical' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_derniers_arts',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article17" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_derniers arts',
								'texte' => '<:escal:onglets_derniers_arts_explication1:>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_exclus arts',
								'texte' => '<:escal:onglets_derniers_arts_explication3:>',
								'alerte_role' => 'alert',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreongletderniers',
								'label' => '<:escal:onglets_derniers_arts_explication2:>',
								'conteneur_class' => 'pleine_largeur',
								)
							),
						)
					),// fin du fieldset
		// Articles avec mot-clé
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglets_arts_mot',
						'label' => '<:escal:onglets_arts_mot:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_onglets_arts_mot',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article281" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_onglets_arts_mot',
								'titre' => '<:escal:onglets_arts_mot_explication2:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreongletmotcle',
								'label' => '<:escal:onglets_arts_mot_choix:> <:info_pour:> "<:escal:onglets_arts_mot:>"',
								'explication' => '<:escal:onglets_arts_mot_explication1:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreongletmotcle2',
								'label' => '<:escal:onglets_arts_mot_choix:> <:info_pour:> "<:escal:onglets_arts_mot:> 2"',
								'explication' => '<:escal:onglets_arts_mot_explication1:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreongletmotcle3',
								'label' => '<:escal:onglets_arts_mot_choix:> <:info_pour:> "<:escal:onglets_arts_mot:> 3"',
								'explication' => '<:escal:onglets_arts_mot_explication1:>',
								)
							),
						)
					),// fin du fieldset
		// article d'accueil
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglet_art_accueil',
						'label' => '<:escal:onglets_art_accueil:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_onglets_art_accueil',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article182" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_onglets_art_accueil',
								'texte' => '<:escal:onglets_art_accueil_explication:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'accueilforum',
								'label' => '<:escal:onglets_art_accueil_forum:>',
								'defaut' => 'non',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset
		// article archive
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglet_art_archive',
						'label' => '<:escal:onglets_art_archive:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_art_archive',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article185" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_art_archive',
								'texte' => '<:escal:onglets_art_archive_explication:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'archiveforum',
								'label' => '<:escal:onglets_art_archive_forum:>',
								'defaut' => 'non',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset
		// articles de rubrique
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglet_art_rub',
						'label' => '<:public:rubrique:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_onglets_rub',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article186" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_onglets_rub',
								'texte' => '<:escal:onglets_rub_explication:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'descriptifrubaccueil',
								'label' => '<:escal:onglets_rub_descriptif:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'texterubaccueil',
								'label' => '<:escal:onglets_rub_texte:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'taillelogorubaccueil',
								'label' => '<:escal:onglets_rub_logo:>',
								'defaut' => '25',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'auteurrubaccueil',
								'label' => '<:escal:onglets_rub_auteur:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'pagrubaccueil',
								'label' => '<:escal:onglets_rub_pagin:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset
		// Mon article
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglet_monart',
						'label' => '<:escal:onglets_monart:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_onglets_monart',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article188" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_onglets_mon_art',
								'texte' => '<:escal:onglets_monart_explication:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titremonart',
								'label' => '<:escal:onglets_monart_titre:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titremonart2',
								'label' => '<:escal:onglets_monart2_titre:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titremonart3',
								'label' => '<:escal:onglets_monart3_titre:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titremonart4',
								'label' => '<:escal:onglets_monart4_titre:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titremonart5',
								'label' => '<:escal:onglets_monart5_titre:>',
								)
							),
						)
					),// fin du fieldset
		// sur le web
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_onglet_sur_web',
						'label' => '<:public:sur_web:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_onglets_sur_web',
								'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article311" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_onglets_mon_art',
								'texte' => '<:escal:onglets_sur_web_explication:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titresitesaccueil',
								'explication' => '<:escal:onglets_sur_web_explication2:>',
								'conteneur_class' => 'pleine_largeur',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartsyndic',
								'label' => '<:escal:onglets_nombre_articles:>',
								'defaut' => '20',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'paginsitesaccueil',
								'label' => '<:escal:onglets_sur_web_pagin:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset
				)
			),// fin du fieldset paramétrage des onglets





				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}