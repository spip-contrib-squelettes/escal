<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_menuh_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configmenu-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:menu_horizontal').'</h3>'
				),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'docmenuh',
				'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article29" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
				)
		),
// un fieldset
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetmenuh',
				'label' => '<:affichage:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'menuH',
						'label' => '<:escal:menu_horizontal_affichage:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'choixMenuH',
						'label' => '<:escal:menu_horizontal_choix:>',
						'defaut' => 'menuH1',
						'data' => array(
							'menuH1' => '<:escal:menu_horizontal_choix_liste:>',
							'menuH2' => '<:escal:menu_horizontal_choix_bloc:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'articlesmenuH',
						'label' => '<:escal:affiche_articles:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'menufixe',
						'label' => '<:escal:menu_horizontal_fixer:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// lien vers accueil
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetlienaccueil',
				'label' => '<:escal:menu_horizontal_accueil:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'alertesecteur',
						'texte' => '<:escal:menu_horizontal_notice:>',
						'conteneur_class' => 'notice',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'texteaccueil',
						'label' => '<:escal:menu_horizontal_titre_accueil:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'logoaccueil',
						'label' => '<:escal:menu_horizontal_logo_accueil:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// secteurs
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetsecteurs',
				'label' => '<:escal:menu_horizontal_secteur:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicsecteur',
						'texte' => '<:escal:menu_horizontal_secteur_explication:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'alertesecteur',
						'texte' => '<:escal:menu_horizontal_notice:>',
						'conteneur_class' => 'notice',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'texterubmenu',
						'label' => '<:escal:menu_horizontal_titre:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'logorubmenu',
						'label' => '<:escal:menu_horizontal_logo:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogorubmenu',
						'label' => '<:escal:menu_horizontal_hauteur:>',
						'explication' => '<:escal:menu_horizontal_hauteur_explication:>',
						'defaut' => '20',
						)
					),
				)
			),// fin du fieldset
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}