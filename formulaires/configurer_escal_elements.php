<?php


if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_elements_saisies_dist(){

	$saisies = array(
				'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configelements-xx.svg').'" alt=""/>
				<h3 class="titrem">'._T('escal:elements').'</h3>'
				),
// accessibilité
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetaccessible',
				'label' => '<:escal:accessibilite:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'accessibilite',
						'label' => '<:escal:accessibilite_explication:>',
						'conteneur_class' => 'pleine_largeur',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'policesite',
						'label' => '<:escal:accessibilite_police:>',
						'conteneur_class' => 'pleine_largeur',
						'defaut' => 'verdana',
						'data' => array(
							'verdana' => '<:escal:police_verdana:>',
							'luciole' => '<:escal:police_luciole:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'policeexplication',
						'texte' => '<:escal:police_readme:>',
						'conteneur_class' => 'pleine_largeur',
						'afficher_si' => '@policesite@=="luciole"',
						)
					)
				)
			),// fin du fieldset
// mots-clés
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetmotscles',
				'label' => '<:escal:mots_clefs:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicmotscles',
						'titre' => '<:escal:affichage_mots_cles:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'motsclesune',
						'label' => '<:escal:affichage_mots_cles_une:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'motsclesrubrique',
						'label' => '<:escal:affichage_mots_cles_rubrique:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'motsclesarticles',
						'label' => '<:escal:affichage_mots_cles_article:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicmotscles2',
						'titre' => '<:escal:noisettes_multi_choix:>',
						)
					),
				array(
					'saisie' => 'escal_titre_groupe_mots',
					'options' => array(
						'nom' => 'motscles1',
						'label' => '<:escal:noisettes_multi_groupe1:>',
						'option_intro' => '<:escal:noisettes_multi_non:>',
						'table_liaison' => 'articles',
						'groupes_exclus' => array('affichage','trombino','type_rubrique')
						)
					),
				array(
					'saisie' => 'escal_titre_groupe_mots',
					'options' => array(
						'nom' => 'motscles2',
						'label' => '<:escal:noisettes_multi_groupe2:>',
						'option_intro' => '<:escal:noisettes_multi_non:>',
						'table_liaison' => 'articles',
						'groupes_exclus' => array('affichage','trombino','type_rubrique')
						)
					),
				array(
					'saisie' => 'escal_titre_groupe_mots',
					'options' => array(
						'nom' => 'motscles3',
						'label' => '<:escal:noisettes_multi_groupe3:>',
						'option_intro' => '<:escal:noisettes_multi_non:>',
						'table_liaison' => 'articles',
						'groupes_exclus' => array('affichage','trombino','type_rubrique')
						)
					),
				array(
					'saisie' => 'escal_titre_groupe_mots',
					'options' => array(
						'nom' => 'motscles4',
						'label' => '<:escal:noisettes_multi_groupe4:>',
						'option_intro' => '<:escal:noisettes_multi_non:>',
						'table_liaison' => 'articles',
						'groupes_exclus' => array('affichage','trombino','type_rubrique')
						)
					),
				array(
					'saisie' => 'escal_titre_groupe_mots',
					'options' => array(
						'nom' => 'motscles5',
						'label' => '<:escal:noisettes_multi_groupe5:>',
						'option_intro' => '<:escal:noisettes_multi_non:>',
						'table_liaison' => 'articles',
						'groupes_exclus' => array('affichage','trombino','type_rubrique')
						)
					),
				)
			),// fin du fieldset
// favicon
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetfavicon',
				'label' => '<:escal:favicon:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'favicon',
						'defaut' => 'favicon',
						'conteneur_class' => 'pleine_largeur',
						'data' => array(
							'favicon' => '<:escal:favicon_choix1:>',
							'logosite' => '<:escal:favicon_choix2:>',
							'faviconlibre' => '<:escal:favicon_choix3:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrefavicon',
						'explication' => '<:escal:favicon_choix_fichier:>',
						'afficher_si' => '@favicon@=="faviconlibre"',
						)
					)
				)
			),// fin du fieldset
// pointeurs
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetpointeurs',
				'label' => '<:escal:pointeur:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'pointeur',
						'texte' => '<:escal:pointeur_explication:>',
						)
					),
				array(
					'saisie' => 'escal_pointeur',
					'options' => array(
						'nom' => 'pointeurbody',
						'label' => '<:escal:pointeur_general:>',
						'option_intro' => '<:escal:pointeur_defaut:>',
						)
					),
				array(
					'saisie' => 'escal_pointeur',
					'options' => array(
						'nom' => 'pointeura',
						'label' => '<:escal:pointeur_liens:>',
						'option_intro' => '<:escal:pointeur_defaut:>',
						)
					),
				)
			),// fin du fieldset
// bord des pages
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetbordpages',
				'label' => '<:escal:bord_pages:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'ombres',
						'label' => '<:escal:bord_pages_label:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'couleur',
					'options' => array(
						'nom' => 'couleurombre',
						'label' => '<:escal:bord_pages_couleur:>',
						'defaut' => '#000000',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'decalombre',
						'label' => '<:escal:bord_pages_decalage:>',
						'defaut' => '5',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'degradeombre',
						'label' => '<:escal:bord_pages_force:>',
						'defaut' => '10',
						'explication' => '<:escal:bord_pages_nb:>',
						)
					)
				)
			),// fin du fieldset
// barre d'outils
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetbarreoutils',
				'label' => '<:label_bando_outils:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'espace',
						'label' => '<:escal:barre_outils_place:>',
						'defaut' => 'bas',
						'conteneur_class' => 'pleine_largeur',
						'data' => array(
							'haut' => '<:escal:barre_outils_dessus:>',
							'bas' => '<:escal:barre_outils_dessous:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'fixespace',
						'label' => '<:escal:barre_outils_fixer:>',
						'defaut' => 'non',
						'conteneur_class' => 'pleine_largeur',
						'afficher_si' => '@espace@=="haut"',
						'afficher_si_avec_post' => "oui",
						'explication' => '<:escal:barre_outils_explication:>',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'idlight',
						'label' => '<:escal:barre_outils_identification:>',
						'defaut' => 'oui',
						'conteneur_class' => 'pleine_largeur',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					)
				)
			),// fin du fieldset
// formulaire de recherche
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetrecherche',
				'label' => '<:escal:form_recherche:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'formrecherche',
						'label' => '<:escal:form_recherche_place:>',
						'defaut' => 'espace',
						'data' => array(
							'espace' => '<:escal:form_recherche_item1:>',
							'menuH' => '<:escal:form_recherche_item5:>',
							'colgauche' => '<:escal:form_recherche_item2:>',
							'coldroite' => '<:escal:form_recherche_item3:>',
							'non' => '<:escal:form_recherche_item4:>',
							)
						)
					)
				)
			),// fin du fieldset
// un hidden pour stocker les données
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}