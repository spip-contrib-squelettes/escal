<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_pied_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
					<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configpied-xx.svg').'" alt="" />
					<h3 class="titrem">'._T('escal:pied').'</h3>'
				),

		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'docpiedliens',
				'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article30" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
				)
			),
// liens
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetpiedliens',
				'label' => '<:escal:fonds_textes_liens:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'lienplan',
						'label' => '<:escal:pied_lien_plan:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'liencontact',
						'label' => '<:escal:pied_lien_contact:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'lienmentionschoix',
						'label' => '<:escal:pied_lien_mentions:>',
						'defaut' => 'article_mot',
						'data' => array(
							'article' => '<:escal:pied_lien_mentions_choix3:>',
							'article_mot' => '<:escal:pied_lien_mentions_choix4:>',
							'legalen' => '<:escal:pied_lien_mentions_choix2:>',
							'plugin' => '<:escal:pied_lien_mentions_choix1:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'lienmentionsarticle',
						'label' => '<:escal:pied_lien_mentions_article:>',
						'afficher_si' => '@lienmentionschoix@=="article"',
						'afficher_si_avec_post' => 'oui',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'lienecrire',
						'label' => '<:escal:pied_lien_prive:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'liensquelette',
						'label' => '<:escal:pied_lien_squelette:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'texteicone',
						'label' => '<:escal:pied_lien_affichage:>',
						'defaut' => 'texte',
						'data' => array(
							'texte' => '<:escal:pied_lien_texte:>',
							'icone' => '<:escal:pied_lien_icone:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailleiconespied',
						'label' => '<:escal:taille_icones_pied:>',
						'defaut' => '32',
						'afficher_si' => '@texteicone@=="icone"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'lienRSS',
						'label' => '<:escal:pied_lien_rss:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// autres mentions
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_pied_autres_mentions',
				'label' => '<:escal:pied_autres_mentions:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'copyright',
						'label' => '<:escal:pied_copyright:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'hebergeur',
						'label' => '<:escal:pied_hebergeur:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nomhebergeur',
						'label' => '<:escal:pied_hebergeur_nom:>',
						'afficher_si' => '@hebergeur@=="oui"',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'urlhebergeur',
						'label' => '<:escal:pied_hebergeur_url:>',
						'afficher_si' => '@hebergeur@=="oui"',
						)
					),
				)
			),// fin du fieldset
// citations
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetcitations',
				'label' => '<:escal:citations:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'expliccitations',
						'texte' => '<:escal:pied_citations_explication:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'citations',
						'label' => '<:escal:pied_citations_activer:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrecitations',
						'label' => '<:escal:pied_citations_titre:>',
						'explication' => '<:escal:pied_citations_titre_explication:>',
						)
					),
				)
			),// fin du fieldset
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}