<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_sommaire_une_derniers_ter_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configune-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:derniers_articles_ter').'</h3>'
				),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_exclus arts2',
								'texte' => '<:escal:onglets_derniers_arts_explication4:>',
								'alerte_role' => 'alert',
								)
							),
// presentation
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_ter_presentation',
				'label' => '<:escal:onglets_derniers_presentation:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'articleexergueter',
						'label' => '<:escal:onglets_ter_mot_exergue:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'nbrecolter',
						'label' => '<:escal:nombre_colonnes:>',
						'defaut' => '2',
						'data' => array(
							'1' => '<:escal:choix_une:>',
							'2' => '<:escal:choix_deux:>',
							'3' => '<:escal:choix_trois:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'ordrealauneter',
						'label' => '<:escal:affichage_ordre:>',
						'defaut' => 'date',
						'data' => array(
							'date' => '<:escal:affichage_ordre_dateinv:>',
							'date_modif' => '<:escal:affichage_ordre_datemodif:>',
							'hasard' => '<:escal:affichage_ordre_hasard:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nombrearticlesuneter',
						'label' => '<:escal:onglets_nombre_articles_exergue:>',
						'defaut' => '20',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'modelepaginarticlesuneter',
						'label' => '<:escal:onglets_bis_pagination:>',
						'defaut' => '',
						'data' => array(
							'' => '<:escal:onglets_bis_pagin_defaut:>',
							'prive' => '<:escal:onglets_bis_pagin_prive:>',
							'page' => '<:escal:onglets_bis_pagin_page:>',
							'page_precedent_suivant' => '<:escal:onglets_bis_pagin_pps:>',
							'precedent_suivant' => '<:escal:onglets_bis_pagin_ps:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'paginarticlesuneter',
						'label' => '<:escal:nombre_articles_pagination:>',
						'defaut' => '5',
						)
					),
				)
			),// fin du fieldset
// article en exergue
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_ter_exergue',
				'label' => '<:escal:onglets_derniers_exergue:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoartexergueter',
						'label' => '<:escal:article_logo:>',
						'defaut' => '100',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'surtitreuneter',
						'label' => '<:escal:affichage_surtitre:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'soustitreuneter',
						'label' => '<:escal:affichage_soustitre:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteuruneter',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'datepubuneter',
						'label' => '<:escal:affichage_date_pub:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'datemodifuneter',
						'label' => '<:escal:affichage_date_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'rubriqueuneter',
						'label' => '<:escal:affichage_rubrique:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifuneter',
						'label' => '<:escal:affichage_descriptif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'chapeauuneter',
						'label' => '<:escal:affichage_chapeau:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'texteuneter',
						'label' => '<:escal:affichage_debut:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailletexteuneter',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '300',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'imageuneter',
						'label' => '<:escal:affichage_image:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoimgexergueter',
						'label' => '<:escal:articles_largeur_image:>',
						'defaut' => '150',
						)
					),
				)
			),// fin du fieldset
// autres articles
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_ter_autres',
				'label' => '<:escal:onglets_derniers_autres:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoartuneter',
						'label' => '<:escal:articles_logo:>',
						'defaut' => '40',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrecaracttitreter',
						'label' => '<:escal:titre_coupe:>',
						'defaut' => '30',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'dateuneautrester',
						'label' => '<:escal:affichage_date_pub_ou_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteuruneautrester',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichrubriqueter',
						'label' => '<:escal:affichage_rubrique:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichdescriptifter',
						'label' => '<:escal:affichage_descriptif:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichtextederniersartter',
						'label' => '<:escal:affichage_debut:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrecaracttexteter',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichcommter',
						'label' => '<:escal:affichage_nombre_comments:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichvideo',
						'label' => '<:escal:affichage_video:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset



				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}
