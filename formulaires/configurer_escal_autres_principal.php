<?php


if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_autres_principal_saisies_dist(){

	$saisies = array(
				'options' => array(
				'inserer_debut' => '
				<h3 class="titrem">'._T('escal:page_autres').'</h3>'
				),
// la page rubrique
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_page_rubrique',
				'label' => '<:escal:page_rubrique:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
// les annonces
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_rub_annonce',
				'label' => '<:escal:annonces:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annoncerub',
						'label' => '<:escal:annonce_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annoncedefilrub',
						'label' => '<:escal:annonce_defil_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				)
			),// fin du fieldset les annonces
// la rubrique
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_rubrique_la',
				'label' => '<:escal:rubrique_la:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'fluxrssrub',
						'label' => '<:escal:rubrique_rss:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'fluxrsssousrub',
						'label' => '<:escal:rubrique_rss2:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogorub',
						'label' => '<:escal:rubrique_taille_logo:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'daterub',
						'label' => '<:escal:rubrique_date:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteurrub',
						'label' => '<:escal:affichage_auteur_articles:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nombreart',
						'label' => '<:escal:nombre_articles_pagination:>',
						'defaut' => '5',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'nbrecolrub',
						'label' => '<:escal:nombre_colonnes2:>',
						'defaut' => '1',
						'data' => array(
							'1' => '<:escal:choix_une:>',
							'2' => '<:escal:choix_deux:>',
							'3' => '<:escal:choix_trois:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbrecaracttexterub',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'paginhaut',
						'label' => '<:escal:rubrique_pagin_haut:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'paginbas',
						'label' => '<:escal:rubrique_pagin_bas:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset la rubrique
				)
			),// fin du fieldset la page rubrique
// la page article
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_page_article',
				'label' => '<:escal:page_article:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
// les annonces
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_art_annonce',
				'label' => '<:escal:annonces:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annonceart',
						'label' => '<:escal:annonce_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annoncedefilart',
						'label' => '<:escal:annonce_defil_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				)
			),// fin du fieldset les annonces
// les articles
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_articles_les',
				'label' => '<:titre_les_articles:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoart',
						'label' => '<:escal:articles_logo:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurimage',
						'label' => '<:escal:articles_largeur_images:>',
						'explication' => '<:escal:articles_largeur_images_classique:>',
						'defaut' => '400',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurimagepleinepage',
						'label' => '<:escal:articles_largeur_images:>',
						'explication' => '<:escal:articles_largeur_images_pleinepage:>',
						'defaut' => '850',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'dateart',
						'label' => '<:escal:affichage_date_pub_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteurart',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'visites',
						'label' => '<:escal:affichage_visites_popularite:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'visites-inter',
						'label' => '<:escal:affichage_visites_inter:>',
						'defaut' => ' - ',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailleoutilsarticle',
						'label' => '<:escal:taille_outils_article:>',
						'defaut' => '32',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'indent',
						'label' => '<:escal:indentation:>',
						'defaut' => '0',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_articles_reponses_forum',
						'titre' => '<:escal:articles_reponses_forum:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'afficheformreponse',
						'label' => '<:escal:affichage_form_reponse:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglereponsearticle',
						'label' => '<:escal:articles_premier_message:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_portfolio',
						'titre' => '<:escal:portfolio:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'portfolio',
						'label' => '<:escal:articles_portfolio:>',
						'explication' => '<:escal:articles_portfolio_explication:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifdoc',
						'label' => '<:escal:articles_portfolio_descriptif:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'descriptifdoccouper',
						'label' => '<:escal:texte_coupe:>',
						'afficher_si' => '@descriptifdoc@=="oui"',
						'afficher_si_avec_post' => "oui",
						'defaut' => '300',
						)
					),
				)
			),// fin du fieldsetles articles
				)
			),// fin du fieldset la page article
// la page contact
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_page_contact',
				'label' => '<:escal:page_contact3:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
// infos
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_contact_infos',
				'label' => '<:escal:infos:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_contact',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article44" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_contact',
						'texte' => '<:escal:contact_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'contactbienvenue',
						'label' => '<:escal:contact_texte_accueil:>',
						'explication' => '<:escal:contact_texte_accueil_explication:>',
						)
					),
				)
			),// fin du fieldset infos

// champ mail
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_champ_mail',
				'label' => '<:escal:contact_champ_mail:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'contactmail',
						'label' => '<:escal:contact_expediteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset champ mail
// champ supplémentaire 1
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_champ_sup1',
				'label' => '<:escal:contact_sup1:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup1',
						'label' => '<:escal:contact_sup:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrechampsup1',
						'label' => '<:escal:contact_libelle:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup1oblig',
						'label' => '<:escal:contact_obli:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset champ supplémentaire 1
// champ supplémentaire 2
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_champ_sup2',
				'label' => '<:escal:contact_sup2:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup2',
						'label' => '<:escal:contact_sup:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrechampsup2',
						'label' => '<:escal:contact_libelle:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup21oblig',
						'label' => '<:escal:contact_obli:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset champ supplémentaire 2
// motif message
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_motif_message',
				'label' => '<:escal:contact_motif_message:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'radio',
						'label' => '<:escal:contact_activer_champ:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'radiooblig',
						'label' => '<:escal:contact_obli:>',
						'explication' => '<:escal:contact_cases_explication:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_contact_libelle',
						'texte' => '<:escal:contact_libelle_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif1',
						'label' => '<:escal:contact_motif1:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif2',
						'label' => '<:escal:contact_motif2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif3',
						'label' => '<:escal:contact_motif3:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif4',
						'label' => '<:escal:contact_motif4:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif5',
						'label' => '<:escal:contact_motif5:>',
						)
					),
				)
			),// fin du fieldset motif message
// cases à cocher
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_contact_cases',
				'label' => '<:escal:contact_cases:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'checkbox',
						'label' => '<:escal:contact_activer_champ:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'checkboxoblig',
						'label' => '<:escal:contact_obli:>',
						'explication' => '<:escal:contact_cases_explication:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'checkboxliste',
						'label' => '<:escal:contact_presentation:>',
						'defaut' => 'ligne',
						'data' => array(
							'ligne' => '<:escal:contact_ligne:>',
							'liste' => '<:escal:contact_liste:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrecheckbox',
						'label' => '<:escal:contact_libelle_gen:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_contact_libelle',
						'texte' => '<:escal:contact_libelle_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox1',
						'label' => '<:escal:contact_libelle1:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox2',
						'label' => '<:escal:contact_libelle2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox3',
						'label' => '<:escal:contact_libelle3:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox4',
						'label' => '<:escal:contact_libelle4:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox5',
						'label' => '<:escal:contact_libelle5:>',
						)
					),
				)
			),// fin du fieldset cases à cocher
// message de retour
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_contact_retour',
				'label' => '<:escal:contact_retour:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'textarea',
					'options' => array(
						'nom' => 'contactretour',
						'explication' => '<:escal:contact_retour_explication:>',
						'defaut' => _T('escal:contact_retour_commentaire'),
						'rows' => '3',
						'conteneur_class' => 'pleine_largeur',
						)
					),
				)
			),// fin du fieldset message de retour

// destinataire
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_destinataire',
				'label' => '<:escal:destinataire:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'destinataire',
						'label' => '<:escal:contact_destinataire:>',
						'defaut' => lire_config('email_webmaster'),
						'conteneur_class' => 'pleine_largeur',
						'onglet' => 'oui',
						)
					),
				)
			),// fin du fieldset destinataire

				)
			),// fin du fieldset la page contact




// la page recherche
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_page_recherche',
				'label' => '<:escal:page_recherche:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'explic_resultats_recherche',
				'texte' => '<:escal:resultats_recherche:>',
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_articles',
				'label' => '<:escal:resultat_articles:>',
				'defaut' => 'oui',
				'data' => array(
					'oui' => '<:escal:oui:>',
					'non' => '<:escal:non:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_ordre_articles',
				'label' => '<:escal:resultat_ordre_articles:>',
				'defaut' => 'points',
				'data' => array(
					'points' => '<:escal:affichage_ordre_pertinence:>',
					'date' => '<:escal:affichage_ordre_dateinv:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_rubriques',
				'label' => '<:escal:resultat_rubriques:>',
				'defaut' => 'oui',
				'data' => array(
					'oui' => '<:escal:oui:>',
					'non' => '<:escal:non:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_ordre_rubriques',
				'label' => '<:escal:resultat_ordre_rubriques:>',
				'defaut' => 'points',
				'data' => array(
					'points' => '<:escal:affichage_ordre_pertinence:>',
					'date' => '<:escal:affichage_ordre_dateinv:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_messages',
				'label' => '<:escal:resultat_messages:>',
				'defaut' => 'oui',
				'data' => array(
					'oui' => '<:escal:oui:>',
					'non' => '<:escal:non:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_ordre_forums',
				'label' => '<:escal:resultat_ordre_forums:>',
				'defaut' => 'points',
				'data' => array(
					'points' => '<:escal:affichage_ordre_pertinence:>',
					'date_heure' => '<:escal:affichage_ordre_dateinv:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_sites',
				'label' => '<:escal:resultat_sites:>',
				'defaut' => 'oui',
				'data' => array(
					'oui' => '<:escal:oui:>',
					'non' => '<:escal:non:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'resultat_ordre_sites',
				'label' => '<:escal:resultat_ordre_sites:>',
				'defaut' => 'points',
				'data' => array(
					'points' => '<:escal:affichage_ordre_pertinence:>',
					'date' => '<:escal:affichage_ordre_dateinv:>',
					)
				)
			),

				)
			),// fin du fieldset le forum




// le forum du site
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_page_forum',
				'label' => '<:escal:page_forum:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'doc_contact',
				'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?page=forumSite-rubrique&lang=fr" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
				)
			),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'paginforumsite',
				'label' => '<:escal:nombre_sujets_pagination:>',
				'defaut' => '10',
				)
			),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'explic_noisettes_forum',
				'texte' => '<:escal:noisettes_forum_explication:>',
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'colforumsite',
				'label' => '<:escal:noisettes_forum_colonne:>',
				'defaut' => 'gauche',
				'data' => array(
					'gauche' => '<:escal:gauche:>',
					'droite' => '<:escal:droite:>',
					)
				)
			),

				)
			),// fin du fieldset le forum


// un hidden pour stocker les données
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}