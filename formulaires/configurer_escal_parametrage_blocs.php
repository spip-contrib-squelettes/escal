<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_parametrage_blocs_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configparam-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:bloc_parametrage').'</h3>'
				),

// titres et contenus
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_titre_contenu',
				'label' => '<:escal:titre_contenu:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_titre_contenu2',
						'texte' => '<:escal:noisettes_explication:>',
						)
					),
// blocs communs
		// acces direct
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_acces_direct',
						'label' => '<:escal:acces_direct:>',
						'onglet' => 'oui',
						'onglet_vertical' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_acces_direct',
								'titre' => '<:escal:doc_acces_direct:>',
								'texte' => '<:escal:noisettes_acces_direct_explication:>',
								)
							),
						)
					),// fin du fieldset acces direct
		// Actus
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_actus',
						'label' => '<:escal:actus:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_actus',
								'titre' => '<:escal:doc_actus:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreactus',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'taillelogoactus',
								'label' => '<:escal:taille_logo:>',
								'defaut' => '50',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'couperactus',
								'label' => '<:escal:nombre_caracteres:>',
								'defaut' => '80',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'hauteuractus',
								'label' => '<:escal:annonce_defil_hauteur:>',
								'defaut' => '100',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'tempoactus',
								'label' => '<:escal:noisettes_actus_tempo:>',
								'explication' => '<:escal:annonce_defil_tempo_explication:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nbractus',
								'label' => '<:escal:nombre_actus:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'testactus',
								'label' => '<:escal:noisettes_actus_enlever:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset actus
		// annuaire
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_annuaire_auteurs',
						'label' => '<:escal:annuaire_auteurs:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_annuaire_auteurs',
								'titre' => '<:escal:doc_annuaire_auteurs:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'lien_trombino_auteurs',
								'label' => '<:escal:noisettes_annuaire_lien:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'iconesauteurs',
								'label' => '<:escal:noisettes_annuaire_icones:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'couleur',
							'options' => array(
								'nom' => 'couleuradmin',
								'label' => '<:escal:noisettes_annuaire_admin:>',
								'explication' => '<:escal:par_defaut:>#336699',
								'defaut' => '#336699',
								)
							),
						array(
							'saisie' => 'couleur',
							'options' => array(
								'nom' => 'couleurredacteur',
								'label' => '<:escal:noisettes_annuaire_redacteur:>',
								'explication' => '<:escal:par_defaut:>#009933',
								'defaut' => '#009933',
								)
							),
						array(
							'saisie' => 'couleur',
							'options' => array(
								'nom' => 'couleurvisiteur',
								'label' => '<:escal:noisettes_annuaire_visiteur:>',
								'explication' => '<:escal:par_defaut:>#000000',
								'defaut' => '#000000',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'paginauteurs',
								'label' => '<:escal:noisettes_annuaire_pagination:>',
								'defaut' => '20',
								)
							),
						)
					),// fin du fieldset annuaire
		// articles de rubrique
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_articles_rubrique',
						'label' => '<:escal:articles_rubrique:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_articles_rubrique',
								'titre' => '<:escal:doc_articles_rubrique:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreartderub',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartderub',
								'label' => '<:escal:nombre_articles:>',
								'defaut' => '10',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'paginartderub',
								'label' => '<:escal:nombre_articles_pagination:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'dateartderub',
								'label' => '<:escal:affichage_date_pub:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'auteurartderub',
								'label' => '<:escal:affichage_nom_auteur:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset articles de rubrique
		// articles les plus vus
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_articles_plus_vus',
						'label' => '<:escal:articles_plus_vus:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_articles_plus_vus',
								'titre' => '<:escal:doc_articles_plus_vus:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titretop',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombretop',
								'label' => '<:escal:nombre_articles:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset articles les plus vus
		// perso
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_noisette_perso',
						'label' => '<:escal:noisette_perso:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_personnalise',
								'titre' => '<:escal:doc_personnalise:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreperso',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'ordreperso',
								'label' => '<:escal:affichage_ordre:>',
								'defaut' => 'date',
								'data' => array(
									'date' => '<:escal:affichage_ordre_dateinv:>',
									'num titre' => '<:escal:affichage_ordre_num:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'choixperso',
								'label' => '<:escal:affichage_choix:>',
								'defaut' => 'listedateauteur',
								'data' => array(
									'listedateauteur' => '<:escal:noisettes_perso_liste1:>',
									'listesimple' => '<:escal:noisettes_perso_liste2:>',
									'listetexte' => '<:escal:noisettes_perso_liste3:>',
									'listedefilante' => '<:escal:noisettes_perso_liste4:>',
									)
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_noisettes_perso_choix3',
								'titre' => '<:escal:noisettes_perso_choix3:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartperso',
								'label' => '<:escal:nombre_articles:>',
								'defaut' => '15',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'paginperso',
								'label' => '<:escal:nombre_articles_pagination:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_noisettes_perso_choix2',
								'titre' => '<:escal:noisettes_perso_choix2:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'couperperso',
								'label' => '<:escal:nombre_caracteres:>',
								'defaut' => '80',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_noisettes_perso_choix4',
								'titre' => '<:escal:noisettes_perso_choix4:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'tempoperso',
								'label' => '<:escal:noisettes_perso_tempo:>',
								'explication' => '<:escal:annonce_defil_tempo_explication:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset perso
		// calendrier
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_calendrier',
						'label' => '<:escal:calendrier:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_calendrier',
								'titre' => '<:escal:doc_mini_calendrier:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrecalendrier',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'lienagenda',
								'label' => '<:escal:noisettes_calendrier_lien_agenda:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'listeeventscalendrier',
								'label' => '<:escal:noisettes_calendrier_events:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'url_event',
								'label' => '<:escal:noisettes_calendrier_renvoi:>',
								'explication' => '<:escal:noisettes_calendrier_explication2:>',
								'defaut' => 'evenement',
								'data' => array(
									'evenement' => '<:escal:noisettes_calendrier_renvoi_evenement:>',
									'article' => '<:info_l_article:>',
									'aucun' => '<:escal:noisettes_calendrier_renvoi_aucun:>',
									)
								)
							),
						array(
							'saisie' => 'couleur',
							'options' => array(
								'nom' => 'couleureventscalendrier',
								'label' => '<:escal:noisettes_calendrier_couleur:>',
								'explication' => '<:escal:noisettes_calendrier_explication2:>',
								'defaut' => '#336699',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'pagincalendrier',
								'label' => '<:escal:noisettes_calendrier_pagination:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'liennouvelevent',
								'label' => '<:escal:noisettes_calendrier_ajouter:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset calendrier
		// derniers_articles
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_derniers_articles',
						'label' => '<:public:derniers_articles:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_derniers_articles',
								'titre' => '<:escal:doc_derniers_articles2:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrederniersart',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombrederniersart',
								'label' => '<:escal:nombre_articles:>',
								'defaut' => '10',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'datederniersart',
								'label' => '<:escal:affichage_date_pub:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'auteurderniersart',
								'label' => '<:escal:affichage_nom_auteur:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'textederniersart',
								'label' => '<:escal:affichage_debut:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'tailletextederniersarts',
								'label' => '<:escal:texte_coupe:>',
								'defaut' => '150',
								)
							),
						)
					),// fin du derniers_articles
		// Derniers articles syndiqués
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_derniers_art_syndic',
						'label' => '<:escal:derniers_articles_syndiques:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_derniers_art_syndic',
								'titre' => '<:escal:doc_derniers_art_syndic:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titresitesrecents',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'exclurefav3',
								'label' => '<:escal:noisettes_sites_exclus:>',
								'explication' => '<:escal:noisettes_sites_exclus_explication:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartsitesrecents',
								'label' => '<:escal:nombre_articles:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset Derniers articles syndiqués
		// derniers_commentaires
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_derniers_comments',
						'label' => '<:public:derniers_commentaires:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_derniers_comments',
								'titre' => '<:escal:doc_derniers_comments:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titredernierscomms',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nbredernierscomms',
								'label' => '<:escal:nombre_comments:>',
								'defaut' => '15',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'pagincomment',
								'label' => '<:escal:nombre_comments_pagination:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset derniers_commentaires
		// edito
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_edito',
						'label' => '<:escal:edito:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_edito',
								'titre' => '<:escal:doc_edito:>',
								'texte' => '<:escal:noisettes_edito_explication:>',
								)
							),
						)
					),// fin du fieldset edito
		// évènements à venir
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_evenements_a_venir',
						'label' => '<:escal:evenements_venir:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_evenements_a_venir',
								'titre' => '<:escal:doc_evenements_a_venir:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titreevenements',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'paginevenements',
								'label' => '<:escal:noisettes_calendrier_pagination:>',
								'defaut' => '10',
								)
							),
						)
					),// fin du fieldset évènements à venir
		// identification
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_identification',
						'label' => '<:escal:identification:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_identification',
								'titre' => '<:escal:doc_identification:>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_identification',
								'texte' => '<:escal:noisettes_identification_explication:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'inscription',
								'label' => '<:escal:noisettes_identification_redacteur_label:>',
								'explication' => '<:escal:noisettes_identification_redacteur:>',
								'defaut' => 'non',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'inscription_visiteur',
								'label' => '<:escal:noisettes_identification_visiteur_label:>',
								'explication' => '<:escal:noisettes_identification_visiteur:>',
								'defaut' => 'non',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset identification
		// menu vertical depliant
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_menuV',
						'label' => '<:escal:menu_vertical_depliant:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_menuV',
								'titre' => '<:escal:doc_menuv1:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titremenuV1',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'articlesmenuV1',
								'label' => '<:escal:affiche_articles:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset menu vertical depliant
		// menu vertical déroulant à droite
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_menuV2',
						'label' => '<:escal:menu_vertical_deroulant:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_menuV2',
								'titre' => '<:escal:doc_menuv2:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'articlesmenuV2',
								'label' => '<:escal:affiche_articles:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset menu vertical déroulant à droite
		// navigation par mots-clés
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_nav_mots2',
						'label' => '<:escal:nav_mot_cle:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_navigation_mots_cles',
								'titre' => '<:escal:doc_navigation_mots_cles:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrenavmot2',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'groupemot',
								'label' => '<:escal:noisettes_nav_groupe:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nbreartnavmot2',
								'label' => '<:escal:noisettes_nav_nombre:>',
								'defaut' => '10',
								)
							),
						)
					),// fin du fieldset navigation par mots-clés
		// photos
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_photos_hasard',
						'label' => '<:escal:photos_hasard2:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_photos_hasard',
								'titre' => '<:escal:doc_photos_hasard:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrephotos',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombrephotos',
								'label' => '<:escal:nombre_images:>',
								'defaut' => '10',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'tempophotos',
								'label' => '<:escal:noisettes_photos_tempo:>',
								'explication' => '<:escal:annonce_defil_tempo_explication:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'lienphotos',
								'label' => '<:escal:noisettes_photos_lien:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),//fin du fieldset photos
		// recherche multi-critères
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_recherche_multi',
						'label' => '<:escal:recherche_multi:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_recherche_multi',
								'titre' => '<:escal:doc_recherche_multi:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrerecherchemulti',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_recherche_multi',
								'titre' => '<:escal:noisettes_multi_choix:>',
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti1',
								'label' => '<:escal:noisettes_multi_groupe1:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti2',
								'label' => '<:escal:noisettes_multi_groupe2:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti3',
								'label' => '<:escal:noisettes_multi_groupe3:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti4',
								'label' => '<:escal:noisettes_multi_groupe4:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti5',
								'label' => '<:escal:noisettes_multi_groupe5:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti6',
								'label' => '<:escal:noisettes_multi_groupe6:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti7',
								'label' => '<:escal:noisettes_multi_groupe7:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti8',
								'label' => '<:escal:noisettes_multi_groupe8:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						array(
							'saisie' => 'escal_titre_groupe_mots',
							'options' => array(
								'nom' => 'recherchemulti9',
								'label' => '<:escal:noisettes_multi_groupe9:>',
								'option_intro' => '<:escal:aucun:>',
								'table_liaison' => 'articles',
								'groupes_exclus' => array('affichage','trombino','type_rubrique')
								)
							),
						)
					),// fin du fieldset recherche multi-critères
		// sites favoris
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_sites_favoris',
						'label' => '<:escal:sites_favoris:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_sites_favoris',
								'titre' => '<:escal:doc_sites_favoris:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titresitesfav',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombresitesfav',
								'label' => '<:escal:nombre_sites:>',
								'defaut' => '10',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'ordresitesfav',
								'label' => '<:escal:affichage_ordre:>',
								'defaut' => 'XXXXXX',
								'data' => array(
									'date' => '<:escal:affichage_ordre_date:>',
									'dateinverse' => '<:escal:affichage_ordre_dateinv:>',
									'hasard' => '<:escal:affichage_ordre_hasard:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'tempositesfav',
								'label' => '<:escal:noisettes_sites_tempo:>',
								'explication' => '<:escal:annonce_defil_tempo_explication:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset sites favoris
		// statistiques
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_stats',
						'label' => '<:titre_statistiques:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_stats',
								'titre' => '<:escal:doc_stats:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrestats',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_noisettes_stats_choix',
								'titre' => '<:escal:noisettes_stats_choix:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'totalvisites',
								'label' => '<:escal:noisettes_stats_visites:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'totalpagesvisitees',
								'label' => '<:escal:noisettes_stats_pages:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'moyennevisites',
								'label' => '<:escal:noisettes_stats_moyenne:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'periodevisites',
								'label' => '<:escal:noisettes_stats_periode:>',
								'defaut' => '365',
								),
							'verifier' => array(
								'type' => 'entier',
								'options' => array(
									'min' => 1,
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'grosjour',
								'label' => '<:escal:noisettes_stats_max:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'aujourdhui',
								'label' => '<:escal:noisettes_stats_jour:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'visiteursenligne',
								'label' => '<:escal:noisettes_stats_ligne:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'dernierarticleparu',
								'label' => '<:escal:article_dernier:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'sujetsforum',
								'label' => '<:escal:noisettes_stats_sujets:>',
								'defaut' => 'non',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_noisettes_stats_affiche',
								'texte' => '<:escal:noisettes_stats_affiche:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'cesitecompte',
								'label' => '<:escal:noisettes_stats_libelle:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'explic_noisettes_stats_et',
								'texte' => '<:escal:noisettes_stats_et:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'auteurssite',
								'label' => '<:escal:noisettes_stats_auteurs:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								'afficher_si_avec_post' => "oui",
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'rubriquessite',
								'label' => '<:escal:noisettes_stats_rubriques:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								'afficher_si_avec_post' => "oui",
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'articlessite',
								'label' => '<:escal:noisettes_stats_articles:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								'afficher_si_avec_post' => "oui",
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'commssite',
								'label' => '<:escal:noisettes_stats_comments:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								'afficher_si_avec_post' => "oui",
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'motssite',
								'label' => '<:escal:noisettes_stats_mots:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								'afficher_si_avec_post' => "oui",
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'sitessite',
								'label' => '<:escal:noisettes_stats_sites:>',
								'afficher_si' => '@cesitecompte@=="oui"',
								'afficher_si_avec_post' => "oui",
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset statistiques
		// sites (sur le web)
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_sur_web',
						'label' => '<:public:sur_web:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_sur_web',
								'titre' => '<:escal:doc_sur_web:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titresites',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'sitessyndic',
								'label' => '<:escal:noisettes_sur_web_syndic:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'exclurefav',
								'label' => '<:escal:noisettes_sites_exclus:>',
								'explication' => '<:escal:noisettes_sites_exclus_explication:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'logositeweb',
								'label' => '<:escal:affichage_logo_site:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'taillelogositeweb',
								'label' => '<:escal:taille_logo_site:>',
								'defaut' => '40',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombresites',
								'label' => '<:escal:nombre_sites:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartsites',
								'label' => '<:escal:nombre_articles_sites:>',
								'defaut' => '5',
								)
							),
						)
					),// fin du fieldset sites (sur le web)
		// videos
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_videos',
						'label' => '<:escal:videos:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_videos',
								'titre' => '<:escal:doc_videos:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrevideoaccueil',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						)
					),// fin du fieldset videos

// blocs page rubrique
			array(
				'saisie' => 'explication',
				'options' => array(
					'nom' => 'explic_noisette_rubrique',
					'titre' => '<:escal:noisette_rubrique:>',
					)
				),
		// à découvrir
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_noisettes_decouvrir',
						'label' => '<:escal:a_decouvrir:>',
						'onglet' => 'oui',
						'onglet_vertical' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_noisettes_decouvrir',
								'titre' => '<:escal:doc_a_decouvrir:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titredecouvrirarticles',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'siteourub',
								'label' => '<:escal:noisettes_decouvrir_ou:>',
								'defaut' => 'rub',
								'data' => array(
									'site' => '<:escal:noisettes_decouvrir_site:>',
									'rub' => '<:escal:noisettes_decouvrir_rubrique:>',
									)
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartrecents',
								'label' => '<:escal:noisettes_decouvrir_recents:>',
								'defaut' => '2',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombrearthasard',
								'label' => '<:escal:noisettes_decouvrir_hasard:>',
								'defaut' => '2',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartplusvus',
								'label' => '<:escal:noisettes_decouvrir_plus_vus:>',
								'defaut' => '2',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombreartmoinsvus',
								'label' => '<:escal:noisettes_decouvrir_moins_vus:>',
								'defaut' => '2',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'pagindecouvrir',
								'label' => '<:escal:nombre_articles_pagination:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'auteurdecouvrir',
								'label' => '<:escal:affichage_nom_auteur:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'datedecouvrir',
								'label' => '<:escal:affichage_date_pub:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset à découvrir
		// à télécharger
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_rub_telecharger',
						'label' => '<:escal:a_telecharger:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_rub_telecharger',
								'titre' => '<:escal:doc_telecharger_art_rub:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titredocsrub',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						)
					),// fin du fieldset à télécharger
// blocs page article
			array(
				'saisie' => 'explication',
				'options' => array(
					'nom' => 'explic_noisette_article',
					'titre' => '<:escal:noisette_article:>',
					)
				),
		// à télécharger
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_art_telecharger',
						'label' => '<:escal:a_telecharger:>',
						'onglet' => 'oui',
						'onglet_vertical' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_art_telecharger',
								'titre' => '<:escal:doc_telecharger_art_rub:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titredocsart',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						)
					),// fin du fieldset à télécharger
		// Dans la même rubrique
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_meme_rub',
						'label' => '<:info_meme_rubrique:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_meme_rub',
								'titre' => '<:escal:doc_meme_rubrique:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrememerub',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'nombrememerub',
								'label' => '<:escal:nombre_articles:>',
								'defaut' => '10',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'paginmemerub',
								'label' => '<:escal:nombre_articles_pagination:>',
								'defaut' => '5',
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'ordrememerub',
								'label' => '<:escal:affichage_ordre:>',
								'defaut' => 'date',
								'data' => array(
									'date' => '<:escal:affichage_ordre_dateinv:>',
									'hasard' => '<:escal:affichage_ordre_hasard:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'datememerub',
								'label' => '<:escal:affichage_date_pub:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						array(
							'saisie' => 'radio',
							'options' => array(
								'nom' => 'auteurmemerub',
								'label' => '<:escal:affichage_nom_auteur:>',
								'defaut' => 'oui',
								'data' => array(
									'oui' => '<:item_oui:>',
									'non' => '<:item_non:>',
									)
								)
							),
						)
					),// fin du fieldset Dans la même rubrique
		// Mots-clés associés
				array(
					'saisie' => 'fieldset',
					'options' => array(
						'nom' => 'fieldset_mots_clefs_associes',
						'label' => '<:escal:mots_clefs_associes:>',
						'onglet' => 'oui',
						),
				'saisies' => array(
						array(
							'saisie' => 'explication',
							'options' => array(
								'nom' => 'doc_mot_clefs_associes',
								'titre' => '<:escal:doc_mot_cles_associes:>',
								)
							),
						array(
							'saisie' => 'input',
							'options' => array(
								'nom' => 'titrenavmot',
								'label' => '<:escal:titre_noisette:>',
								)
							),
						)
					),// fin du fieldset Mots-clés associés
				)
			),// fin du fieldset titre et contenu
// deplier et replier
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_deplier',
				'label' => '<:escal:deplier_replier:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_deplier_replier',
						'texte' => '<:escal:deplier_replier_explication:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_deplier_replier2',
						'titre' => '<:escal:noisette_commun:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleaccesdirect',
						'label' => '<:escal:acces_direct:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleactus',
						'label' => '<:escal:actus:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglelisteauteurs',
						'label' => '<:escal:annuaire_auteurs:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglearticlelibre1',
						'label' => '<:escal:article_libre1:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglearticlelibre2',
						'label' => '<:escal:article_libre2:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglearticlelibre3',
						'label' => '<:escal:article_libre3:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglearticlelibre4',
						'label' => '<:escal:article_libre4:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglearticlelibre5',
						'label' => '<:escal:article_libre5:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleartderub',
						'label' => '<:escal:articles_rubrique:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggletop',
						'label' => '<:escal:articles_plus_vus:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleperso',
						'label' => '<:escal:noisette_perso:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglecalendrier',
						'label' => '<:escal:calendrier:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglesitesrecents',
						'label' => '<:escal:derniers_articles_syndiques:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggledernierscomments',
						'label' => '<:public:derniers_commentaires:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleedito',
						'label' => '<:escal:edito:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleevenements',
						'label' => '<:escal:evenements_venir:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggleidentification',
						'label' => '<:escal:identification:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglephotos',
						'label' => '<:escal:photos_hasard2:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglerainette',
						'label' => '<:escal:rainette:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglemulticritere',
						'label' => '<:escal:recherche_multi:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglesitesfav',
						'label' => '<:escal:sites_favoris:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglestats',
						'label' => '<:titre_statistiques:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglesites',
						'label' => '<:public:sur_web:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_deplier_replier3',
						'titre' => '<:escal:noisette_rubrique:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglederniersarticles',
						'label' => '<:public:derniers_articles:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'toggledecouvrirarticles',
						'label' => '<:escal:a_decouvrir:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_deplier_replier4',
						'titre' => '<:escal:noisette_article:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglememerubrique',
						'label' => '<:info_meme_rubrique:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// blocs latéraux et images
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_cadres',
				'label' => '<:escal:cadres:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'bordcadres',
						'label' => '<:escal:cadres_bords:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurimagenoisettes',
						'label' => '<:escal:cadres_images:>',
						'explication' => '<:escal:cadres_images_largeur:>',
						'defaut' => '170',
						)
					),
				)
			),// fin du fieldset

				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),
		);
	return $saisies;
}