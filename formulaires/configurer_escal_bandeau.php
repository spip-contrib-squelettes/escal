<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_bandeau_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configbandeau-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:bandeau').'</h3>'
				),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'docbandeau',
				'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article25" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
				)
		),
// choix de l'option
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetoptionbandeau',
				'label' => '<:escal:bandeau_choix_option:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'optionbandeau',
						'defaut' => 'option1',
						'conteneur_class' => 'pleine_largeur',
						'data' => array(
							'option1' => '<:escal:bandeau_choix_item1:>',
							'option2' => '<:escal:bandeau_choix_item2:>',
							'option3' => '<:escal:bandeau_choix_item3:>',
							'option4' => '<:escal:bandeau_choix_item4:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'alignbandeau',
						'defaut' => 'center',
						'label' => '<:escal:bandeau_choix_position:>',
						'explication' => '<:escal:bandeau_choix_explication:>',
						'data' => array(
							'left' => '<:escal:gauche:>',
							'center' => '<:escal:centre:>',
							'right' => '<:escal:droite:>',
							)
						)
					),
				)
			),// fin du fieldset
// Pour l'option 3
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetoption3',
				'label' => '<:escal:bandeau_option3:>',
				'afficher_si' => '@optionbandeau@=="option3"',
				'afficher_si_avec_post' => 'oui',
				
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicoption31',
						'texte' => '<:escal:bandeau_option3_explication1:>',
						)
					),
				array(
					'saisie' => 'escal_bandeau',
					'options' => array(
						'nom' => 'fondbandeau',
						'option_intro' => '<:escal:bandeau_option3_aucune:>',
						'conteneur_class' => 'pleine_largeur'
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicoption32',
						'texte' => '<:escal:bandeau_option3_explication2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'fondbandeau2',
						'conteneur_class' => 'pleine_largeur'
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicoption34',
						'titre' => '<:escal:bandeau_option3_explication4titre:>',
						'texte' => '<:escal:bandeau_option3_explication4:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicoption33',
						'titre' => '<:escal:bandeau_option3_explication3titre:>',
						'texte' => '<:escal:bandeau_option3_explication3:>',
						)
					),
				)
			),// fin du fieldset
// Textes du bandeau
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsettextebandeau',
				'label' => '<:escal:bandeau_texte:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'nomsite',
						'label' => '<:escal:bandeau_texte_nom:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nomsitetaille',
						'label' => '<:escal:bandeau_texte_taille:>',
						'defaut' => '50',
						'afficher_si' => '@nomsite@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'slogansite',
						'label' => '<:escal:bandeau_texte_slogan:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'slogansitetaille',
						'label' => '<:escal:bandeau_texte_taille:>',
						'defaut' => '25',
						'afficher_si' => '@slogansite@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifsite',
						'label' => '<:escal:bandeau_texte_descriptif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'descriptifsitetaille',
						'label' => '<:escal:bandeau_texte_taille:>',
						'defaut' => '12',
						'afficher_si' => '@descriptifsite@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				)
			),// fin du fieldset
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}