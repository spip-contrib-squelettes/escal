<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_multilinguisme_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configmultilingue-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('info_multilinguisme').'</h3>'
				),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'docmultilinguisme1',
				'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article151" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
				)
			),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'explicmultilinguisme1',
				'texte' => '<:escal:multilinguisme_explication1:>',
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'rubniveaudeux',
				'label' => '<:escal:multilinguisme_niveau:>',
				'defaut' => 'non',
				'data' => array(
					'oui' => '<:item_oui:>',
					'non' => '<:item_non:>',
					)
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'arianeniveaudeux',
				'label' => '<:escal:multilinguisme_ariane:>',
				'defaut' => 'non',
				'data' => array(
					'oui' => '<:item_oui:>',
					'non' => '<:item_non:>',
					)
				)
			),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'explicmultilinguisme2',
				'texte' => '<:escal:multilinguisme_explication2:>',
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'affichelangue',
				'label' => '<:escal:affichage_choix:>',
				'defaut' => 'code',
				'data' => array(
					'code' => '<:escal:multilinguisme_code:>',
					'nom' => '<:escal:multilinguisme_nom:>',
					'drapeau' => '<:escal:multilinguisme_drapeau:>',
					)
				)
			),
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}