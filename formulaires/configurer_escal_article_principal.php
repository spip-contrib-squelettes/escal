<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_article_principal_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configarticle-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:page_article').'</h3>'
				),
// les annonces
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_art_annonce',
				'label' => '<:escal:annonces:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annonceart',
						'label' => '<:escal:annonce_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'annoncedefilart',
						'label' => '<:escal:annonce_defil_afficher:>',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'haut' => '<:escal:haut:>',
							'bas' => '<:escal:bas:>',
							)
						)
					),
				)
			),// fin du fieldset
// les articles
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_articles_les',
				'label' => '<:titre_les_articles:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoart',
						'label' => '<:escal:articles_logo:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurimage',
						'label' => '<:escal:articles_largeur_images:>',
						'explication' => '<:escal:articles_largeur_images_classique:>',
						'defaut' => '400',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurimagepleinepage',
						'label' => '<:escal:articles_largeur_images:>',
						'explication' => '<:escal:articles_largeur_images_pleinepage:>',
						'defaut' => '850',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'dateart',
						'label' => '<:escal:affichage_date_pub_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteurart',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'visites',
						'label' => '<:escal:affichage_visites_popularite:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'visites-inter',
						'label' => '<:escal:affichage_visites_inter:>',
						'defaut' => ' - ',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailleoutilsarticle',
						'label' => '<:escal:taille_outils_article:>',
						'defaut' => '32',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'indent',
						'label' => '<:escal:indentation:>',
						'defaut' => '0',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_articles_reponses_forum',
						'titre' => '<:escal:articles_reponses_forum:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'afficheformreponse',
						'label' => '<:escal:affichage_form_reponse:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'togglereponsearticle',
						'label' => '<:escal:articles_premier_message:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_portfolio',
						'titre' => '<:escal:portfolio:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'portfolio',
						'label' => '<:escal:articles_portfolio:>',
						'explication' => '<:escal:articles_portfolio_explication:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifdoc',
						'label' => '<:escal:articles_portfolio_descriptif:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'descriptifdoccouper',
						'label' => '<:escal:texte_coupe:>',
						'afficher_si' => '@descriptifdoc@=="oui"',
						'afficher_si_avec_post' => "oui",
						'defaut' => '300',
						)
					),



				)
			),// fin du fieldset


				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}