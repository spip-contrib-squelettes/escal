<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_sommaire_annonces_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configannonce-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:annonces').'</h3>'
				),

// annonce
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetannonce',
				'label' => '<:escal:annonce:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_annonce',
						'titre' => '<:escal:doc_annonce:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'periodeaffichage',
						'label' => '<:escal:annonce_explication:>',
						'conteneur_class' => 'pleine_largeur',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// annonce défilantes
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetannoncdefil',
				'label' => '<:escal:annonce_defil:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_annoncedefil',
						'titre' => '<:escal:doc_annonce_defil:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'hauteurannoncedefil',
						'label' => '<:escal:annonce_defil_hauteur:>',
						'defaut' => '250',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoannonce',
						'label' => '<:escal:articles_logo:>',
						'defaut' => '150',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'ordreannonces',
						'label' => '<:escal:affichage_ordre:>',
						'defaut' => 'dateinverse',
						'data' => array(
							'date' => '<:escal:affichage_ordre_date:>',
							'dateinverse' => '<:escal:affichage_ordre_dateinv:>',
							'numtitre' => '<:escal:affichage_ordre_num:>',
							'hasard' => '<:escal:affichage_ordre_hasard:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'textesousimage',
						'label' => '<:escal:texte_sous_image:>',
						'defaut' => 'acote',
						'data' => array(
							'dessous' => '<:escal:texte_sous_image_oui:>',
							'acote' => '<:escal:texte_sous_image_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifannonce',
						'label' => '<:escal:affichage_descriptif:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'texteannonce',
						'label' => '<:escal:affichage_debut:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tempoannoncedefil',
						'label' => '<:escal:annonce_defil_tempo:>',
						'explication' => '<:escal:annonce_defil_tempo_explication:>',
						'defaut' => '8',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nbreannonces',
						'label' => '<:escal:annonce_defil_nombre:>',
						'defaut' => '10',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'affichecompteur',
						'label' => '<:escal:annonce_defil_nombre_affiche:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),

				)
			),// fin du fieldset



				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}