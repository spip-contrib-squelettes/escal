<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_bords_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configbords-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:bords_arrondis').'</h3>'
				),
// les bords
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_bords',
				'label' => '<:escal:bords:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
		// Items du menu horizontal
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_bords1',
						'titre' => '<:escal:bords1:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'bord1',
						'label' => '<span style="font-size: 50px; color:'
						.lire_config($chemin='escal/config/bord1',$defaut='#CCCCCC').
						'">&mdash;&mdash;</span>',
						'explication' => '<:escal:par_defaut:>#CCCCCC<br>',
						'defaut' => '#CCCCCC',
						)
					),
		// Pages article et rubrique, forums des articles, page contact, annuaire
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_bords2',
						'titre' => '<:escal:bords2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'bord2',
						'label' => '<span style="font-size: 50px; color:'
						.lire_config($chemin='escal/config/bord2',$defaut='#CCCCCC').
						'">&mdash;&mdash;</span>',
						'explication' => '<:escal:par_defaut:>#CCCCCC<br>',
						'defaut' => '#CCCCCC',
						)
					),
		// Tableaux
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_bords3',
						'titre' => '<:escal:bords3:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'bord3',
						'label' => '<span style="font-size: 50px; color:'
						.lire_config($chemin='escal/config/bord3',$defaut='#FFFFFF').
						'">&mdash;&mdash;</span>',
						'explication' => '<:escal:par_defaut:>#FFFFFF<br>',
						'defaut' => '#FFFFFF',
						)
					),
		// Menu vertical déroulant à droite
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_bords4',
						'titre' => '<:escal:menu_vertical_deroulant:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'bord4',
						'label' => '<span style="font-size: 50px; color:'
						.lire_config($chemin='escal/config/bord4',$defaut='#FFFFFF').
						'">&mdash;&mdash;</span>',
						'explication' => '<:escal:par_defaut:>#FFFFFF<br>',
						'defaut' => '#FFFFFF',
						)
					),
		// Items menu horizontal survolés, identification, recherche, événement calendrier
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_bords5',
						'titre' => '<:escal:bords5:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'bord5',
						'label' => '<span style="font-size: 50px; color:'
						.lire_config($chemin='escal/config/bord5',$defaut='#336699').
						'">&mdash;&mdash;</span>',
						'explication' => '<:escal:par_defaut:>#336699<br>',
						'defaut' => '#336699',
						)
					),
		// Forum du site
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_bords6',
						'titre' => '<:escal:bords6:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'bord6',
						'label' => '<span style="font-size: 50px; color:'
						.lire_config($chemin='escal/config/bord6',$defaut='#006699').
						'">&mdash;&mdash;</span>',
						'explication' => '<:escal:par_defaut:>#006699<br>',
						'defaut' => '#006699',
						)
					),
				)
			),// fin du fieldset les bords
// les arrondis
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_arrondis',
				'label' => '<:escal:arrondis:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'arrondis_explication',
						'texte' => '<:escal:arrondis_explication1:>
						<:escal:arrondis_explication2:><img src="'.find_in_path('prive/themes/spip/images/arrondis.png').'" alt="image pour arrondis" />',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'arrondis_explication_plus',
						'texte' => '<:escal:arrondis_explication3:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_arrondis_menus',
						'titre' => '<:escal:arrondis_menus:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi1',
						'label' => '<:escal:menu_horizontal:>',
						'defaut' => '10px',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi2',
						'label' => '<:escal:menu_vertical_deroulant:>',
						'defaut' => '10px',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_arrondis_centre_sommaire',
						'titre' => '<:escal:arrondis_centre_sommaire:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi31',
						'label' => '<:escal:arrondis_onglets:>',
						'defaut' => '8px 8px 0px 0px',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi32',
						'label' => '<:public:articles:>',
						'defaut' => '5px',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_arrondis_blocs_lateraux',
						'titre' => '<:escal:arrondis_blocs_lateraux:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi41',
						'label' => '<:escal:titre:>',
						'defaut' => '0px 20px 0px 0px',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi42',
						'label' => '<:info_texte:>',
						'defaut' => '0px 0px 0px 10px / 0px 0px 0px 100px',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_arrondis_autres',
						'titre' => '<:public:autres:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi5',
						'label' => '<:escal:arrondis_identification_recherche:>',
						'defaut' => '5px',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi6',
						'label' => '<:escal:annonce:>',
						'defaut' => '15px',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'arrondi7',
						'label' => '<:escal:arrondis_calendrier:>',
						'defaut' => '8px 8px 0 0',
						)
					),
				)
			),// fin du fieldset les arrondis

				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),
		);
	return $saisies;
}