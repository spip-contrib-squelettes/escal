<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_contact_principal_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configcontact-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:page_contact').'</h3>'
				),
// infos
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_contact_infos',
				'label' => '<:escal:infos:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'doc_contact',
						'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article44" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_contact',
						'texte' => '<:escal:contact_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'contactbienvenue',
						'label' => '<:escal:contact_texte_accueil:>',
						'explication' => '<:escal:contact_texte_accueil_explication:>',
						)
					),
				)
			),// fin du fieldset

// champ mail
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_champ_mail',
				'label' => '<:escal:contact_champ_mail:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'contactmail',
						'label' => '<:escal:contact_expediteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// champ supplémentaire 1
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_champ_sup1',
				'label' => '<:escal:contact_sup1:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup1',
						'label' => '<:escal:contact_sup:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrechampsup1',
						'label' => '<:escal:contact_libelle:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup1oblig',
						'label' => '<:escal:contact_obli:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// champ supplémentaire 2
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_champ_sup2',
				'label' => '<:escal:contact_sup2:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup2',
						'label' => '<:escal:contact_sup:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrechampsup2',
						'label' => '<:escal:contact_libelle:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'champsup21oblig',
						'label' => '<:escal:contact_obli:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				)
			),// fin du fieldset
// motif message
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_motif_message',
				'label' => '<:escal:contact_motif_message:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'radio',
						'label' => '<:escal:contact_activer_champ:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'radiooblig',
						'label' => '<:escal:contact_obli:>',
						'explication' => '<:escal:contact_cases_explication:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_contact_libelle',
						'texte' => '<:escal:contact_libelle_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif1',
						'label' => '<:escal:contact_motif1:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif2',
						'label' => '<:escal:contact_motif2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif3',
						'label' => '<:escal:contact_motif3:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif4',
						'label' => '<:escal:contact_motif4:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'motif5',
						'label' => '<:escal:contact_motif5:>',
						)
					),
				)
			),// fin du fieldset
// cases à cocher
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_contact_cases',
				'label' => '<:escal:contact_cases:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'checkbox',
						'label' => '<:escal:contact_activer_champ:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'checkboxoblig',
						'label' => '<:escal:contact_obli:>',
						'explication' => '<:escal:contact_cases_explication:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'checkboxliste',
						'label' => '<:escal:contact_presentation:>',
						'defaut' => 'ligne',
						'data' => array(
							'ligne' => '<:escal:contact_ligne:>',
							'liste' => '<:escal:contact_liste:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'titrecheckbox',
						'label' => '<:escal:contact_libelle_gen:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_contact_libelle',
						'texte' => '<:escal:contact_libelle_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox1',
						'label' => '<:escal:contact_libelle1:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox2',
						'label' => '<:escal:contact_libelle2:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox3',
						'label' => '<:escal:contact_libelle3:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox4',
						'label' => '<:escal:contact_libelle4:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'checkbox5',
						'label' => '<:escal:contact_libelle5:>',
						)
					),
				)
			),// fin du fieldset
// message de retour
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_contact_retour',
				'label' => '<:escal:contact_retour:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'textarea',
					'options' => array(
						'nom' => 'contactretour',
						'explication' => '<:escal:contact_retour_explication:>',
						'defaut' => _T('escal:contact_retour_commentaire'),
						'rows' => '3',
						'conteneur_class' => 'pleine_largeur',
						)
					),
				)
			),// fin du fieldset

// destinataire
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_destinataire',
				'label' => '<:escal:destinataire:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'destinataire',
						'label' => '<:escal:contact_destinataire:>',
						'defaut' => lire_config('email_webmaster'),
						'conteneur_class' => 'pleine_largeur',
						'onglet' => 'oui',
						)
					),
				)
			),// fin du fieldset





				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}