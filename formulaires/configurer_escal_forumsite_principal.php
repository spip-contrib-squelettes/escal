<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_forumsite_principal_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configforum-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:page_forum').'</h3>'
				),

		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'doc_contact',
				'titre' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?page=forumSite-rubrique&lang=fr" title="<:escal:documentation_voir:>"><:escal:documentation:></a>',
				)
			),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'paginforumsite',
				'label' => '<:escal:nombre_sujets_pagination:>',
				'defaut' => '10',
				)
			),
		array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'explic_noisettes_forum',
				'texte' => '<:escal:noisettes_forum_explication:>',
				)
			),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'colforumsite',
				'label' => '<:escal:noisettes_forum_colonne:>',
				'defaut' => 'gauche',
				'data' => array(
					'gauche' => '<:escal:gauche:>',
					'droite' => '<:escal:droite:>',
					)
				)
			),

				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}
