<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_layout_saisies_dist(){

	$saisies = array(

				'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configlayout-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:layout').'</h3>'
				),

// taille de la police
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsettaillepolice',
				'label' => '<:escal:taille_police:>',
				'onglet' => 'oui',
				'onglet_vertical' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explictaillepolice',
						'texte' => '<:escal:taille_police_explication:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillepolice',
						'label' => '<:escal:taille_pourcent:>',
						'defaut' => '75'
						)
					)
				)
			),// fin du fieldset
// Choix du layout
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldsetchoixlayout',
				'label' => '<:escal:layout_choix:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicchoixlayout',
						'titre' => '<:escal:layout_documentation:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'layout',
						'label' => '<:escal:layout_fluide:>',
						'defaut' => 'PMPfluide',
						'data' => array(
							'PMPfluide' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPMP.png').'" alt="layout PMP fluide" />',
							'MPPfluide' => '<img src="'.find_in_path('prive/themes/spip/images/layoutMPP.png').'" alt="layout MPP fluide" />',
							'PPMfluide' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPPM.png').'" alt="layout PPM fluide" />',
							'MPfluide' => '<img src="'.find_in_path('prive/themes/spip/images/layoutMP.png').'" alt="layout MP fluide" />',
							'PMfluide' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPM.png').'" alt="layout PM fluide" />',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'layout',
						'label' => '<:escal:layout_fixe:>',
						'data' => array(
							'PMP' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPMP.png').'" alt="layout PMP fixe" />',
							'MPP' => '<img src="'.find_in_path('prive/themes/spip/images/layoutMPP.png').'" alt="layout MPP fixe" />',
							'PPM' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPPM.png').'" alt="layout PPM fixe" />',
							'MP' => '<img src="'.find_in_path('prive/themes/spip/images/layoutMP.png').'" alt="layout MP fixe" />',
							'PM' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPM.png').'" alt="layout PM fixe" />',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'layout',
						'label' => '<:escal:layout_mixte:>',
						'data' => array(
							'PMPmixte' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPMP.png').'" alt="layout PMP mixte" />',
							'MPPmixte' => '<img src="'.find_in_path('prive/themes/spip/images/layoutMPP.png').'" alt="layout MPP mixte" />',
							'PPMmixte' => '<img src="'.find_in_path('prive/themes/spip/images/layoutPPM.png').'" alt="layout PPM mixte" />',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicchoixlayout',
						'texte' => '<:escal:layout_explication:>',
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explicchoixlayout',
						'titre' => '<:escal:layout_largeur:>',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurmaxlayoutfluide',
						'label' => '<:escal:layout_fluide_si:>',
						'defaut' => '1200',
						'explication' => '<:escal:layout_fluide_largeur:>',
						'afficher_si' => "@layout@ MATCH '/fluide/'",
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurlayoutbase',
						'label' => '<:escal:layout_fixe_si:>',
						'defaut' => '1200',
						'explication' => '<:escal:layout_fixe_largeur:>',
						'afficher_si' => '@layout@=="PMP"||@layout@=="MPP"||@layout@=="PPM"||@layout@=="MP"||@layout@=="PM"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'colonnelayoutbase',
						'defaut' => '250',
						'autocomplete' => 'defaut',
						'explication' => '<:escal:layout_largeur_colonnes:>',
						'afficher_si' => '@layout@=="PMP"||@layout@=="MPP"||@layout@=="PPM"||@layout@=="MP"||@layout@=="PM"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'colonnelayoutmixte',
						'label' => '<:escal:layout_mixte_si:>',
						'defaut' => '250',
						'autocomplete' => 'defaut',
						'explication' => '<:escal:layout_largeur_colonnes:>',
						'afficher_si' => "@layout@ MATCH '/mixte/'",
						'afficher_si_avec_post' => "oui",
						)
					),
				)
			),// fin du fieldset



// un hidden pour stocker les données
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}