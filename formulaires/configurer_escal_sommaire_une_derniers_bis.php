<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_sommaire_une_derniers_bis_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configune-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:derniers_articles_bis').'</h3>'
				),

// un fieldset
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_presentation',
				'label' => '<:escal:onglets_derniers_presentation:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nombrearticlesbis',
						'label' => '<:escal:onglets_nombre_articles:>',
						'defaut' => '20',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'modelepaginarticlesbis',
						'label' => '<:escal:onglets_bis_pagination:>',
						'defaut' => '',
						'data' => array(
							'' => '<:escal:onglets_bis_pagin_defaut:>',
							'prive' => '<:escal:onglets_bis_pagin_prive:>',
							'page' => '<:escal:onglets_bis_pagin_page:>',
							'page_precedent_suivant' => '<:escal:onglets_bis_pagin_pps:>',
							'precedent_suivant' => '<:escal:onglets_bis_pagin_ps:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'paginarticlesbis',
						'label' => '<:escal:nombre_articles_pagination:>',
						'defaut' => '5',
						)
					),
				)
			),// fin du fieldset
// pour chaque article
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_onglets_derniers_article_bis',
				'label' => '<:escal:onglets_derniers_article:>',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoartbis',
						'label' => '<:escal:articles_logo:>',
						'defaut' => '100',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'surtitrebis',
						'label' => '<:escal:affichage_surtitre:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'soustitrebis',
						'label' => '<:escal:affichage_soustitre:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'auteurbis',
						'label' => '<:escal:affichage_nom_auteur:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'datepubbis',
						'label' => '<:escal:affichage_date_pub:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'datemodifbis',
						'label' => '<:escal:affichage_date_modif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'rubriquebis',
						'label' => '<:escal:affichage_rubrique:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'descriptifbis',
						'label' => '<:escal:affichage_descriptif:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'chapeaubis',
						'label' => '<:escal:affichage_chapeau:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'textebis',
						'label' => '<:escal:affichage_debut:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailletextebis',
						'label' => '<:escal:texte_coupe:>',
						'defaut' => '300',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'imagebis',
						'label' => '<:escal:affichage_image:>',
						'defaut' => 'oui',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'taillelogoimgexerguebis',
						'label' => '<:escal:articles_largeur_image:>',
						'defaut' => '150',
						)
					),
				)
			),// fin du fieldset

				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}