<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_escal_popup_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<img class="cadre-icone" src="'.find_in_path('prive/themes/spip/images/configpopup-xx.svg').'" alt="" />
				<h3 class="titrem">'._T('escal:popup').'</h3>'
				),


				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_popup',
						'titre' => '<:escal:popup_doc:>',
						)
					),
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'activerpopup',
						'label' => '<:escal:popup_activer:>',
						'defaut' => 'non',
						'data' => array(
							'oui' => '<:item_oui:>',
							'non' => '<:item_non:>',
							)
						)
					),
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_popup2',
						'texte' => '<:escal:popup2_doc:>',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'largeurpopup',
						'label' => '<:escal:popup_largeur:>',
						'defaut' => '500',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tempspopup',
						'label' => '<:escal:popup_temps:>',
						'defaut' => '8',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'tailleimagepopup',
						'label' => '<:escal:articles_largeur_images:>',
						'defaut' => '200',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleurfondpopup',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_fond:>',
						'explication' => '<:escal:par_defaut:>#336699',
						'defaut' => '#336699',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'couleurtextepopup',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_texte:>',
						'explication' => '<:escal:par_defaut:>#ffffff',
						'defaut' => '#ffffff',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'lienpopup',
						'type' => 'color',
						'label' => '<:escal:fonds_noisettes_lien:>',
						'explication' => '<:escal:par_defaut:>#ecc442',
						'defaut' => '#ecc442',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'type' => 'color',
						'nom' => 'lienpopupsurvol',
						'label' => '<:escal:fonds_noisettes_lien_survol:>',
						'explication' => '<:escal:par_defaut:>#ff7f50',
						'defaut' => '#ff7f50',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'dureecookie',
						'label' => '<:escal:popup_dureecookie:>',
						'defaut' => '8',
						'afficher_si' => '@activerpopup@=="oui"',
						'afficher_si_avec_post' => "oui",
						)
					),
				array(
					'saisie' => 'hidden',
					'options' => array(
						'nom' => '_meta_casier',
						'defaut' => 'escal/config',
						)
					),

		);
	return $saisies;
}