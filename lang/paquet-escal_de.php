<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-escal?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'escal_description' => 'Fassung für SPIP 3 - enthält :
-* erweiterte Konfiguration im Redaktionsbereich
-* Wechsel von zwei- und dreispaltigem Layout per Wahl des Stylesheet
-* Unterstützung mehrsprachiger Websites
-* Forum im Stil von phpBB
-* grosse Auswahl an Seiten-Funktionselementen (noisettes), die Funktionen teilweise mehrfach abdecken (Benutzer-Identifikation, horizontale und vertikale Menüs)
-* Einfache Modifikation von Anordnung, Farben und Inhalten der vertikalen Funktionsblöcke
-*  2 vertikale und ein horizontales Menü mit aufklappbaren Untermenüs zur Auswahl, aktuelle Rubrik wird hervorgehoben
-* automatische Weiterleitung zum einzigen Artikel einer Rubrik
-* Schlagworte als Navigationselemente
-* Kalender bzw. Eventliste
-* Anzeige des neuesten oder der anderen Artikel der aktuellen Rubrik
-* Anzeige der Unterrubriken und Artikelübersicht für jede Rubrik
-* Anzeige der Forumsbeiträge zu einem Artikel
-* Anzeige eines Kontaktformulars für alle Autoren, die ihre Mailadresse eingetragen haben.
-* Erweiterte Kontaktseite
-* Sitemap
-* Backend-Datei unterstützt Syndikation per RSS
-* eigenes Stylesheet zum Drucken von Artikeln
-* Startseite mit Anmeldefunktion in zwei Varianten
', # MODIF
	'escal_slogan' => 'Multifunktionales Squelett, responsive und mehrsprachig, mit umfangreichen erweiterten Einstellungsmöglichkeiten.'
);
