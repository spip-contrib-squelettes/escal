<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-escal?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'escal_description' => 'Adaptado para la versión 3 de SPIP, propone:
-* una configuración avanzada en el espacio privado
-* una maquetación configurable en 2 o 3 columnas por elección de una hoja de estilo
-* una gestión del multilingüismo
-* un foro simple estilo phpBB
-* una amplia selección de avellanas para guardar o no, algunas son redundantes: identificación, menú horizontal y/o vertical.
-* lugar, color y contenido de los bloques laterales fáciles de cambiar
-* un menú horizontal y/o vertical (2 a elegir) desplegables con resaltado del elemento actual
-* una redirección automática al artículo si está solo en su sección
-* una navegación por palabras clave
-* un calendario y/o una lista de eventos
-* una visualización de los últimos artículos o artículos en la misma sección
-* una visualización de subtítulos y artículos en cada página rúbrica
-* una visualización de los grupos de artículos
-* un formulario de contacto de los autores si han indicado su correo electrónico
-* una página de contacto elaborada
-* un mapa del sitio
-* un archivo backend para la sindicación del sitio
-* una hoja de estilo especial para la impresión de artículos
-* un cuadro de inicio de sesión (2 opciones de visualización)', # MODIF
	'escal_slogan' => 'Esqueleto general, altamente configurable, sensible y multilingüe'
);
