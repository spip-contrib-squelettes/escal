<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/escal?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_decouvrir' => 'Виявити',
	'a_telecharger' => 'Завантажити',
	'acces_direct' => 'Прямий доступ',
	'accessibilite' => 'Доступність',
	'accessibilite_explication' => 'Відображення кнопки налаштування спеціальних можливостей над банером',
	'accessibilite_logo' => 'Вибір кольору логотипу',
	'accessibilite_police' => 'Вибір шрифту сайту',
	'actus' => 'Новини',
	'adresse_non' => 'Автор не повідомив свій e-mail',
	'affichage_auteur_articles' => 'Показувати імена авторів статей',
	'affichage_chapeau' => 'показувати шапку',
	'affichage_choix' => 'Вибір відображення',
	'affichage_date_modif' => 'Показувати дату оновлення',
	'affichage_date_pub' => 'Показувати дату публікації',
	'affichage_date_pub_modif' => 'Показувати дату публікації та дату зміни',
	'affichage_date_pub_ou_modif' => 'Відображення дати <br>(зміни, інакше публікації)',
	'affichage_debut' => 'Показувати початок тексту',
	'affichage_descriptif' => 'Показувати короткий опис',
	'affichage_form_reponse' => 'Форма для коментаря під статтею',
	'affichage_image' => 'Показати перше зображення',
	'affichage_logo_site' => 'Показати логотип сайту, якщо він є',
	'affichage_mots_cles' => '<strong>Показувати ключові слова, пов’язані зі статтями</strong>',
	'affichage_mots_cles_article' => 'Для сторінки статті',
	'affichage_mots_cles_rubrique' => 'Для сторінки теми',
	'affichage_mots_cles_une' => 'Для домашньої сторінки',
	'affichage_nom_auteur' => 'Показувати ім’я автора',
	'affichage_nombre_comments' => 'Показати кількість коментарів',
	'affichage_ordre' => 'Порядок відображення',
	'affichage_ordre_date' => 'Хронологічний порядок',
	'affichage_ordre_dateinv' => 'Зворотний хронологічний порядок',
	'affichage_ordre_datemodif' => 'За датою останньої зміни',
	'affichage_ordre_hasard' => 'Випадково',
	'affichage_ordre_num' => 'За номером у заголовку',
	'affichage_ordre_pertinence' => 'Релевантність',
	'affichage_rubrique' => 'Показати батьківську рубрику',
	'affichage_soustitre' => 'Показати підзаголовок',
	'affichage_surtitre' => 'Показати надзаголовок',
	'affichage_video' => 'Замініть всі ці елементи відео, якщо у статті є хоча б один.',
	'affichage_visites_inter' => 'Розділювач відвідувань та популярності',
	'affichage_visites_popularite' => 'Показати кількість відвідувань та популярність',
	'affiche_articles' => 'З відображенням статті',
	'agenda' => 'Розклад',
	'aide' => 'Помилка, технічна проблема, пропозиція... <a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?page=forumSite-rubrique&lang =fr " title="Форум">форум</a> створений для цього',
	'alerte_javascript' => 'Ця сторінка не може правильно працювати без JavaScript. Будь ласка, повторно активуйте його.',
	'alt_telechargements' => 'Завантаження',
	'annonce' => 'Оголошення',
	'annonce_afficher' => 'Показати блок «Оголошення»',
	'annonce_defil' => 'Прокручується реклама',
	'annonce_defil_afficher' => 'Показати блок “Annonces défilantes” («Прокручування реклами»)',
	'annonce_defil_hauteur' => 'Висота блоку (у пікселях)',
	'annonce_defil_nombre' => 'Кількість оголошень для відображення',
	'annonce_defil_nombre_affiche' => 'Показати кількість оголошень',
	'annonce_defil_tempo' => 'Затримка між оголошеннями (у секундах)',
	'annonce_defil_tempo_explication' => 'Для застосування нового тайм-ауту потрібно очищення кешу.',
	'annonce_explication' => '<ul>Виберіть дати показу статті серед об’яв:
		<li>дата початку показу = дата останньої зміни</li>
		<li>дата закінчення показу = дата попередньої публікації</li>
		</ul>',
	'annonces' => 'Оголошення',
	'annuaire' => 'Каталог',
	'annuaire_auteurs' => 'Каталог авторів',
	'annuaire_invitation' => 'Ви теж використовуєте ESCAL?<br>Тоді додайте свій сайт на цю сторінку.',
	'arrondis' => 'Заокруглення',
	'arrondis_blocs_lateraux' => 'Бічні блоки',
	'arrondis_calendrier' => 'Календар (майбутні події)',
	'arrondis_centre_sommaire' => 'Центральний блок зведеної сторінки',
	'arrondis_explication1' => 'Для однакового закруглення чотирьох кутів просто вкажіть бажане значення закруглення. Приклад: 10px<br>Для незакруглених кутів введіть значення 0<br>',
	'arrondis_explication2' => 'Для кутів з різним закругленням вказати значення в порядку :',
	'arrondis_explication3' => 'Щоб запустити тести та згенерувати відповідний код: <a class="spip_out" href="http://www.cssmatic.com/border-radius">CSSmatic</a>
		Для більш просунутого використання еліптичних фігур: <a class ="spip_out" href="http://www.alsacreations.com/astuce/lire/979-ovale-forme-elliptique-css3-sans-image.html">Alsacréations</a>',
	'arrondis_identification_recherche' => 'Ідентифікація та пошук',
	'arrondis_menus' => 'Меню',
	'arrondis_onglets' => 'Вкладки',
	'article_acces_direct_descriptif' => 'Ця стаття з’явиться в блоці «Прямий доступ» (“Accès direct”), якщо ви її активуєте',
	'article_acces_direct_texte' => 'Тут можна відобразити зміст статті з ключовим словом «acces-direct».<br><br>
		Заголовок блоку буде таким самим, як і заголовок статті.<br><br>
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24">Додаткова інформація</a>',
	'article_archive' => 'Пам’ятай!',
	'article_dernier' => 'Остання опублікована стаття:',
	'article_edito_descriptif' => 'Ця стаття з’явиться в блоці “Edito”, якщо ви активуєте її',
	'article_edito_texte' => 'Тут можна відобразити зміст статті з ключовим словом "edito".<br>
		Заголовок блоку буде таким самим, як і у статті.<br>
		Додаткова інформація у <a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23">цій статті</a>',
	'article_edito_titre' => 'Edito («редакційна стаття»)',
	'article_forum' => 'Форум статті',
	'article_imprimer' => 'Друк',
	'article_libre1' => 'Вільна стаття 1',
	'article_libre2' => 'Вільна стаття 2',
	'article_libre3' => 'Вільна стаття 3',
	'article_libre4' => 'Вільна стаття 4',
	'article_libre5' => 'Вільна стаття 5',
	'article_licence' => 'Ліцензія',
	'article_logo' => 'Максимальний розмір логотипу статті (у пікселях)',
	'article_mise_en_ligne' => 'Стаття опублікована ',
	'article_modifie' => 'останні зміни',
	'article_trouve' => 'знайдено статтю',
	'article_une' => 'Стаття для першої сторінки',
	'articlepdf_afficher' => 'Показати значок для рубрик',
	'articlepdf_explication' => 'Якщо ви активували плагін <a class="spip_out" href="http://contrib.spip.net/Article-PDF,2226" title="Documentation">"PDF Article"</a> вам більше нічого не потрібно робити. Значок, що дозволяє зберегти або роздрукувати статтю у форматі .pdf, буде автоматично відображатися на сторінках статті.<br>Він також може відображатися на сторінках рубрик.',
	'articlepdf_plugin' => 'ArticlePDF',
	'articles_associes' => 'Див. пов’язані статті ',
	'articles_associes_mot' => 'Статті/документи, пов’язані з ключем',
	'articles_auteur' => 'Статті цього автора:',
	'articles_derniers' => 'Нові статті',
	'articles_largeur_image' => 'Максимальна ширина зображення (у пікселях)',
	'articles_largeur_images' => 'Максимальна ширина зображень (у пікселях)',
	'articles_largeur_images_classique' => 'Для класичних статей',
	'articles_largeur_images_pleinepage' => 'Для повносторінкових статей (статей з ключовим словом "повносторінкові" -"pleinepage"), без бічних блоків',
	'articles_logo' => 'Максимальний розмір логотипу статті (у пікселях)',
	'articles_plus_vus' => 'Найпопулярніші статті',
	'articles_portfolio' => 'Показати портфоліо (дублювати з бічним блоком «Завантажити»)',
	'articles_portfolio_descriptif' => 'Відобразити опис документа, якщо він існує',
	'articles_portfolio_explication' => 'Дублювати з блоком «Завантажити»',
	'articles_premier_message' => 'Показувати лише перше повідомлення кожної гілки, відповіді можна розгорнути',
	'articles_reponses_forum' => 'Відповіді з форумів статей',
	'articles_rubrique' => 'Статті в рубриці',
	'articles_site' => 'Погляньте статті цього сайту',
	'articles_trouves' => 'знайдено статей',
	'aucun' => 'Ні',
	'aujourdhui' => 'Сьогодні: ',
	'auteurs' => 'автори',
	'avec' => 'з',
	'avec_le_squelette' => 'з шаблоном',

	// B
	'bandeau' => 'Банер',
	'bandeau_choix_explication' => 'Якщо зображення вашого банера не займає всю ширину, 
		ви можете вибрати його положення.<br>
		Однак будьте обережні з «крайовими ефектами», 
		коли відображаєте назву сайту, його слоган або опис.',
	'bandeau_choix_item1' => '<strong>Варіант 1</strong>: відображення логотипу Escal (за замовчуванням)',
	'bandeau_choix_item2' => '<strong>Варіант 2</strong>: відображення логотипу сайту.',
	'bandeau_choix_item3' => '<strong>Варіант 3</strong>: показ персоналізованого банера.',
	'bandeau_choix_item4' => '<strong>Варіант 4</strong>: немає зображення',
	'bandeau_choix_option' => 'Вибір варіанту зображення',
	'bandeau_choix_position' => 'Положення зображення на банері',
	'bandeau_option3' => 'Для варіанта 3',
	'bandeau_option3_aucune' => 'Немає зображення для заміни',
	'bandeau_option3_explication1' => 'Ваше зображення має бути розміщене в папці <strong>/squelettes/images/bandeau</strong><br>.
		Це зображення буде «адаптивним». Всі зображення .jpg, .png .webp та .gif у цьому каталозі будуть перелічені<br>
		Ви навіть побачите свій живий банер! ',
	'bandeau_option3_explication2' => 'А якщо ваш хостинг не дозволяє вам перелічити наявні файли, ви можете вказати тут шлях до файлу зображення.<br>Приклад: <strong>images/bandeau/bandeau.jpg</strong>, якщо файл <strong>bandeau.jpg</strong> знаходиться в <strong>squelettes/images/bandeau</strong>',
	'bandeau_option3_explication3' => 'Якщо у вашій папці <strong>/squelettes/images/bandeau</strong> знаходиться зображення з ім’ям <strong>rubriqueXX.jpg</strong>, де XX — номер рубрики, то ця рубрика, її підрубрики та їх статті автоматично відобразять цей банер.
		Інші відображатимуть банер, визначений вище.',
	'bandeau_option3_explication3titre' => 'Банер по сектору?<br><br>',
	'bandeau_option3_explication4' => 'Якщо ваша папка /squelettes/images/bandeau містить зображення з ім’ям
		<ul>
			<li>hiver.jpg або hiver.png - "зима"</li>
			<li>printemps.jpg або printemps.png - "весна"</li>
			<li>été.jpg або été.png - "літо"</li>
			<li>automne.jpg або automne.png - "осінь"</li>
		</ul>ці зображення автоматично відображатимуться під час відповідного сезону.',
	'bandeau_option3_explication4titre' => 'Банер по сезону?<br><br>',
	'bandeau_texte' => 'Вибір текстів',
	'bandeau_texte_descriptif' => '<strong>Опис</strong><br> сайту',
	'bandeau_texte_nom' => '<strong>Назва</strong><br> цього сайту',
	'bandeau_texte_slogan' => '<strong>Слоган</strong><br> цього сайту',
	'bandeau_texte_taille' => 'Розмір символу (у пікселях)',
	'barre_outils_dessous' => 'Під банером',
	'barre_outils_dessus' => 'Над банером',
	'barre_outils_explication' => 'Ця опція автоматично вимикається на екранах шириною менше 640 пікселів.',
	'barre_outils_fixer' => 'Якщо ви вирішили розмістити панель інструментів над банером, чи хочете ви закріпити її вгорі сторінки?',
	'barre_outils_identification' => 'Відображення простої ідентифікаційної форми',
	'barre_outils_place' => 'Місце панелі інструментів (проста форма ідентифікації, мовне меню, форма пошуку, кнопка-посилання на форум сайту)',
	'bas' => 'Вниз',
	'bienvenue' => 'Вітаємо, ',
	'blanc' => 'білий',
	'bloc_choix' => 'Вибір блоків',
	'bloc_cinq' => 'П’ятий блок',
	'bloc_deux' => 'Другий блок',
	'bloc_dix' => 'Десятий блок',
	'bloc_huit' => 'Восьмий блок',
	'bloc_neuf' => 'Дев’ятий блок',
	'bloc_parametrage' => 'Параметри блоків',
	'bloc_quatre' => 'Четвертий блок',
	'bloc_sept' => 'Сьомий блок',
	'bloc_six' => 'Шостий блок',
	'bloc_trois' => 'Третій блок',
	'bloc_un' => 'Перший блок',
	'blocs_disponibles' => 'Доступні блоки',
	'bord_pages' => 'Бічні краї сторінок',
	'bord_pages_couleur' => 'Колір лінії або тіні',
	'bord_pages_decalage' => 'Ширина обведення або зміщення тіні (у пікселях)',
	'bord_pages_force' => 'Сила градієнта (у пікселях)',
	'bord_pages_label' => 'Дати перевагу',
	'bord_pages_nb' => 'Вкажіть 0, щоб лінія була без тіні.',
	'bords' => 'Границі',
	'bords1' => 'Пункти горизонтального меню',
	'bords2' => 'Сторінки статей та рубрик, форуми статей, сторінка контактів, каталог',
	'bords3' => 'Таблиці',
	'bords5' => 'Елементи горизонтального меню при наведенні, ідентифікація, пошук, подія календаря',
	'bords6' => 'Форум сайту',
	'bords_arrondis' => 'Границі та заокруглення',

	// C
	'cadres' => 'Рамки та картинки',
	'cadres_bords' => 'Краї блоків присутні',
	'cadres_images' => 'Ширина зображення',
	'cadres_images_largeur' => 'Максимальна ширина зображень (у пікселях) для статей, що використовуються у певних бічних блоках: Прямий доступ (Accès direct), Edito, Випадкові фото, Безкоштовні статті з 1 по 5. ',
	'calendrier' => 'Календар',
	'centre' => 'У центрі',
	'cfg_page_accueil' => 'Головна',
	'cfg_page_arrondis' => 'Кути',
	'cfg_page_article' => 'Статті',
	'cfg_page_article_lateral' => 'Статті',
	'cfg_page_article_principal' => 'Статті',
	'cfg_page_articlepdf' => 'Article PDF',
	'cfg_page_autres_principal' => 'Інші сторінки',
	'cfg_page_bandeau' => 'Банер',
	'cfg_page_blocs_lateraux' => 'Бічні блоки',
	'cfg_page_bords' => 'Границі та заокруглення',
	'cfg_page_choix_blocs' => 'Вибір бічних блоків',
	'cfg_page_colonne_principale' => 'Основна колонка',
	'cfg_page_contact_principal' => 'Сторінка контактів',
	'cfg_page_deplier_replier' => 'Розгорнути та згорнути',
	'cfg_page_elements' => 'Елементи',
	'cfg_page_facebook' => 'Facebook',
	'cfg_page_fonds' => 'Фони',
	'cfg_page_forumsite' => 'Форум',
	'cfg_page_forumsite_lateral' => 'Форум',
	'cfg_page_forumsite_principal' => 'Форум',
	'cfg_page_galleria' => 'Галерея',
	'cfg_page_generalites' => 'Загальне',
	'cfg_page_layout' => 'Макет',
	'cfg_page_licence' => 'Ліцензія',
	'cfg_page_liens_sociaux' => 'Соціальні зв’язки',
	'cfg_page_mentions' => 'Юридичні згадки',
	'cfg_page_menuh' => 'Меню',
	'cfg_page_meta' => 'META-теги',
	'cfg_page_multilinguisme' => 'Багатомовність',
	'cfg_page_pages_noisettes' => 'Інші сторінки',
	'cfg_page_parametrage_blocs' => 'Параметри бічних блоків',
	'cfg_page_pied' => 'Підвал (footer)',
	'cfg_page_plugins' => 'Плагіни в Ескалі',
	'cfg_page_popup' => 'Модальне вікно',
	'cfg_page_qrcode' => 'QR код',
	'cfg_page_rainette' => 'Rainette (погода)',
	'cfg_page_rubrique' => 'Рубрики',
	'cfg_page_rubrique_lateral' => 'Рубрики',
	'cfg_page_rubrique_principal' => 'Рубрики',
	'cfg_page_shoutbox' => 'Рупор',
	'cfg_page_signalement' => 'Звіти (Signalement)',
	'cfg_page_socialtags' => 'Соціальні теги',
	'cfg_page_sommaire_colonnes' => 'Вибір бічних блоків',
	'cfg_page_sommaire_lateral' => 'Головна сторінка',
	'cfg_page_sommaire_noisettes' => 'Налаштування бічних блоків',
	'cfg_page_sommaire_principal' => 'Головна сторінка',
	'cfg_page_spip400' => 'Spip 400',
	'cfg_page_spipdf' => 'spiPDF',
	'cfg_page_style' => 'Інші стилі',
	'cfg_page_textes' => 'Тексти',
	'chercher_parmi_les_signataires' => 'Пошук серед підписантів',
	'choix_article' => 'Список статей',
	'choix_article_choix' => 'Вибір статей',
	'choix_article_choix_explication' => 'Номери статей через кому',
	'choix_article_colonne_explication' => 'Введіть число від 1 до 3',
	'choix_article_fond' => 'Фон статті',
	'choix_article_fond_explication' => 'або колір (ім’я, шістнадцятковий код або код RGB)
		<br>або градієнт (приклад: лінійний градієнт(#DAE6F6, #336699))
		<br>або зображення (приклад: url("squelettes/images/mon_image.jpg") без повтору).',
	'choix_article_taillelogo' => 'Максимальна ширина логотипу статті (у пікселях)',
	'choix_article_taillelogo_explication' => '80 за замовчуванням',
	'choix_blocs_lateraux' => 'Вибір бічних блоків',
	'choix_deux' => 'Два',
	'choix_groupe1' => 'Перша група ключових слів',
	'choix_groupe2' => 'Друга група ключових слів',
	'choix_groupe3' => 'Третя група ключових слів',
	'choix_trois' => 'Три',
	'choix_une' => 'Один',
	'citations' => 'Цитати',
	'clic_suite' => 'Натисніть, щоб прочитати більше',
	'collegues' => 'Всі колеги',
	'colonne_extra' => 'Колонка «Додатково» (за замовчуванням праворуч)',
	'colonne_laterale' => 'Бічна колонка',
	'colonne_nav' => 'Колонка «Навігація» (за замовчуванням ліворуч)',
	'commentaires' => 'коментарі',
	'configurer_escal' => 'Налаштувати ЕСКАЛЬ',
	'contact_activer_champ' => 'Активувати поле',
	'contact_alerte_entete' => 'Введений текст містить помилки!',
	'contact_alerte_mail' => 'Ваша адреса email неправильна.',
	'contact_alerte_message' => 'Ви не написали повідомлення! За неуважністю?',
	'contact_bienvenue' => 'Для зв’язку зі службою технічної підтримки <br>заповніть, будь ласка, всі поля форми.',
	'contact_cases' => 'Прапорці (“checkbox”)',
	'contact_cases_explication' => '(якщо ви помітите так, не забудьте заповнити хоча б одну мітку, інакше видасть помилку)',
	'contact_champ_mail' => 'Поле адреси електронної пошти відправника',
	'contact_destinataire' => 'Адреса одержувача (за замовчуванням веб-майстра)<br>
		Щоб ввести кілька адрес, розділіть їх комою <br>
		Приклад: tata@yoyo.fr, titi@paris.fr, toto@blague.fr',
	'contact_envoyer' => 'Надіслати',
	'contact_expediteur' => 'Текстове поле для адреси електронної пошти',
	'contact_explication' => 'Всі поля цієї форми допускають багатомовність за допомогою тега <multi><br>приклад: <multi>[fr]Приклад [en]Приклад [es]Ejemplo </multi><br>Щоб змінити текст іншого елемента сторінки контактів, використовуйте мовні файли.<br><a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip .php?article152">Докладніше</a>',
	'contact_libelle' => 'Мітка',
	'contact_libelle1' => 'Box label 1',
	'contact_libelle2' => 'Box label 2',
	'contact_libelle3' => 'Box label 3',
	'contact_libelle4' => 'Box label 4',
	'contact_libelle5' => 'Box label 5',
	'contact_libelle_explication' => 'Якщо залишити мітку порожньою, відповідний елемент не відображатиметься.',
	'contact_libelle_gen' => 'Спільна мітка (без апострофа та лапок)',
	'contact_ligne' => 'У мережі',
	'contact_liste' => 'У списку',
	'contact_mail' => 'Ваша адреса email:',
	'contact_message' => 'Ваше повідомлення:',
	'contact_motif' => 'Тема вашого повідомлення:',
	'contact_motif1' => 'Інформація',
	'contact_motif2' => 'Реєстрація',
	'contact_motif3' => 'Технічна проблема',
	'contact_motif4' => 'Ваша думка про сайт',
	'contact_motif5' => 'Інше',
	'contact_motif_message' => 'Тема повідомлення (перемикачі)',
	'contact_nom' => 'Ваше прізвище:',
	'contact_obli' => 'Обов’язкове поле',
	'contact_prenom' => 'Ваше ім’я:',
	'contact_presentation' => 'Презентація',
	'contact_retour' => 'Підтвердження повідомлення',
	'contact_retour_commentaire' => 'Ваше повідомлення успішно надіслано веб-майстру сайту, і він відповість вам найближчим часом за цією адресою:',
	'contact_retour_explication' => 'Ви можете налаштувати повідомлення, яке відображатиметься користувачеві після надсилання форми. За вашим повідомленням слідуватиме адреса користувача.',
	'contact_sup' => 'Додаємо додаткове поле',
	'contact_sup1' => 'Додаткове текстове поле 1',
	'contact_sup2' => 'Додаткове текстове поле 2',
	'contact_texte_accueil' => 'Текст, який буде відображатися вгорі сторінки',
	'contact_texte_accueil_explication' => 'замість<br>
		«Щоб зв’язатися з технічним референтом, будь ласка, заповніть усі поля цієї форми».<br>
		(Щоб нічого не відображати, просто введіть пробіл)',
	'contenu_site' => 'Цей сайт містить: ',
	'copyright' => 'Всі права захищені',

	// D
	'dans_site' => 'на цьому сайті',
	'deplier_replier' => 'Згорнути і розгорнути',
	'deplier_replier_explication' => 'Якщо ви виберете «Так», блок буде складено, а кнопка дозволить розгорнути та знову скласти його.',
	'derniers_articles_bis' => 'Нові статті 2',
	'derniers_articles_syndiques' => 'Останні імпортовані статті',
	'derniers_articles_ter' => 'Нові статті 3',
	'destinataire' => 'Отримувач',
	'details' => 'Більше деталей',
	'doc_a_decouvrir' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article180&lang=fr" title="Дивитись документацію">Пошук</a>',
	'doc_acces_direct' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article24&lang=fr" title="Дивитись документацію">Прямий доступ</a>',
	'doc_actus' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article130&lang=fr" title="Дивитись документацію">Новини</a>',
	'doc_annonce' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article174&lang=fr" title="Дивитись документацію">Оголошення</a>',
	'doc_annonce_defil' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article179&lang=fr" title="Дивитись документацію">Прокручування реклами</a>',
	'doc_annuaire_auteurs' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article201&lang=fr" title="Дивитись документацію">Каталог авторів</a>',
	'doc_articles_plus_vus' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article78&lang=fr" title="Дивитись документацію">Найпопулярніші статті</a>',
	'doc_articles_rubrique' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article279&lang=fr" title="Дивитись документацію">Статті рубріки</a>',
	'doc_choix_blocs' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article174" title="Переглянути документацію">Оголошення</a>: найновіша стаття з ключовим словом "annonce" на вкладці.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article179" title="Переглянути документацію">Оголошення, що прокручуються</a>: усі статті з ключовим слово "annonce-defilant" будуть прокручуватись у вкладці.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article5" title="Переглянути документацію">Головна (“A la une”)</a>: вкладка для останніх статей на сайті, карти сайту, окремої статті, статтей з рубрики...',
	'doc_derniers_art_syndic' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article291&lang=fr" title="Дивитись документацію">Derniers articles syndiqués</a>',
	'doc_derniers_articles' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article26&lang=fr" title="Дивитись документацію">Нові статті</a>',
	'doc_derniers_articles2' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="Дивитись документацію">Найновіші статті</a>',
	'doc_derniers_comments' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article178&lang=fr" title="Дивитись документацію"> Derniers commentaires</a>',
	'doc_edito' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article23&lang=fr" title="Дивитись документацію">Edito</a>',
	'doc_evenements_a_venir' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article77&lang=fr" title="Дивитись документацію">Найближчі події</a>',
	'doc_identification' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article27&lang=fr" title="Дивитись документацію">Ідентифікація</a>',
	'doc_meme_rubrique' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article28&lang=fr" title="Дивитись документацію">У цій же рубриці</a>',
	'doc_menuv1' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article102&lang=fr" title="Дивитись документацію">Вертикальне розкладне меню</a>',
	'doc_menuv2' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article112&lang=fr" title="Дивитись документацію">Вертикальне розкладне меню праворуч</a>',
	'doc_mini_calendrier' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article81&lang=fr" title="Дивитись документацію">Mini calendrier</a>',
	'doc_mot_cles_associes' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article131&lang=fr" title="Дивитись документацію">Пов’язані ключові слова</a>',
	'doc_navigation_mots_cles' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article312&lang=fr" title="Дивитись документацію">Навігація за ключовими словами</a>',
	'doc_noisettes' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24" title="Переглянути документацію">Прямий доступ</a>: стаття з ключовим словом "access-direct" для внутрішніх посилань. Заголовком блоку буде заголовок статті.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article130" title="Переглянути документацію">Новини</a>: статті з ключовим словом "actus".
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article201" title="Переглянути документацію">Каталог авторів</a>: список авторів та їх статуси.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article279" title="Переглянути документацію">Статті рубрики</a>: відображає статті рубрики з ключовим словом "articles-of-section".
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article78" title="Переглянути документацію">Найпопулярніші статті</a>: відображення найбільш відвідуваних статті.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article200" title="Переглянути документацію">Вільні статті 1–5</a>: стаття з ключовим словом "article-libreN" (N = від 1 до 5). Заголовком блоку буде заголовок статті.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article180" title="Переглянути документацію">Щоб дізнатися</a>: вибір статті сайту або поточної рубрики та її підрубрик.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article191" title="Переглянути документацію">Для завантаження</a>: список документів рубрики (а не її статей) або статті залежно від контексту.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article190" title="Переглянути документацію">Спеціальні налаштування</a>: статті з ключовим словом “special”. Конфігурація цього блоку виконується в «Налаштування бічних блоків -> Заголовки та вміст».
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article28" title="Переглянути документацію">У цій рубриці</a>: нові статті з тієї ж рубрики, що й поточна стаття.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="Переглянути документацію">Нові статті</a>: нові статті з поточного сайту або рубрики та її підрубрик.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article291" title="Переглянути документацію">Останні синдиковані статті</a>: останні статті з синдикованих сайтів із зазначенням їх джерела.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article178" title="Переглянути документацію">Останні коментарі</a>: останні коментарі з усього сайту.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23" title="Переглянути документацію">Редакційна стаття</a>: стаття з ключовим словом “edito”. Заголовком блоку буде заголовок статті.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article77" title="Переглянути документацію">Майбутні події</a>: список подій, що плануються.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article27" title="Переглянути документацію">Ідентифікація</a>: форма для входу.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article81" title="Переглянути документацію">Міні-календар</a>: використовує плагін «Міні-календар» автоматично активований за допомогою Escal.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article131" title="Переглянути документацію">Пов’язані ключові слова</a>: навігація за ключовими словами , якщо в статті є хоча б одне.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article312" title="Переглянути документацію">Перегляд за ключовими словами</a>: меню навігації за допомогою груп ключових слів .
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article144" title="Переглянути документацію">Випадкові фотографії</a>: статті, у яких є зображення і ключове слово "photo-one" прокрутяться.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article192" title="Переглянути документацію">Погода</a>: потрібен плагін “Rainette”. Інформація про погоду для обраного вами міста.<br>Налаштування цього блоку здійснюється в розділі «Плагіни в Escal».
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article198" title="Переглянути документацію">Пошук за багатьма критеріями</a>: пошук за групами ключових слів і за ключовими словами.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article181" title="Переглянути документацію">Улюблені сайти</a>: мініатюри сайтів посилання на які є в секторі, де ми знаходимося, з ключовим словом "favori".
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article126" title="Переглянути документацію">Статистика</a>: уся статистика сайту.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article82" title="Переглянути документацію">В Інтернеті</a>: назва та останні статті синдиковані сайти, відсортовані за сайтом-джерелом.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article197" title="Переглянути документацію">Відео</a>: відображає відео у форматі flv або mp4 із статей із ключовим словом «video-une».',
	'doc_noisettes2' => 'Деякі блоки доступні лише на певних сторінках.',
	'doc_personnalise' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article190&lang=fr" title="Дивитись документацію">Bloc à personnaliser</a>',
	'doc_photos_hasard' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article144&lang=fr" title="Дивитись документацію">Випадкові фото/a>',
	'doc_rainette' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article192&lang=fr" title="Дивитись документацію">Погода</a>',
	'doc_recherche_multi' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article198&lang=fr" title="Дивитись документацію">Багатокритеріальний пошук</a>',
	'doc_site_escal' => '<a class="doc-escal" href="http://escal.ac-lyon.fr" title="Дивитись документацію">Документація: шаблон ESCAL</a>',
	'doc_sites_favoris' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article181&lang=fr" title="Дивитись документацію">Улюблені сайти</a>',
	'doc_stats' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article126&lang=fr" title="Дивитись документацію">Статистика</a>',
	'doc_sur_web' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article82&lang=fr" title="Дивитись документацію">В Інтернеті</a>',
	'doc_telecharger_art_rub' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article191&lang=fr" title="Дивитись документацію">Завантажити</a>',
	'doc_videos' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article197&lang=fr" title="Дивитись документацію">Відео</a>',
	'document_trouve' => 'документ знайдено',
	'documentation' => 'Документація',
	'documentation_voir' => 'Дивіться документацію',
	'documents' => 'Документи',
	'documents_trouves' => 'знайдено документів',
	'droite' => 'Праворуч',

	// E
	'edito' => 'Редакційна стаття (“Édito”)',
	'elements' => 'Елементи',
	'envoi_mail_message' => 'Повідомлення: ',
	'envoi_mail_motif' => 'Тема: ',
	'envoi_mail_nom' => 'Прізвище: ',
	'envoi_mail_prenom' => 'Ім’я: ',
	'envoyer_message' => 'Надіслати повідомлення для ',
	'erreur401' => 'Помилка 401',
	'erreur401_message' => 'У вас недостатньо прав для доступу до запитаної сторінки або документа... <br>
		Щоб отримати доступ, зверніться до веб-майстра сайту зі сторінки [Контактів|Page contact->@url@] цього сайту.',
	'erreur404' => 'Помилка 404',
	'erreur404_message' => 'Сторінка, яку ви шукаєте, або переміщена, або відсутня. Якщо це здається вам помилкою, 
		напишіть про це веб-майстру за допомогою [сторінки контактів|Contact page ->@url@] цього сайту.',
	'escal' => 'Escal',
	'evenement_associe' => 'Пов’язана подія:',
	'evenements_ajouter' => 'Додати захід<br>(Доступ обмежений)',
	'evenements_associes' => 'Супутні події:',
	'evenements_non' => 'Не очікується подій',
	'evenements_venir' => 'Найближчі події',

	// F
	'facebook_bouton' => 'Зовнішній вигляд кнопки',
	'facebook_explication' => 'Якщо ви активували плагін <a class="spip_out" href="http://contrib.spip.net/Modeles-Facebook" title="Documentation">"Моделі Facebook"</a>, напис «Мені подобається» з’явиться у нижньому колонтитулі.
		<br>Не забудьте налаштувати цей плагін, вказавши URL-адресу вашої сторінки або профілю Facebook.',
	'facebook_partager' => 'Кнопка «Поділитися»',
	'facebook_plugin' => 'Шаблони Facebook',
	'favicon' => 'Favicon',
	'favicon_choix1' => 'з файлу favicon.ico.<br>За замовчуванням - Escal, або ваш власний, помістити в папку /squelettes(у корені). ',
	'favicon_choix2' => 'з логотипу вашого сайту.<br>Escal його автоматично перетворює (розмір, формат).',
	'favicon_choix3' => 'із зображення на ваш вибір (gif, png, jpg...), поміщеного в папку /squelettes.',
	'favicon_choix_fichier' => 'Нижче необхідно вказати ім’я та розширення файлу.',
	'fleche' => 'стрілка',
	'fois' => 'раз',
	'fonds_couleurs' => '<strong>Кольори</strong>: Ви можете вказати
		<ul>
			<li>колір у шістнадцятковому коді → приклад: #0000FF </li>
			<li>іменований колір → приклад: grey </li>
			<li>a колір у коді RGB → приклад: rgb(24,125,255) </li>
			<li>колір у коді RGB з прозорістю → приклад: rgba(24,125,255,0.5) </li>
			<li>або будь-який інший колірний код: HSL, HSB , CMYK ... </li>
			<li>або «прозорий» для видалення кольору</li>
			</ul>',
	'fonds_noisettes' => 'Фони та тексти в блоках',
	'fonds_noisettes_annonce' => 'Оголошення в одному<br>Виділення шуканих слів',
	'fonds_noisettes_apercu' => 'Попередній перегляд тексту',
	'fonds_noisettes_articles' => 'Статті<br>Форум статей<br>Порядок денний<br>',
	'fonds_noisettes_bandeau' => 'Банер',
	'fonds_noisettes_boutons' => 'Кнопки ідентифікації та пошуку<br>
		Форма зворотнього зв’язку<br>
		Парні рядки таблиць
		Портфоліо<br>
		Постскриптум<br>
		Форум сайту,...',
	'fonds_noisettes_contenu' => 'Вміст сторінки',
	'fonds_noisettes_contenu_transparent' => 'Ви також можете написати прозорий, що буде зручно з фоновим зображенням.',
	'fonds_noisettes_derniers_articles' => 'Список останніх статей на Головній',
	'fonds_noisettes_fond' => 'Фон:',
	'fonds_noisettes_lien' => 'Колір посилань: ',
	'fonds_noisettes_lien_survol' => 'Колір посилань при наведенні: ',
	'fonds_noisettes_menuh' => 'Горизонтальне меню<br>
		Посилання при наведенні у вертикальному меню, що випадає справа<br>
		Вставка заголовка для рубрик та статей<br>
		План<br>
		Шапки форуму',
	'fonds_noisettes_onglets_actifs' => 'Активна вкладка головної сторінки',
	'fonds_noisettes_onglets_inactifs' => 'Вибрані вкладки, якщо вони неактивні',
	'fonds_noisettes_pied' => 'Футер',
	'fonds_noisettes_rubriques' => 'Активний пункт горизонтального меню<br>
		Непарні рядки таблиць<br>
		попередній перегляд форумів,...',
	'fonds_noisettes_survol' => 'Пункти меню при наведенні, вибрані елементи блоків, хлібні крихти...',
	'fonds_noisettes_texte' => 'Колір тексту:',
	'fonds_noisettes_textes' => 'Текст бічних блоків',
	'fonds_noisettes_titres' => 'Заголовок та краї бічних блоків<br>Вертикальне спливаюче меню<br>Заголовок таблиць,...',
	'fonds_site' => 'Фон для сайту',
	'fonds_site_aucune_image' => 'Немає фонового зображення',
	'fonds_site_couleur' => 'Колір фону сайту',
	'fonds_site_couleur_defaut' => '(за замовчуванням: #DFDFDF)',
	'fonds_site_explication1' => 'Ваші фони повинні бути розміщені в <strong>/squelettes/images/fonds</strong> <br>
		Усі зображення .jpg, .png та .gif у цьому каталозі будуть перераховані. <br>
		Ви навіть зможете попередньо переглянути свій живий фон!',
	'fonds_site_explication2' => 'А якщо ваш хостинг не дозволяє вам перелічувати наявні файли, ви можете вказати тут шлях до файлу зображення.<br>
		Приклад: <strong>images/fonds/fond.jpg</strong>, якщо файл <strong>fond.jpg</strong> знаходиться в <strong>squelettes/images/fonds</strong>',
	'fonds_site_explication3' => 'За замовчуванням фонове зображення повторюється по горизонталі та/або вертикалі,
		що добре, наприклад, для градієнтного фону. Але якщо у вас є зображення, яке не повинно повторюватися, позначте це нижче,
		і ваше зображення адаптується до екрана користувача та залишиться зафіксованим. <br><br>
		– Рекомендований розмір для більшості екранів: 2000 на 1300 пікселів.<br>
		– Зображення має бути оптимізовано, щоб не уповільнювати завантаження.<br>
		Див. <a class="spip_out" href="http://www.alsacreations.com/astuce/lire/1216-arriere-plan-extensible-background.html" title="Докладніше">Розширюваний фон</a> ',
	'fonds_site_image' => 'Фонове зображення',
	'fonds_site_imagefondunique' => 'Підігнати фон під екран',
	'fonds_textes' => 'Інші тексти',
	'fonds_textes_alerte' => 'Оповіщення на сторінці контактів',
	'fonds_textes_comment1' => '(у разі порожнього поля, наприклад)',
	'fonds_textes_comment2' => 'ідентифікація, вхід, інформація, реєстрація...',
	'fonds_textes_couleur' => 'Області введення SPIP',
	'fonds_textes_explication' => 'Вам все одно доведеться вибирати кольори для деяких конкретних текстів...',
	'fonds_textes_input' => 'Області введення SPIP',
	'fonds_textes_liens' => 'Посилання',
	'fonds_textes_liens_survol' => 'Посилання при наведенніі',
	'fonds_textes_liens_vus' => 'Відвідані посилання',
	'fonds_textes_texte' => 'Попередній перегляд',
	'form_pet_envoi_mail_confirmation' => 'Лист-підтвердження (e-mail) було надіслано веб-майстру сайту для підтвердження вашої реєстрації.',
	'form_recherche' => 'Форма пошуку',
	'form_recherche_item1' => 'На панелі інструментів',
	'form_recherche_item2' => 'Лівий стовпець',
	'form_recherche_item3' => 'Правий стовпець',
	'form_recherche_item4' => 'Ніде',
	'form_recherche_item5' => 'У горизонтальному рядку меню',
	'form_recherche_place' => 'Місце форми',
	'format' => 'Тип файлу:',
	'forum' => 'Форум',
	'forum_site_avant' => 'Перш ніж почати',
	'forum_site_derniers' => 'Останні<br>повідомлення',
	'forum_site_reponses' => 'Відповіді',
	'forum_site_sujets' => 'теми',
	'forum_trouve' => 'знайдено повідомлення форумa',
	'forums' => 'Повідомлення на форумах',
	'forums_trouves' => 'знайдені повідомлення форуму',

	// G
	'galleria_caracteres' => 'Розмір шрифту заголовка зображення',
	'galleria_couleur_encart' => 'Колір фону інформаційного вікна',
	'galleria_couleur_gallerie' => 'Колір фону галереї',
	'galleria_couleur_texte' => 'Колір тексту інформаційного поля',
	'galleria_explication' => 'Якщо ви активували <a class="spip_out" href="http://contrib.spip.net/Galleria-fr" title="Documentation">плагін "Galleria"</a>, ви можете налаштувати його тут.',
	'galleria_plugin' => 'Галерея',
	'gauche' => 'Зліва',
	'gros_jour' => 'Найважливіший день:',
	'groupe_affichage' => 'Група технічних ключових слів, що використовуються в плагіні Escal.',
	'groupe_agenda_couleur' => 'Група ключових слів для кольору подій календаря в плагіні Escal',

	// H
	'haut' => 'Вгору',
	'haut_page' => 'На початок сторінки ⇑',
	'hebergeur' => 'Хост:',

	// I
	'identification' => 'Ідентифікація',
	'indentation' => 'Відступ першого рядка абзаців (у пікселях)',
	'info_lien_message' => '<strong>Увага! </strong>Якщо ваше повідомлення містить посилання, воно має бути перевірено веб-майстром. Не треба намагатися це ще раз опублікувати ;-)',
	'infos' => 'Інформація',

	// L
	'layout' => 'Макет',
	'layout_choix' => 'Вибір компонування',
	'layout_documentation' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article1" title="Див. документацію"><strong>Документація </strong></a>',
	'layout_explication' => 'Будьте обережні з вибором ширини: зверніть увагу, що текст із занадто довгими рядками стає важким для читання.',
	'layout_fixe' => '<strong>Фіксований макет</strong><br>за замовчанням: макет 1200 пікселів та бічні колонки 250 пікселів.',
	'layout_fixe_largeur' => 'Ширина макета (у пікселях)',
	'layout_fixe_si' => '<strong>фіксований макет</strong>',
	'layout_fluide' => '<strong>Гнучкий макет</strong><br>адаптується до ширини екрана (рекомендується)',
	'layout_fluide_largeur' => 'Максимальна ширина макета (у пікселях)',
	'layout_fluide_si' => '<strong>текучий макет</strong>',
	'layout_largeur' => 'Регулювання ширини',
	'layout_largeur_colonnes' => 'Ширина фіксованих бічних колонок (у пікселях)',
	'layout_mixte' => '<strong>Змішане компонування</strong><br> гнучка центральна частина та фіксовані бічні колонки',
	'layout_mixte_si' => '<strong>змішаний макет</strong>',
	'le' => ' ',
	'licence_explication' => 'Якщо ви активували плагін <a class="spip_out" href="http://contrib.spip.net/Une-licence-pour-un-article" title="Documentation">"License"</a>, вам більше нема чого робити, угорі статті автоматично з’явиться значок, відповідний ліцензії, яку ви вибрали для статті.',
	'lien_agenda' => '► переглянути повну сторінку',
	'lien_agenda_title' => 'Відобразити календар на всю сторінку',
	'lien_rapide_pied' => 'Перейти у футер',
	'liens_sociaux_espace' => 'Показувати соціальні посилання на панелі інструментів',
	'liens_sociaux_explication' => 'Якщо ви активували <a class="spip_out" href="https://contrib.spip.net/Liens-sociaux" title="Documentation">плагін «Соціальні посилання»</a> ("" Liens sociaux"), ви можете налаштувати його тут.',
	'liens_sociaux_pied' => 'Показувати соціальні посилання у футері',
	'liens_sociaux_plugin' => 'Соціальні зв’язки',
	'liens_sociaux_texte' => 'Можливий текст для відображення:',
	'lire_article' => 'Читати статтю...',
	'lire_suite' => 'Докладніше...',
	'lire_tout' => 'Прочитати всю статтю',

	// M
	'mentions' => 'Юридичні повідомлення',
	'mentions_donnees' => 'Персональні дані та файли cookie',
	'mentions_explication' => 'Якщо ви активували <a class="spip_out" href="http://contrib.spip.net/Mentions-Legales" title="Documentation">плагін «Юридичні повідомлення»</a>, ви Ви можете відобразити посилання на спеціальну сторінку в нижньому колонтитулі сайту.',
	'mentions_texte' => '<!-- Текст, який потрібно змінити, заповнивши data_to_replace:
		###name_of_the_director_of_publication###
		###postal_address### (2 екземпляри)
		###name_of_host###
		###link_to_the_website_of_the_host###
		###email_of_the_host# # #
		-->

		Відображення юридичних повідомлень є зобов’язанням як для професійних, так і для особистих веб-сайтів відповідно до закону від 21 червня 2004 року про довіру в цифровій економіці (LCEN). Відповідно до цього закону тут вказані контактні дані видавця та постачальника послуг, що розміщує сайт @url_du_site@

		<hr>

		{{{Власник/Видавець/Директор видання}}}

		@url_du_site@ — це веб-сайт, що редагується {{@nom_du_site@}} і публікується під керівництвом ###nom_du_directeur_de_publication###. Весь сайт @url_du_site@, його сторінки та контент є власністю {{@nom_du_site@}}.
		Адміністративний контакт для сайту @url_du_site@:
		###postal_address###
		[->@courriel_de_contact@]

		{{{Host}}}

		@url_du_site@ розміщується на хості ###name_of_the_host###.
		Для отримання додаткової інформації відвідайте сайт [->###lien_vers_le_site_de_l_hébergeur###]
		Щоб зв’язатися з ними, використовуйте адресу електронної пошти [->###courriel_de_l_hébergeur###]

		{{@nom_du_site@}} відмовляється від будь-якої відповідальності за будь-які перебої в роботі сайту @url_du_site@ та його сервісів.

		{{{Дизайнер/Творець}}}

		Весь сайт {{@nom_du_site@}}: дизайн, графічний статут та додатки було створено та розроблено {{@nom_du_site@}} за допомогою [SPIP->https://www.spip.net] під скіном [Escal-> https ://escal.edu.ac-lyon.fr].
		Щоб зв’язатися з ними, використовуйте адресу електронної пошти: [->@courriel_de_contact@]

		{{{Functionnement du site}}}

		Сторінки та контент цього сайту створені за допомогою безкоштовного програмного забезпечення SPIP, яке розповсюджується за ліцензією GNU/GPL (Загальна ліцензія). громадський). Ви можете вільно використовувати його для створення власного веб-сайту. Для отримання додаткової інформації завітайте на сайт [spip.net->https://www.spip.net].

		SPIP, система публікацій в Інтернеті.
		Авторські права © 2001–2018, Арно Мартен, Антуан Пітру, Філіп Рів’єр та Еммануель Сен-Джеймс.

		{{{Зміст та права на відтворення}}}

		Відповідно до статей L. 111-1 та L. 123-1 Кодексу інтелектуальної власності, весь контент на цьому сайті (тексти, зображення, відео та будь-які медіафайли в цілому), якщо інше прямо не вказано інше, якщо не вказано інше, захищено авторським правом. Відтворення, навіть часткове, вмісту сторінок даного сайту без попередньої згоди @url_du_site@ суворо заборонено.

		{{{Визнання факту}}}

		Використовуючи @url_du_site@, користувач Інтернету приймає до відома та приймає перелічені тут умови

		{{{Зловживання}}}

		Відповідно до Закону № 2004-575 від 21 червня 2004 р. про довіру в цифровій економіці, щоб повідомити про спірний зміст або якщо ви стали жертвою шахрайського використання сайту @url_du_site@, зв’яжіться з адміністратором сайту за адресою: адреса електронної пошти: [->@courriel_de_contact@]

		{{{Збір та обробка персональних даних}}}

		Будь-яка інформація, зібрана @url_du_site@, ніколи не передається третім особам. За винятком особливих випадків, ця інформація надходить у результаті добровільної реєстрації адреси електронної пошти, наданої користувачем Інтернету, що дозволяє йому отримувати документацію або інформаційний бюлетень, вимагати реєстрацію в редакції сайту або запитувати про будь-який пункт.
		Відповідно до закону № 78-17 від 6 січня 1978 р. про обробку даних, файли та свободи користувач Інтернету має право на доступ, виправлення та видалення особистої інформації, що стосується його, що зберігається на @url_du_site@, яку він можете займатися у будь-який час. у адміністратора @url_du_site@. Для запиту цього відправте лист за адресою
		###address_postale###
		або зв’яжіться за адресою електронної пошти: [->@courriel_de_contact@]

		{{{Cookies}}}

		Певна особиста інформація та певні маркери cookie можуть бути записані @url_du_site@ на персональному комп’ютері користувача без вираження його побажань («cookies» або «аплети JAVA»).
		Якщо явно не зазначено інше, ці технічні процеси не є необхідними для правильного функціонування @url_du_site@, і їх використання, звичайно, може бути деактивовано у браузері, який використовується користувачем Інтернету, без шкоди для перегляду.
		Ця інформація не зберігається на сайті після відключення користувача від Інтернету.
		Тут корисно вказати, що для доступу до різних областей сайту може знадобитися згода користувача Інтернету на зберігання файлу cookie на його персональному комп’ютері з метою безпеки.

		{{{Обмеження відповідальності}}}

		Сайт @url_du_site@ містить інформацію, надану сторонніми компаніями, або гіпертекстові посилання на інші сайти або інші зовнішні джерела, які не були розроблені {{@nom_du_site@}}.
		Поведінка цільових сайтів, можливо, шкідлива, не може бути притягнута до відповідальності {{@nom_du_site@}}.
		У більш загальному сенсі контент, представлений на цьому сайті, призначений для інформаційних цілей.
		Користувач Інтернету повинен скористатися цією інформацією з розумінням.
		{{@nom_du_site@}} не несе відповідальності за інформацію, думки та рекомендації третіх осіб.
		Оскільки @url_du_site@ не може контролювати ці сайти або зовнішні джерела, {{@nom_du_site@}} не може нести відповідальності за надання цих сайтів або зовнішніх джерел і не може нести жодної відповідальності за контент, рекламу, продукти, послуги або інші матеріали. доступні на таких сайтах або із зовнішніх джерел або з них.
		Крім того, {{@nom_du_site@}} не може нести відповідальності за будь-які доведені або передбачувані збитки або збитки, що виникли в результаті або у зв’язку з використанням або фактом довіри до контенту, товарів або послуг, доступних на цих зовнішніх сайтах або джерелах.',
	'menu' => 'У меню',
	'menu_horizontal' => 'Горизонтальне меню',
	'menu_horizontal_accueil' => 'Посилання на Головну сторінку',
	'menu_horizontal_affichage' => 'Показати меню горизонтально',
	'menu_horizontal_choix' => 'Вибір горизонтального меню',
	'menu_horizontal_choix_bloc' => 'Мега-меню, що розкривається ',
	'menu_horizontal_choix_liste' => 'Списки, що розкриваються',
	'menu_horizontal_fixer' => 'Виправити меню вгорі сторінки під час прокручування вниз',
	'menu_horizontal_hauteur' => 'Висота логотипів рубрик (у пікселях)',
	'menu_horizontal_hauteur_explication' => 'Переконайтеся, що висота ваших логотипів принаймні дорівнює вибраному значенню.',
	'menu_horizontal_logo' => 'Показати логотип рубрики, якщо він існує',
	'menu_horizontal_logo_accueil' => 'Показати логотип',
	'menu_horizontal_notice' => '<strong>Увага</strong>: не встановлюйте для обох значень «ні», щоб не отримати порожні записи меню.',
	'menu_horizontal_secteur' => 'Для заголовків першого рівня (= сектор)',
	'menu_horizontal_secteur_explication' => '→ також можна застосувати до меню для мобільних',
	'menu_horizontal_titre' => 'Показати назву рубрики (якщо в ній опубліковано хоча б одну статтю)',
	'menu_horizontal_titre_accueil' => 'Показати слово «Головна»',
	'menu_vertical_depliant' => 'Відкривається вертикальне меню',
	'menu_vertical_deroulant' => 'Вертикальне меню, що випадає праворуч',
	'metadonnees' => 'Метадані',
	'metadonnees_explication' => '<ul>Якщо ви активували плагін <a class="spip_out" href="https://contrib.spip.net/Metadonnees-Photo" title="Documentation">Метадані</a>, ви можете вибрати відображення
		<li>метаданих ваших фотографій</li>
		<li>або їх назв</li>
		<li>або те й інше</li>
		</ul>',
	'metadonnees_titre' => 'Назва та метадані',
	'mini_calendrier' => 'Міні-календар',
	'mot_acces_direct' => 'вибрати статтю, яка відображатиметься в блоці «Прямий доступ» (“Accès direct”)',
	'mot_accueil' => 'вибрати статтю для вкладки Головної сторінки',
	'mot_actus' => 'вибрати статті, які будуть відображатися в блоці «Новини» (“Actus”)',
	'mot_agenda' => 'вибрати статті або рубрику(и), статті яких відображатимуться на порядку денному',
	'mot_annonce' => 'вибрати статтю, текст якої відображатиметься в блоці «Анонс» (“Annonce”) Головна сторінка',
	'mot_annonce_defilant' => 'вибрати статті, текст яких буде відображатися в блоці «Прокрутка» (“Annonces défilantes”) на Головній сторінці',
	'mot_annuaire' => 'щоб вибрати статтю, яка буде використовуватися на сторінці annuaire.html',
	'mot_archive' => 'вибрати рубрику, з якої навмання взята стаття буде відображатися у вкладці «Архів статей» (“Article archive”) головної сторінки ',
	'mot_article_libre1' => 'оберіть статтю, вміст якої буде відображатися в блоці «Вільна стаття 1»',
	'mot_article_libre2' => 'оберіть статтю, вміст якої буде відображатися в блоці «Вільна стаття 2»',
	'mot_article_libre3' => 'оберіть статтю, вміст якої буде відображатися в блоці «Вільна стаття 3»',
	'mot_article_libre4' => 'оберіть статтю, вміст якої буде відображатися в блоці «Вільна стаття 4»',
	'mot_article_libre5' => 'оберіть статтю, вміст якої буде відображатися в блоці «Вільна стаття 5»',
	'mot_article_sans_date' => 'приховати відображення дат публікації та зміни статті',
	'mot_articles_rubrique' => 'вибрати рубрику, статті якої відображатимуться в блоці «Статті рубрики»',
	'mot_chrono' => 'відображення статей розділу в меню в зворотному хронологічному порядку, ця поведінка не передається дочірнім розділам',
	'mot_citations' => 'вибрати статтю, яка буде служити резервуаром для цитат у нижньому колонтитулі',
	'mot_edito' => 'вибрати статтю, яка буде відображатися в блоці “Edito” («Редакційна стаття»)',
	'mot_favori' => 'вибрати сайти, мініатюри яких будуть відображатися в блоці «Улюблені сайти».',
	'mot_forum' => 'вибрати сектор, який буде використовуватися для форуму сайту',
	'mot_invisible' => 'приховати рубрику та її підрубрики з усіх меню, карти сайту та останніх статей',
	'mot_mentions' => 'вибрати статтю, яка відображатиметься як юридичні повідомлення',
	'mot_mon_article' => 'вибрати статтю, яка відображатиметься у вкладці центрального блоку Головної сторінки',
	'mot_mon_article2' => 'вибрати другу статтю, яка відображатиметься у вкладці центрального блоку Головної сторінки',
	'mot_mon_article3' => 'вибрати третю статтю, яка відображатиметься у вкладці центрального блоку Головної сторінки',
	'mot_pas_decouvrir' => 'вибрати рубрики та статті, які слід виключити з відображення в блоці "Виявити" ("A decouvrir"), якщо ви вибрали "по всьому сайту" ',
	'mot_pas_menu' => 'щоб не відображати рубрику або статтю в горизонтальному меню',
	'mot_pas_menu_vertical' => 'не відображати рубрику або статтю у вертикальних меню',
	'mot_pas_plan' => 'не відображати рубрики (та її статті) або певні статті в блоці «Карта сайту» ("Plan du site") головної сторінки',
	'mot_pas_une' => 'щоб не відображати розділ (і його статті) або статті на вкладках «Останні статті» домашньої сторінки',
	'mot_photo_une' => 'вибрати статті, зображення яких будуть відображатися в блоці «Кілька випадкових зображень».',
	'mot_pleine_page' => 'вибрати статті, які будуть відображатися на всю сторінку без бокового блоку',
	'mot_popup' => 'вибрати статтю, яка буде відображатися в модальному вікні під час відкриття сайту (якщо активовано)',
	'mot_rubrique_onglet' => 'вибрати рубрику, яка відображатиметься у вкладках на Головній сторінці',
	'mot_rubrique_onglet2' => 'вибрати другу рубрику, яка відображатиметься у вкладках на Головній сторінці',
	'mot_rubrique_onglet3' => 'вибрати третю рубрику, яка відображатиметься у вкладках на головній сторінці',
	'mot_rubrique_onglet4' => 'вибрати четверту рубрику, яка відображатиметься у вкладках на головній сторінці',
	'mot_rubrique_onglet5' => 'вибрати п’яту рубрику, яка відображатиметься у вкладках на головній сторінці',
	'mot_site_exclu' => 'виключити сайти з блоку «В Інтернеті»',
	'mot_special' => 'вибрати рубрику і/або статті, які будуть відображатися в блоці для персоналізації',
	'mot_texte2colonnes' => 'Для відображення тексту статті у 2 стовпці на сторінці статті',
	'mot_video_une' => 'вибрати статті, відео яких відображатимуться в блоці «Відео»',
	'mots_clefs' => 'Ключі',
	'mots_clefs_associes' => 'Пов’язані ключі',
	'mots_cles' => 'ключи',
	'moyenne_jours' => 'дні:',
	'moyenne_visites' => 'У середньому',
	'multilinguisme_ariane' => 'Також видалити сектор у "Хлібних крихтах"',
	'multilinguisme_code' => 'Коди мов',
	'multilinguisme_drapeau' => 'Прапори',
	'multilinguisme_explication1' => 'За замовчуванням Escal відображає елементи 1-го рівня (= сектори) у меню.<br>
Щоб отримати однакову структуру кількома мовами, прийдеться дублювати усі сектори всіма мовами. Так з’явиться надто багато секторів.<br>
Тому ми пропонуємо віддати перевагу створенню окремого сектора для кожної мови, але в цьому випадку в меню з’явиться лише сектор мови.<br>
Таким чином, цей параметр дозволяє відображати в меню розділи другого рівня замість секторів.
Опція дійсна для горизонтального меню, а також для 2 вертикальних меню.',
	'multilinguisme_explication2' => 'За замовчуванням відображаються коди кожної мови, але їх можна замінити повною назвою мови або маленькими прапорцями. <br> Але будьте обережні: англійською мовою говорять не тільки у Великій Британії, а іспанською розмовляють не тільки в Іспанії, ... тому бажано використовувати коди мов',
	'multilinguisme_niveau' => 'Меню з рубриками другого рівня',
	'multilinguisme_nom' => 'Назви мов',

	// N
	'nav_mot_cle' => ' Перегляд за ключовими словами',
	'noir' => 'чорний',
	'noisette_article' => 'Блоки, специфічні для сторінки статті',
	'noisette_commun' => 'Блоки, спільні для декількох сторінок',
	'noisette_perso' => 'Блок особистих налаштувань',
	'noisette_rubrique' => 'Блоки, специфічні для сторінки рубрики',
	'noisettes_acces_direct_explication' => 'Назва цього блоку задано назвою статті з ключовим словом "прямий доступ" ("acces-direct"). Її переклади мають бути доступні всіма мовами багатомовного сайту.',
	'noisettes_actus_enlever' => 'Приховати блок, якщо немає новин',
	'noisettes_actus_tempo' => 'Затримка між кожною дією (у секундах)',
	'noisettes_annuaire_admin' => 'Колір для адміністраторів',
	'noisettes_annuaire_icones' => 'Показати іконки',
	'noisettes_annuaire_lien' => 'Відображення посилання на тромбіноскоп (trombinoscope)',
	'noisettes_annuaire_pagination' => 'Кількість авторів на сторінці пагінації',
	'noisettes_annuaire_redacteur' => 'Колір для редакторів',
	'noisettes_annuaire_visiteur' => 'Колір для відвідувачів',
	'noisettes_calendrier_ajouter' => 'Посилання «Додати подію», якщо людина пройшла автентифікацію',
	'noisettes_calendrier_couleur' => 'Колір події за замовчуванням',
	'noisettes_calendrier_events' => 'Список майбутніх подій нижче',
	'noisettes_calendrier_explication2' => 'Цей вибір також буде поширюватися на блок «Майбутні події».',
	'noisettes_calendrier_lien_agenda' => 'Назва з посиланням на повносторінковий порядок денний',
	'noisettes_calendrier_pagination' => 'Кількість статей або подій на сторінці пагінації',
	'noisettes_calendrier_renvoi' => 'Для заходів, що використовують плагін Agenda, посилання відноситься до',
	'noisettes_calendrier_renvoi_aucun' => 'немає посилання',
	'noisettes_calendrier_renvoi_evenement' => 'подія',
	'noisettes_decouvrir_hasard' => 'Кількість випадкових статей',
	'noisettes_decouvrir_moins_vus' => 'Кількість найменш відвідуваних статей',
	'noisettes_decouvrir_ou' => 'Звідки брати статті?',
	'noisettes_decouvrir_plus_vus' => 'Кількість найбільш відвідуваних статей',
	'noisettes_decouvrir_recents' => 'Кількість останніх статей',
	'noisettes_decouvrir_rubrique' => 'Тільки в поточній рубриці та її підрубриках',
	'noisettes_decouvrir_site' => 'По всьому сайту (ви можете виключити статті або рубрики з ключовим словом "не для виявлення" - “pas-a-decouvrir”)',
	'noisettes_edito_explication' => 'Заголовок цього блоку задається назвою статті з ключовим словом "edito". На багатомовному сайті у неї мають бути переклади всіма мовами.',
	'noisettes_explication' => '<strong>Блокувати заголовки</strong><br><br>
У випадку <strong>багатомовного сайту</strong>, якщо ви хочете змінити назву блоку зберігаючи багатомовність, ви повинні писати цю назву таким чином:<br><br>
	               &lt;multi&gt; [fr]Exemple [en]Example [de]Beispiel [es]Ejemplo [uk]Приклад &lt;/multi&gt;<br><br>
Якщо залишити поле порожнім, Escal використовуватиме назву за замовчуванням.',
	'noisettes_forum_colonne' => 'Розміщення бічних колонок',
	'noisettes_forum_explication' => 'Тільки одна бічна колонка, тому що форум займає решту простору.',
	'noisettes_identification_explication' => 'Заголовок цього блоку не може бути змінено.',
	'noisettes_identification_redacteur' => 'Відображення форми реєстрації для нових <strong>редакторів</strong><br>(у вас має бути встановлений прапорець «Приймати реєстрацію» в адмін-зоні: Налаштування — Інтерактивність).',
	'noisettes_identification_redacteur_label' => 'Редактори',
	'noisettes_identification_visiteur' => 'Відображення форми реєстрації для нових відвідувачів</strong><br>(у вас має бути встановлений прапорець «Включити реєстрацію відвідувачів на сайті» в адмін-зоні: Налаштування – Реєстрація, розсилки)',
	'noisettes_identification_visiteur_label' => 'відвідувачів',
	'noisettes_multi_choix' => '<strong>Вибір груп ключових слів</strong>',
	'noisettes_multi_groupe1' => 'Перша група (обов’язково)',
	'noisettes_multi_groupe2' => 'Друга група (за бажанням)',
	'noisettes_multi_groupe3' => 'Третя група (за бажанням)',
	'noisettes_multi_groupe4' => 'Четверта група (за бажанням)',
	'noisettes_multi_groupe5' => 'П’ята група (за бажанням)',
	'noisettes_multi_groupe6' => 'Шоста група (за бажанням)',
	'noisettes_multi_groupe7' => 'Сьома група (за бажанням)',
	'noisettes_multi_groupe8' => 'Восьма група (за бажанням)',
	'noisettes_multi_groupe9' => 'Дев’ята група (за бажанням)',
	'noisettes_multi_non' => 'Не використовується',
	'noisettes_nav_groupe' => 'Група ключових слів, які потрібно використовувати',
	'noisettes_nav_nombre' => 'Максимальна кількість статей для відображення за кожним ключовим словом',
	'noisettes_perso_choix2' => 'Для останніх двох варіантів відображення:',
	'noisettes_perso_choix3' => 'Для перших трьох варіантів відображення:',
	'noisettes_perso_choix4' => 'Для четвертого варіанта відображення:',
	'noisettes_perso_liste1' => 'Список з назвою (і посиланням), датою публікації та автором.',
	'noisettes_perso_liste2' => 'Список тільки із заголовком (і посиланням)',
	'noisettes_perso_liste3' => 'Список із заголовком (і посиланням) та початком тексту',
	'noisettes_perso_liste4' => 'Прокручуваний список із заголовком (і посиланням) та початком тексту',
	'noisettes_perso_tempo' => 'Затримка між статтями (у секундах)',
	'noisettes_photos_lien' => 'Посилання на статтю, джерело фото',
	'noisettes_photos_tempo' => 'Затримка між зображеннями (у секундах)',
	'noisettes_sites_exclus' => 'Виключити сайти з ключовим словом "вибране" ("favori")',
	'noisettes_sites_exclus_explication' => 'бо вже є у блоці «Улюблені сайти» ("Sites favoris"), якщо ви ним користуєтеся',
	'noisettes_sites_tempo' => 'Затримка між логотипами сайтів (у секундах)',
	'noisettes_stats_affiche' => 'Відображати ...',
	'noisettes_stats_articles' => '...кількість статей на сайті',
	'noisettes_stats_auteurs' => '...кількість авторів',
	'noisettes_stats_choix' => 'Вибір статистики для відображення:',
	'noisettes_stats_comments' => '...кількість коментарів',
	'noisettes_stats_et' => 'і...',
	'noisettes_stats_jour' => 'Кількість відвідувачів на день',
	'noisettes_stats_libelle' => '...ярлик «Цей сайт важливий»',
	'noisettes_stats_ligne' => 'Кількість онлайн-відвідувачів',
	'noisettes_stats_max' => 'Найбільший день',
	'noisettes_stats_mots' => '...кількість ключових слів',
	'noisettes_stats_moyenne' => 'Середня кількість відвідувань',
	'noisettes_stats_pages' => 'Загальна кількість відвідуваних сторінок',
	'noisettes_stats_periode' => 'протягом ... (у днях)',
	'noisettes_stats_rubriques' => '...кількість рубрик',
	'noisettes_stats_sites' => '...кількість посилальних сайтів',
	'noisettes_stats_sujets' => 'Кількість тем форуму',
	'noisettes_stats_visites' => 'Загальна кількість відвідувань',
	'noisettes_sur_web_syndic' => 'Тільки синдиковані сайти',
	'nombre_actus' => 'Кількість новин',
	'nombre_articles' => 'Кількість статей',
	'nombre_articles_pagination' => 'Кількість статей на сторінці пагінації',
	'nombre_articles_sites' => 'Кількість статей на сайті',
	'nombre_caracteres' => 'Кількість символів, що відображаються в тексті статті/статей',
	'nombre_colonnes' => 'Кількість колонок',
	'nombre_colonnes2' => 'Кількість колонок (для статей і підзаголовків)',
	'nombre_comments' => 'Кількість коментарів',
	'nombre_comments_pagination' => 'Кількість коментарів на сторінці пагінації',
	'nombre_images' => 'Кількість зображень',
	'nombre_mots' => 'Кількість ключових слів для відображення',
	'nombre_sites' => 'Кількість сайтів',
	'nombre_sujets_pagination' => 'Кількість тем на сторінці пагінації',
	'nuage' => 'Хмарка ключових слів',

	// O
	'onglets_art_accueil' => 'Головна',
	'onglets_art_accueil_explication' => 'Відображається стаття з ключовим словом accueil, інакше вона буде опублікована останньою.<br> Назва статті відображається на вкладці.',
	'onglets_art_accueil_forum' => 'Показати головну статтю форуму',
	'onglets_art_archive' => 'Архівна стаття',
	'onglets_art_archive_explication' => 'Відображувана стаття буде випадковою статтею в рубриці з ключовим словом "архів" ("archive") або в одній з її підрубрик.<br> Заголовок вкладки - "Пам’ятай!" з підтримкою багатомовності мовами, доступними в Escal.',
	'onglets_art_archive_forum' => 'Подивитися архів статей форуму',
	'onglets_arts_mot' => 'Статті з ключовим словом',
	'onglets_arts_mot_choix' => 'Вибір ключового слова',
	'onglets_arts_mot_explication1' => 'Заголовок цієї вкладки буде відповідати ключовому слову',
	'onglets_arts_mot_explication2' => '<strong><a href="#articles_keywords">→ Налаштувати</a></strong>',
	'onglets_bis_pagin_defaut' => '0 | 10 | 20 | 30',
	'onglets_bis_pagin_page' => '1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | ...',
	'onglets_bis_pagin_pps' => '< 1 | 2 | 3 | 4 | 5 | 6 | >',
	'onglets_bis_pagin_prive' => '0 | 10 | 20 | 30 | Показати все',
	'onglets_bis_pagin_ps' => 'попередня сторінка | Наступна сторінка',
	'onglets_bis_pagination' => 'Шаблон нумерації сторінок (спробуйте!)',
	'onglets_choix' => 'Вибір вкладок',
	'onglets_cinq' => 'П’ята вкладка',
	'onglets_derniers_article' => 'Налаштування кожної статті',
	'onglets_derniers_arts_explication1' => '<ul>Їх можна відобразити трьома способами:
		<li><strong>Нові статті <a href="#last_articles">→ Налаштувати</a><br></strong></li>
		<li><strong>Нові статті 2 <a href="#last_articles_bis">→ Налаштувати</a><br></strong></li>
		<li><strong>Нові статті 3 <a href="#last_articles_ter">→ Налаштувати </a></strong></li>
		</ul>',
	'onglets_derniers_arts_explication2' => 'Заголовком цієї вкладки буде «Нові статті», якщо ви не дасте їй іншу назву нижче.',
	'onglets_derniers_arts_explication3' => 'Для «Останніх статей» ("Derniers articles") і «Останніх статей з ...» ("Derniers articles ter") статті з ключовим словом "annonce" або "annonce-defilant" будуть виключені зі списку, щоб уникнути дублікатів, окрім блоків «Оголошення» та «Прокручування реклами», де це не використовується.',
	'onglets_derniers_arts_explication4' => 'Статті з ключовим словом "annonce" або "annonce-defilant" будуть виключені зі списку, щоб уникнути дублікатів, якщо не використовуються блоки "Annonce" («Реклама») і "Annonces défilantes" («Прокрутка реклами»).',
	'onglets_derniers_autres' => 'Інші статті',
	'onglets_derniers_exergue' => 'Вибрана стаття',
	'onglets_derniers_presentation' => 'Загальна презентація',
	'onglets_deux' => 'Друга вкладка',
	'onglets_monart' => 'Моя стаття',
	'onglets_monart2' => 'Моя стаття 2',
	'onglets_monart2_titre' => 'Заголовок вкладки<br>"Моя стаття 2"',
	'onglets_monart3' => 'Моя стаття 3',
	'onglets_monart3_titre' => 'Заголовок вкладки <br>"Моя стаття 3"',
	'onglets_monart4' => 'Моя стаття 4',
	'onglets_monart4_titre' => 'Заголовок вкладки<br>"Моя стаття 4"',
	'onglets_monart5' => 'Моя стаття 5',
	'onglets_monart5_titre' => 'Заголовок вкладки<br>"Моя стаття 5""',
	'onglets_monart_explication' => '<strong>Моя стаття</strong> або <strong>Моя стаття 2</strong> або <strong>Моя стаття 3</strong> або <strong>Моя стаття 4</strong> або <strong>Моя стаття 5</strong> – це статті з ключовими словами "mon-article", "mon-article2", "mon-article3", "mon-article4" та "mon-article5".<br><br>Заголовком вкладки буде назва статті, якщо ви не дасте їй іншу назву нижче. Відображаються лише текст та PS.',
	'onglets_monart_titre' => 'Заголовок вкладки <br>"Моя стаття"',
	'onglets_nombre_articles' => 'Загальна кількість статей для відображення',
	'onglets_nombre_articles_exergue' => 'Кількість статей для відображення, включаючи обрану статтю',
	'onglets_parametrage' => 'Налаштування вкладки',
	'onglets_placer' => 'Розташовуйте вкладки вгорі сторінки при натисканні на одну з них.',
	'onglets_quatre' => 'Четверта вкладка',
	'onglets_rien' => 'Не показувати цю вкладку',
	'onglets_rub2' => 'Рубрика 2',
	'onglets_rub3' => 'Рубрика 3',
	'onglets_rub4' => 'Рубрика 4',
	'onglets_rub5' => 'Рубрика 5',
	'onglets_rub_auteur' => 'показати автора статті',
	'onglets_rub_descriptif' => 'показати опис рубрики',
	'onglets_rub_explication' => '<strong>Рубрика</strong> або <strong>Рубрика 2</strong> або <strong>Рубрика 3</strong> або <strong>Рубрика 4</strong> або <strong>Рубрика 5 </strong><br>використовуються для відображення статей рубрики, позначеної ключовим словом "RubriqueOnglet" або "RubriqueOngletN" (N = від 2 до 5).<br><br>На вкладці відображається назва рубрики, а внизу - статті цієї рубрики та її пiдрубрик.',
	'onglets_rub_logo' => 'виберіть розмір логотипу статті (у пікселях)',
	'onglets_rub_pagin' => 'визначити крок нумерації сторінок для статей',
	'onglets_rub_texte' => 'відображати текст рубрики',
	'onglets_sur_web_explication' => 'На цій вкладці буде відображено список синдикованих статей на вашому сайті із зазначенням вихідного сайту та дати його публікації.',
	'onglets_sur_web_explication2' => 'Назва вкладки — «В Інтернеті», якщо ви не дасте їй іншу назву нижче.',
	'onglets_sur_web_pagin' => 'Для цих статей немає нумерації сторінок.',
	'onglets_ter_mot_exergue' => 'Показ обраної статті',
	'onglets_trois' => 'Третя вкладка',
	'onglets_un' => 'Перша вкладка',

	// P
	'page_accueil' => 'Головна (La page d’accueil)',
	'page_article' => 'Сторінка статті',
	'page_auteur' => 'Автори сторінок',
	'page_autres' => 'Інші сторінки',
	'page_autres_explication' => 'Сторінка контактів, каталог, автор, пошук, 404,...',
	'page_contact' => 'Сторінка зворотнього зв’язку',
	'page_contact2' => 'Сторінка зворотного зв’язку з',
	'page_contact3' => 'Сторінка контактів',
	'page_forum' => 'Форум сайту',
	'page_recherche' => 'Сторінка пошуку',
	'page_rubrique' => 'Сторінка рубрики',
	'pages' => 'сторінок',
	'pages_vues' => 'Кількість відвіданих сторінок: ',
	'par_defaut' => 'за замовчуванням:',
	'photos_hasard' => 'Кілька випадкових фотографій',
	'photos_hasard2' => 'Випадкові фото',
	'pied' => 'Футер (нижній колонтитул)',
	'pied_autres_mentions' => 'Інші згадки',
	'pied_citations_activer' => 'Включити цитати',
	'pied_citations_explication' => 'Ви можете відображати цитати ліворуч у футері. <br><br>
		<ul>Ці цитати
		<li>мають бути в окремій статті з ключовим словом «citations»</li>
		<li>кожна має бути в одному (окремому) абзаці</li>
		<li>не повинні перевищувати 200 символів</li>
		<li>мають бути розділені порожнім рядком.</li>
		</ul>',
	'pied_citations_titre' => 'Назва, яка відображатиметься вище',
	'pied_citations_titre_explication' => 'Залиште порожнім - не буде назви',
	'pied_copyright' => 'Відображення авторських прав',
	'pied_hebergeur' => 'Відображення хоста',
	'pied_hebergeur_nom' => 'Ім’я вашого хостера',
	'pied_hebergeur_url' => 'URL вашого хоста (якщо вам потрібне посилання на нього)',
	'pied_lien_affichage' => 'Показати посилання, обрані вище',
	'pied_lien_contact' => 'Посилання на сторінку <strong>контактів</strong>',
	'pied_lien_icone' => 'Як значки',
	'pied_lien_mentions' => 'Посилання на сторінку <strong>юридичних повідомлень</strong>.',
	'pied_lien_mentions_article' => 'Номер вибраного елемента',
	'pied_lien_mentions_choix' => 'Вибір сторінки для відображення',
	'pied_lien_mentions_choix1' => 'До плагіна «Правові повідомлення SPIP» ("SPIP Mentions légales"), якщо ви його активували.',
	'pied_lien_mentions_choix2' => 'До плагіна «Legal EN», якщо ви активували «Юридичну інформацію» та «Особисті дані та файли cookie» (плагін, призначений саме для сайтів National Education)',
	'pied_lien_mentions_choix3' => 'До статті за вашим вибором',
	'pied_lien_mentions_choix4' => 'До статті з тегом "legal-notice"',
	'pied_lien_plan' => 'Посилання на <strong>карту сайту</strong>',
	'pied_lien_prive' => 'Посилання на <strong>адмін-зону</strong>',
	'pied_lien_rss' => 'Значок з посиланням на сторінку <strong>RSS-канал</strong>.',
	'pied_lien_squelette' => 'Посилання на <strong>шаблон</strong> сторінки.',
	'pied_lien_texte' => 'У вигляді текстів',
	'pointeur' => 'Покажчики миші',
	'pointeur_defaut' => 'Покажчик за замовчуванням',
	'pointeur_explication' => 'Тут можна змінити зображення вказівників.
		<ul>Ці зображення повинні:
		<li>бути у форматі .cur або .ani</li>
		<li>не перевищувати 32x32 пікселів</li>
		<li>перебувати в папці /squelettes/images/pointeurs</li>
		</ul>',
	'pointeur_general' => 'Загальний покажчик',
	'pointeur_liens' => 'Покажчик для посилань',
	'police_luciole' => 'Luciole (підходить для людей з вадами зору)',
	'police_readme' => 'Ці шрифти розповсюджуються безкоштовно відповідно до міжнародної публічної ліцензії Creative Commons Attribution 4.0: https://creativecommons.org/licenses/by/4.0/legalcode.fr<br><br>Luciole © Laurent Bourcellier & Jonathan Perez',
	'police_verdana' => 'Verdana, Arial, Helvetica (за замовчуванням)',
	'popup' => 'Модальне вікно',
	'popup2_doc' => 'Вміст цього вікна буде текстом останньої статті з ключовим словом "popup" в групі "affichage".',
	'popup_activer' => 'Увімкнути модальне вікно',
	'popup_doc' => 'Відображати модальне поле під час відкриття сайту. (Використовуйте економно)<br>
					<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article448" title="Переглянути документацію">Документація</a>',
	'popup_dureecookie' => 'Термін дії файлів cookie (в днях)',
	'popup_largeur' => 'Розмір popup (у px)',
	'popup_temps' => 'Час відображення (у секундах)',
	'portfolio' => 'Портфоліо',
	'poster_message' => 'Розмістити нове повідомлення',
	'premier_visiteur' => 'Ви - перший!',
	'proposer_sujet' => 'Запропонувати тему',
	'publie' => 'Опубліковано',
	'puce' => 'пункт списку',

	// Q
	'qrcode_explication' => 'Якщо ви активуєте плагін <strong><a class="spip_out" href="https://contrib.spip.net/Qrcode" title="Documentation">"QrCode"</a></strong>, QR-код, що відповідає URL-адресі вашої статті, автоматично з’явиться внизу <strong>версії для друку</strong> цієї статті.',
	'qrcode_plugin' => 'QR-код',

	// R
	'rainette' => 'Rainette (погода)',
	'rainette_code' => 'Код міста:',
	'rainette_explication' => 'Якщо ви активували та налаштували (вибір послуги та ключ реєстрації) <a class="spip_out" href="https://contrib.spip.net/Rainette-v3-une-evolution-majeure" title=" Документація" >плагін «Погода» ("Rainette")</a>, введіть нижче код міста, яке вас цікавить, або його назву чи GPS-координати.<br>
Щоб дізнатися код для введення, дивіться додаткові пояснення в <a class="spip_out" href="https://contrib.spip.net/Rainette-v3-une-evolution-majeure#L-indication-du-lieu" title ="Documentation">документації плагіна</a>',
	'rainette_service' => 'Вибір метеослужби',
	'rappel_discussion' => 'Нагадування про обговорення',
	'reagir_sujet' => 'Реагувати на тему',
	'recherche' => 'Пошук',
	'recherche_articles_non' => 'Не знайдено статей за цим запитом',
	'recherche_multi' => 'Пошук за кількома критеріями',
	'recherche_multi_resultat' => 'Результати пошуку:',
	'recherche_non' => 'Немає результатів за цим запитом',
	'recherche_resultat' => 'Результат пошуку для ',
	'recherche_rubriques_non' => 'Не знайдені рубрики за цим запитом',
	'rechercher' => 'Пошук...',
	'repondre_message' => 'Відповісти на це повідомлення',
	'resultat_articles' => 'Статті',
	'resultat_messages' => 'Повідомлення на форумах',
	'resultat_ordre_articles' => 'Сортувати статті за',
	'resultat_ordre_forums' => 'Сортувати повідомлення за',
	'resultat_ordre_rubriques' => 'Сортувати рубрики за',
	'resultat_ordre_sites' => 'Сортувати сайти за',
	'resultat_rubriques' => 'Рубрики',
	'resultat_sites' => 'Сайти, на які посилаються',
	'resultats_recherche' => 'Елементи для відображення в результатах пошуку:',
	'retour_forum' => 'Повернутись на форум',
	'rubrique_articles' => 'Статті цієї рубрики',
	'rubrique_cachee_descriptif' => 'Рубрика для спеціальних статей, які не відображатимуться ні в меню, ні в карті сайту, ні в останніх статтях.',
	'rubrique_cachee_titre' => 'Прихована рубрика',
	'rubrique_contenu' => 'Ця рубрика містить',
	'rubrique_date' => 'Відображення дати публікації та дати зміни статей.',
	'rubrique_la' => 'Рубрика',
	'rubrique_pagin_bas' => 'Показувати нумерацію сторінок внизу',
	'rubrique_pagin_haut' => 'Показувати нумерацію сторінок вгорі',
	'rubrique_rss' => 'Відображення логотипу RSS з посиланням на сторінку каналу.',
	'rubrique_rss2' => 'Відображення логотипу RSS з посиланням на сторінку каналу для підрубрик',
	'rubrique_site_reference' => 'Сайт, вказаний у цій рубриці',
	'rubrique_sites_references' => 'Реферовані сайти в цій рубриці',
	'rubrique_sous_rubriques' => 'Подрубрики в цій рубриці',
	'rubrique_taille_logo' => 'Максимальний розмір логотипу рубрик (у пікселях)',
	'rubrique_trouvee' => 'знайдена рубрика',
	'rubriques_trouvees' => 'рубрик(и) знайдено',

	// S
	'shoutbox_explication' => 'Якщо ви ввімкнули <a class="spip_out" href="https://contrib.spip.net/Shoutbox-pour-SPIP" title="Documentation">плагін "Shoutbox"</a>, ви можете налаштувати:',
	'shoutbox_hauteur' => 'висота блоку повідомлення (у пікселях)',
	'shoutbox_nombre' => 'кількість повідомлень для відображення',
	'shoutbox_plugin' => 'Рупор (плагін голосової пошти)',
	'signalement_explication' => 'Якщо ви активували <a class="spip_out" href="http://www.mediaspip.net/documentation/les-plugins-utiles-par-mediaspip/signal-alert-on-the-validity-of/article /reporting-alert-on-the" title="Документація">плагін «Звіти» ("Reporting")</a>, вам більше нічого робити.
  Кнопка звіту <img src="" style="vertical-align:middle;" alt="" /> автоматично з’являтиметься у верхній частині кожної статті.',
	'signalement_plugin' => 'Звіти (Signalement)',
	'site_realise_avec' => 'Сайт створений на',
	'site_reference' => 'реферований сайт',
	'sites' => 'Сайти, на які посилаються',
	'sites_favoris' => 'Улюблені сайти',
	'sites_references' => 'реферовані сайти',
	'socialtags_explication' => 'Якщо ви активували <a class="spip_out" href="http://contrib.spip.net/Social-tags" title="Documentation">плагін «Соціальні теги»</a>, Escal пропонує вам 3 варіанти вибору селекторів, які можна прописати в конфігурації плагіна Social tags:
		<ul>
		<li><strong>#socialtags-tools</strong> для відображення піктограм на панелі інструментів Escal</li>
		<li><strong>#socialtags-article</strong> для відображення поруч із написом «Роздрукувати цю статтю» у ваших статтях</li>
		<li><strong>#content</strong>, щоб відобразити їх внизу кожної статті.</li>
		</ul>
		<br>Але це тільки 3 можливості серед багатьох інших.',
	'socialtags_plugin' => 'Соціальні теги',
	'sous_rubrique' => 'підрубрика',
	'sous_rubriques' => 'підрубрик(и)',
	'spip400_explication' => 'Якщо ви активували <a class="spip_out" href="http://contrib.spip.net/SPIP-400-Gestion-des-erreurs-HTTP" title="Документація">плагін “SPIP 400”</ a>, вам більше нічого не потрібно робити, сторінка 404 цього плагіна автоматично займе місце сторінки Escal.',
	'spip400_plugin' => 'Spip 400',
	'spipdf_explication' => 'Якщо ви активували <a class="spip_out" href="http://contrib.spip.net/spiPDF-generer-des-contenus-sur-mesure-en-PDF" title="Документація">плагін "spiPDF "</a>,
 на сторінках «статтей» буде автоматично відображатися  піктограма, натискання на яку дозволяє зберегти або роздрукувати статтю у форматі .pdf. <br><br>
Не забудьте завантажити архів бібліотеки та розархівувати його в папку /lib/mpdf у корені вашого SPIP-сайту (див. документацію).',
	'spipdf_plugin' => 'SpiPDF',
	'suggerer_reponse' => 'Перш ніж продовжити, перегляньте наступні сторінки. 	<br>Вони можуть містити відповідь, яку ви шукаєте.',
	'sujets' => 'теми',
	'sur_forum' => 'На форумі знаходиться:',

	// T
	'taille_augmenter' => 'Збільшити розмір символів',
	'taille_diminuer' => 'Зменшити розмір символів',
	'taille_fichier' => 'Розмір файлу',
	'taille_icones_pied' => 'Розмір піктограми (у пікселях)',
	'taille_logo' => 'Максимальний розмір логотипу (у пікселях)',
	'taille_logo_site' => 'Максимальний розмір логотипу сайту (у пікселях)',
	'taille_outils_article' => 'Розмір піктограм на панелі інструментів (у пікселях)',
	'taille_police' => 'Розмір шрифту:',
	'taille_police_explication' => 'Розмір шрифту для всього сайту (визначається в rem)<br> Приклади: <br>
			62,5% → 1rem = 10 пікселів<br>
			75% → 1rem = 12 пікселів<br>
			87,5% → 1rem = 14 пікселів<br>
			100% → 1rem=16px<br>',
	'taille_pourcent' => 'Розмір (у %)',
	'telechargement' => 'Файл для завантаження:',
	'telechargements' => 'Файли для завантаження:',
	'texte_accueil' => 'Ви знаходитесь на сторінці конфігурації шаблону Escal.<br> Ця сторінка дозволить вам налаштувати ваш сайт від вибору блоків до вибору кольорів, відображаючи або приховуючи безліч елементів. <br>Вирішувати вам!',
	'texte_coupe' => 'Обрізати текст на (кількість символів)',
	'texte_sous_image' => 'Назва та опис',
	'texte_sous_image_non' => 'Поруч із логотипом',
	'texte_sous_image_oui' => 'Під логотипом',
	'title_contact' => 'Звязатися зі службою технічної підтримки',
	'title_escal' => 'Посилання на офіційний сайт Escal',
	'title_espace_redacteurs' => 'Особистий простір для авторів та адміністраторів',
	'title_mentions' => 'Юридичні повідомлення сайту',
	'titre' => 'Заголовок',
	'titre_contenu' => 'Назва та зміст',
	'titre_coupe' => 'Кількість символів, що відображаються в заголовках статей',
	'titre_noisette' => 'Заголовок блоку',
	'total_visites' => 'Усього відвідувань:',
	'traductions_article' => 'Переклад цієї статті:',
	'transparent' => 'Прозорий фон',

	// U
	'une' => 'Вибране (“A la une”)',
	'une_bloc' => 'Блок «Вибрані» ("A la une")',
	'une_mots_cles' => 'Статті з ключовими словами',

	// V
	'version_actuelle' => 'Поточна версія:',
	'version_maj' => 'Доступна версія:',
	'version_ok' => 'У вас встановлена остання доступна версія',
	'video' => 'Відео',
	'videos' => 'Відео',
	'videos_explication' => 'Щоб відобразити свої відео, ви повинні <br>- активувати <a class="spip_out" href="https://contrib.spip.net/Plugin-Video-s" title="Documentation">відео-плагін</a> <br>- зв’зати ключове слово "video-une" зі статтями, що містять ваші відео',
	'visites' => 'Відвідування',
	'visites_jour' => 'відвідувань на день',
	'visiteur' => 'відвідувач',
	'visiteurs' => 'відвідувачів',
	'vu' => 'див.',

	// W
	'webmestre' => 'Веб-майстер'
);
