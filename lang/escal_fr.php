<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-squelettes/escal.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_decouvrir' => 'A découvrir',
	'a_telecharger' => 'A télécharger',
	'acces_direct' => 'Accès direct',
	'accessibilite' => 'Accessibilité',
	'accessibilite_explication' => 'Affichage d’un bouton de paramétrage d’accessibilité au-dessus du bandeau',
	'accessibilite_logo' => 'Choix de la couleur du logo',
	'accessibilite_police' => 'Choix de la police du site',
	'actus' => 'Actus',
	'adresse_non' => 'L’auteur ou autrice n’a pas fourni son adresse e-mail',
	'affichage_auteur_articles' => 'Affichage du nom de l’auteur ou autrice des articles',
	'affichage_chapeau' => 'Affichage du chapeau',
	'affichage_choix' => 'Choix de l’affichage',
	'affichage_date_modif' => 'Affichage de la date de mise à jour',
	'affichage_date_pub' => 'Affichage de la date de publication',
	'affichage_date_pub_modif' => 'Affichage de la date de publication et de la date de modification',
	'affichage_date_pub_ou_modif' => 'Affichage de la date <br>(de modification, sinon de publication)',
	'affichage_debut' => 'Affichage du début du texte',
	'affichage_descriptif' => 'Affichage du descriptif',
	'affichage_form_reponse' => 'Affichage du formulaire de réponse sous l’article',
	'affichage_image' => 'Affichage de la première image',
	'affichage_logo_site' => 'Affichage du logo du site s’il existe',
	'affichage_mots_cles' => 'Affichage des mots-clés associés aux articles',
	'affichage_mots_cles_article' => 'Pour la page article',
	'affichage_mots_cles_rubrique' => 'Pour la page rubrique',
	'affichage_mots_cles_une' => 'Pour la page d’accueil',
	'affichage_nom_auteur' => 'Affichage du nom de l’auteur ou autrice',
	'affichage_nombre_comments' => 'Affichage du nombre de commentaires',
	'affichage_ordre' => 'Ordre d’affichage',
	'affichage_ordre_date' => 'Ordre chronologique',
	'affichage_ordre_dateinv' => 'Ordre antéchronologique',
	'affichage_ordre_datemodif' => 'Par date de dernière modification',
	'affichage_ordre_hasard' => 'Au hasard',
	'affichage_ordre_num' => 'Par numéro de titre',
	'affichage_ordre_pertinence' => 'Pertinence',
	'affichage_rubrique' => 'Affichage de la rubrique mère',
	'affichage_soustitre' => 'Affichage du Sous-titre',
	'affichage_surtitre' => 'Affichage du Surtitre',
	'affichage_video' => 'Remplacer tous ces éléments par une vidéo si l’article en contient au moins une',
	'affichage_visites_inter' => 'Séparateur entre visites et popularité',
	'affichage_visites_popularite' => 'Affichage du nombre de visites et de la popularité',
	'affiche_articles' => 'Avec affichage des articles',
	'agenda' => 'Agenda',
	'aide' => 'Un bug, un problème technique, une suggestion... le <a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?page=forumSite-rubrique&lang=fr" title="Le forum">forum</a> est là pour ça',
	'alerte_javascript' => 'Cette page ne peut pas fonctionner correctement sans JavaScript. Veuillez le réactiver',
	'alt_telechargements' => 'Téléchargements',
	'annonce' => 'Annonce',
	'annonce_afficher' => 'Afficher le bloc "Annonce"',
	'annonce_defil' => 'Annonces défilantes',
	'annonce_defil_afficher' => 'Afficher le bloc "Annonces défilantes"',
	'annonce_defil_hauteur' => 'Hauteur du bloc (en px)',
	'annonce_defil_nombre' => 'Nombre d’annonces à afficher',
	'annonce_defil_nombre_affiche' => 'Afficher le nombre d’annonces',
	'annonce_defil_tempo' => 'Temporisation entre chaque annonce (en secondes)',
	'annonce_defil_tempo_explication' => 'Un vidage de cache est nécessaire pour appliquer une nouvelle temporisation.',
	'annonce_explication' => '<ul>Choisir les dates d’affichage de l’article en annonce :
                    <li>date du début d’affichage = date de publication</li>
                    <li>date de fin d’affichage = date de rédaction antérieure</li>
                    </ul>',
	'annonces' => 'Les annonces',
	'annuaire' => 'Annuaire',
	'annuaire_auteurs' => 'Annuaire auteurs et autrices',
	'annuaire_invitation' => 'Vous aussi, vous utilisez ESCAL ?<br>Alors ajoutez votre site dans cette page.',
	'arrondis' => 'Les arrondis',
	'arrondis_blocs_lateraux' => 'Les blocs latéraux',
	'arrondis_calendrier' => 'Calendrier (partie événements à venir)',
	'arrondis_centre_sommaire' => 'Le bloc central de la page sommaire',
	'arrondis_explication1' => 'Pour des arrondis identiques aux 4 coins, indiquez simplement la valeur de l’arrondi que vous désirez. Exemple : 10px<br><br>
    			Pour des coins sans arrondis inscrivez la valeur 0<br><br> ',
	'arrondis_explication2' => 'Pour des coins aux arrondis différents, ordre des valeurs à indiquer :',
	'arrondis_explication3' => 'Pour faire des tests et générer le code correspondant : <a class="spip_out" href="http://www.cssmatic.com/border-radius">CSSmatic</a>
		Pour une utilisation plus poussée avec des formes elliptiques : <a class="spip_out" href="http://www.alsacreations.com/astuce/lire/979-ovale-forme-elliptique-css3-sans-image.html">Alsacréations</a>',
	'arrondis_identification_recherche' => 'Identification et recherche',
	'arrondis_menus' => 'Les menus',
	'arrondis_onglets' => 'Onglets',
	'article_acces_direct_descriptif' => 'Cet article sera celui qui apparaîtra dans le bloc "Accès direct" si vous le laissez activé" ',
	'article_acces_direct_texte' => 'Ici vous pouvez afficher le contenu d’un article avec le mot-clé "acces-direct".<br><br>
	                                 Le titre du bloc sera celui de l’article.<br><br>
							   Plus d’infos dans <a  class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24">cet article</a> ',
	'article_archive' => 'Souvenez-vous !',
	'article_dernier' => 'Dernier article paru :',
	'article_edito_descriptif' => 'Cet article sera celui qui apparaîtra dans le bloc "Edito" si vous le laissez activé',
	'article_edito_texte' => 'Ici vous pouvez afficher le contenu d’un article avec le mot-clé "edito".<br><br>
	                          Le titre du bloc sera celui de l’article.<br><br>
	                          Plus d’infos dans <a  class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23">cet article</a>',
	'article_edito_titre' => 'Edito',
	'article_forum' => 'Forum de l’article',
	'article_imprimer' => 'Imprimer',
	'article_libre1' => 'Article libre 1',
	'article_libre2' => 'Article libre 2',
	'article_libre3' => 'Article libre 3',
	'article_libre4' => 'Article libre 4',
	'article_libre5' => 'Article libre 5',
	'article_licence' => 'Licence :',
	'article_logo' => 'Taille max du logo de l’article (en px)',
	'article_mise_en_ligne' => 'Article mis en ligne le',
	'article_modifie' => 'dernière modification le',
	'article_trouve' => 'article trouvé',
	'article_une' => 'Mettre cet article à la une',
	'articlepdf_afficher' => 'Afficher l’icône dans les rubriques',
	'articlepdf_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/Article-PDF,2226" title="Documentation">plugin "Article PDF"</a>, vous n’avez rien d’autre à faire, l’icône permettant d’enregistrer ou d’imprimer un article au format .pdf s’affichera automatiquement dans les pages "article".<br><br>
            On peut aussi l’afficher dans les pages "rubrique".',
	'articlepdf_plugin' => 'ArticlePDF',
	'articles_associes' => 'Voir les articles associés à ',
	'articles_associes_mot' => 'Articles/documents associés au mot',
	'articles_auteur' => 'Cet auteur ou autrice a écrit :',
	'articles_derniers' => 'Les derniers articles',
	'articles_largeur_image' => 'Largeur maximum de l’image (en px)',
	'articles_largeur_images' => 'Largeur maximum des images (en px)',
	'articles_largeur_images_classique' => 'Pour les articles classiques',
	'articles_largeur_images_pleinepage' => 'Pour les articles en pleine page (articles avec le mot-clé "pleinepage" qui supprime les blocs latéraux)',
	'articles_logo' => 'Taille max du logo des articles (en px)',
	'articles_plus_vus' => 'Articles les plus vus',
	'articles_portfolio' => 'Afficher le portfolio',
	'articles_portfolio_descriptif' => 'Afficher le descriptif du document s’il existe',
	'articles_portfolio_explication' => 'Doublon avec le bloc "A télécharger"',
	'articles_premier_message' => 'N’afficher que le premier message de chaque fil de discussion, les réponses étant dépliables',
	'articles_reponses_forum' => 'Réponses des forums d’articles',
	'articles_rubrique' => 'Articles de rubrique',
	'articles_site' => 'Voir les articles de ce site',
	'articles_trouves' => 'articles trouvés',
	'aucun' => 'Aucun',
	'aujourdhui' => 'Aujourd’hui : ',
	'auteurs' => 'auteurs ou autrices',
	'avec' => 'avec',
	'avec_le_squelette' => 'Habillage',

	// B
	'bandeau' => 'Bandeau',
	'bandeau_choix_explication' => 'Si votre image de bandeau n’occupe pas toute la largeur, vous pouvez choisir sa position.<br>
              Attention néanmoins aux "effets de bords" si vous affichez le nom du  site, son slogan ou son descriptif.',
	'bandeau_choix_item1' => '<strong>Option 1</strong> : affichage du logo d’Escal (par défaut)',
	'bandeau_choix_item2' => '<strong>Option 2</strong> : affichage du logo du site',
	'bandeau_choix_item3' => '<strong>Option 3</strong> : affichage d’un bandeau personnalisé',
	'bandeau_choix_item4' => '<strong>Option 4</strong> : aucune image',
	'bandeau_choix_option' => 'Choix de l’option pour l’image',
	'bandeau_choix_position' => 'Position de l’image du bandeau ',
	'bandeau_option3' => 'Pour l’option 3',
	'bandeau_option3_aucune' => 'Aucune image de remplacement',
	'bandeau_option3_explication1' => 'Votre image doit être placée dans <strong>/squelettes/images/bandeau</strong><br>
								Cette image sera "responsive"<br>
								Toutes les images .jpg .png .webp et .gif de ce répertoire seront listées<br>
								Vous aurez même un aperçu de votre bandeau en direct !',
	'bandeau_option3_explication2' => 'Et si votre hébergeur ne permet pas de lister les fichiers présents,
                vous pouvez indiquer ici le chemin de votre fichier image.<br>
                exemple : <strong>images/bandeau/bandeau.jpg</strong><br>
                si votre fichier <strong>bandeau.jpg</strong> est dans <strong>squelettes/images/bandeau</strong>',
	'bandeau_option3_explication3' => 'Si votre dossier <strong>/squelettes/images/bandeau</strong> contient une image appelée <strong>rubriqueXX.jpg</strong> où XX est le numéro d’un secteur, alors ce secteur, ses sous-rubriques et leurs articles afficheront automatiquement ce bandeau.
              Les autres afficheront le bandeau défini au-dessus.',
	'bandeau_option3_explication3titre' => 'Un bandeau en fonction du secteur ?<br><br>',
	'bandeau_option3_explication4' => 'Si votre dossier /squelettes/images/bandeau contient des images appelées
     				<ul>
	     			<li>hiver.jpg ou hiver.png</li>
	     			<li>printemps.jpg ou printemps.png</li>
	     			<li>été.jpg ou été.png</li>
	     			<li>automne.jpg ou automne.png</li>
	     			</ul>
    				ces images s’afficheront automatiquement à la saison correspondante.',
	'bandeau_option3_explication4titre' => 'Un bandeau en fonction de la saison ?<br><br>',
	'bandeau_texte' => 'Choix des textes',
	'bandeau_texte_descriptif' => '<strong>Descriptif</strong><br>du site présent',
	'bandeau_texte_nom' => '<strong>Nom</strong><br>du site présent',
	'bandeau_texte_slogan' => '<strong>Slogan</strong><br>du site présent',
	'bandeau_texte_taille' => 'Taille des caractères (en px)',
	'barre_outils_dessous' => 'Sous le bandeau',
	'barre_outils_dessus' => 'Au-dessus du bandeau',
	'barre_outils_explication' => 'Cette option se désactive automatiquement sur les écrans inférieurs à 640px de large',
	'barre_outils_fixer' => 'Si vous avez choisi de placer la barre d’outils au-dessus du bandeau, voulez-vous la fixer en haut de la page ?',
	'barre_outils_identification' => 'Affichage du formulaire d’identification simple',
	'barre_outils_place' => 'Place de la barre d’outils (formulaire d’identification simple, menu de langues, formulaire de recherche, bouton-lien vers le forum du site)',
	'bas' => 'En bas',
	'bienvenue' => 'Bienvenue',
	'blanc' => 'blanc',
	'bloc_choix' => 'Choix des blocs',
	'bloc_cinq' => 'Cinquième bloc',
	'bloc_deux' => 'Deuxième bloc',
	'bloc_dix' => 'Dixième bloc',
	'bloc_huit' => 'Huitième bloc',
	'bloc_neuf' => 'Neuvième bloc',
	'bloc_parametrage' => 'Paramétrage des blocs',
	'bloc_quatre' => 'Quatrième bloc',
	'bloc_sept' => 'Septième bloc',
	'bloc_six' => 'Sixième bloc',
	'bloc_trois' => 'Troisième bloc',
	'bloc_un' => 'Premier bloc',
	'blocs_disponibles' => 'Blocs disponibles',
	'bord_pages' => 'Bord latéral des pages',
	'bord_pages_couleur' => 'Couleur du trait ou de l’ombre',
	'bord_pages_decalage' => 'Largeur du trait ou décalage de l’ombre (en px)',
	'bord_pages_force' => 'Force du dégradé (en px)',
	'bord_pages_label' => 'Mettre un bord',
	'bord_pages_nb' => 'Indiquer 0 pour avoir un trait sans ombre ',
	'bords' => 'Les bords',
	'bords1' => 'Items du menu horizontal',
	'bords2' => 'Pages article et rubrique, forums des articles, page contact, annuaire',
	'bords3' => 'Tableaux',
	'bords5' => 'Items menu horizontal survolés, identification, recherche, événement calendrier',
	'bords6' => 'Forum du site',
	'bords_arrondis' => 'Les bords et les arrondis',

	// C
	'cadres' => 'Bord et images',
	'cadres_bords' => 'Bord des blocs présents',
	'cadres_images' => 'Largeur des images',
	'cadres_images_largeur' => 'Largeur maximum des images (en px) pour les articles utilisés dans certains blocs latéraux : Accès direct, Edito, Photos au hasard, Articles libres 1 à 5',
	'calendrier' => 'Calendrier',
	'centre' => 'Au centre',
	'cfg_page_accueil' => 'Accueil',
	'cfg_page_arrondis' => 'Les arrondis',
	'cfg_page_article' => 'Les articles',
	'cfg_page_article_lateral' => 'Les articles',
	'cfg_page_article_principal' => 'Les articles',
	'cfg_page_articlepdf' => 'Article PDF',
	'cfg_page_autres_principal' => 'Les autres pages',
	'cfg_page_bandeau' => 'Bandeau',
	'cfg_page_blocs_lateraux' => 'Blocs latéraux',
	'cfg_page_bords' => 'Les bords et les arrondis',
	'cfg_page_choix_blocs' => 'Choix des blocs latéraux',
	'cfg_page_colonne_principale' => 'Colonne principale',
	'cfg_page_contact_principal' => 'La page contact',
	'cfg_page_deplier_replier' => 'Déplier et replier',
	'cfg_page_elements' => 'Éléments',
	'cfg_page_facebook' => 'Facebook',
	'cfg_page_fonds' => 'Les fonds et les textes',
	'cfg_page_forumsite' => 'Le forum du site',
	'cfg_page_forumsite_lateral' => 'Le forum du site',
	'cfg_page_forumsite_principal' => 'Le forum du site',
	'cfg_page_galleria' => 'Galleria',
	'cfg_page_generalites' => 'Généralités',
	'cfg_page_layout' => 'Mise en page',
	'cfg_page_licence' => 'Licence',
	'cfg_page_liens_sociaux' => 'Liens sociaux',
	'cfg_page_mentions' => 'Mentions légales',
	'cfg_page_menuh' => 'Menu horizontal',
	'cfg_page_meta' => 'Balises "meta"',
	'cfg_page_multilinguisme' => 'Multilinguisme',
	'cfg_page_pages_noisettes' => 'Les autres pages',
	'cfg_page_parametrage_blocs' => 'Paramétrage des blocs latéraux',
	'cfg_page_pied' => 'Pied de page',
	'cfg_page_plugins' => 'Des plugins dans Escal',
	'cfg_page_popup' => 'Boîte modale',
	'cfg_page_qrcode' => 'QrCode',
	'cfg_page_rainette' => 'Rainette',
	'cfg_page_rubrique' => 'Les rubriques',
	'cfg_page_rubrique_lateral' => 'Les rubriques',
	'cfg_page_rubrique_principal' => 'Les rubriques',
	'cfg_page_shoutbox' => 'Shoutbox',
	'cfg_page_signalement' => 'Signalement',
	'cfg_page_socialtags' => 'Social Tags',
	'cfg_page_sommaire_colonnes' => 'Choix des blocs latéraux',
	'cfg_page_sommaire_lateral' => 'Page d’accueil',
	'cfg_page_sommaire_noisettes' => 'Paramétrage des blocs latéraux',
	'cfg_page_sommaire_principal' => 'Page d’accueil',
	'cfg_page_spip400' => 'Spip 400',
	'cfg_page_spipdf' => 'spiPDF',
	'cfg_page_style' => 'Un peu de style',
	'cfg_page_textes' => 'Les textes',
	'chercher_parmi_les_signataires' => 'Chercher parmi les signataires',
	'choix_article' => 'Une liste d’articles',
	'choix_article_choix' => 'Choix des articles',
	'choix_article_choix_explication' => 'Numéros des articles séparés par des virgules',
	'choix_article_colonne_explication' => 'Entrer un chiffre de 1 à 3',
	'choix_article_fond' => 'Fond des articles',
	'choix_article_fond_explication' => 'soit une couleur (nom, code hexa ou code rgb)
	                                     <br/>soit un dégradé (exemple : linear-gradient(#DAE6F6, #336699) )
	                                     <br/>soit une image (exemple : url("squelettes/images/mon_image.jpg") no-repeat).',
	'choix_article_taillelogo' => 'Largeur maximale du logo des articles (en px)',
	'choix_article_taillelogo_explication' => '80 par défaut',
	'choix_blocs_lateraux' => 'Choix des blocs latéraux',
	'choix_deux' => 'Deux',
	'choix_groupe1' => 'Premier groupe de mots-clés',
	'choix_groupe2' => 'Deuxième groupe de mots-clés',
	'choix_groupe3' => 'Troisième groupe de mots-clés',
	'choix_trois' => 'Trois',
	'choix_une' => 'Une',
	'citations' => 'Citations',
	'clic_suite' => 'Cliquer pour lire la suite',
	'collegues' => 'Tous les collègues',
	'colonne_extra' => 'Colonne "extra" (par défaut à droite)',
	'colonne_laterale' => 'Colonne latérale',
	'colonne_nav' => 'Colonne "navigation" (par défaut à gauche)',
	'commentaires' => 'commentaires',
	'configurer_escal' => 'Configurer ESCAL',
	'contact_activer_champ' => 'Activer le champ',
	'contact_alerte_entete' => 'Votre saisie contient des oublis ou des erreurs !',
	'contact_alerte_mail' => 'Votre adresse e-mail n’est pas correcte.',
	'contact_alerte_message' => ' Vous n’avez pas composé de message ! Distrait(e) ?',
	'contact_bienvenue' => 'Pour contacter le référent technique,
                          <br>veuillez remplir tous les champs de ce formulaire.',
	'contact_cases' => 'Cases à cocher',
	'contact_cases_explication' => '(si vous cochez oui, pensez à renseigner au moins un libellé, sinon cela génère une erreur)',
	'contact_champ_mail' => 'Champ adresse mail expéditeur',
	'contact_destinataire' => 'Adresse du destinataire (par défaut, celle du webmestre)<br>
                                              Pour saisir plusieurs adresses, les séparer par une virgule <br>
                                              Exemple : tata@yoyo.fr, titi@paris.fr, toto@blague.fr',
	'contact_envoyer' => 'Envoyer',
	'contact_expediteur' => 'Case texte pour l’adresse électronique',
	'contact_explication' => 'Tous les champs de ce formulaire acceptent le multilinguisme avec la balise &lt;multi&gt;<br>exemple : &lt;multi&gt; [fr]Exemple [en]Example [es]Ejemplo &lt;/multi&gt;
		<br>Pour changer le texte d’un autre élément de la page Contact, utilisez les fichiers de langue.<br>
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article152">En savoir plus</a>',
	'contact_libelle' => 'Libellé',
	'contact_libelle1' => 'Libellé case 1',
	'contact_libelle2' => 'Libellé case 2',
	'contact_libelle3' => 'Libellé case 3',
	'contact_libelle4' => 'Libellé case 4',
	'contact_libelle5' => 'Libellé case 5',
	'contact_libelle_explication' => 'En laissant un libellé vide, l’item correspondant ne s’affiche pas.',
	'contact_libelle_gen' => 'Libellé général  (pas d’apostrophe ni de guillemets)',
	'contact_ligne' => 'En ligne',
	'contact_liste' => 'En liste',
	'contact_mail' => 'Votre e-mail :',
	'contact_message' => 'Votre message :',
	'contact_motif' => 'Motif de votre message :',
	'contact_motif1' => 'Libellé motif 1',
	'contact_motif2' => 'Libellé motif 2',
	'contact_motif3' => 'Libellé motif 3',
	'contact_motif4' => 'Libellé motif 4',
	'contact_motif5' => 'Libellé motif 5',
	'contact_motif_message' => 'Motif du message (boutons radio)',
	'contact_nom' => 'Votre nom :',
	'contact_obli' => 'Champ obligatoire',
	'contact_prenom' => 'Votre prénom :',
	'contact_presentation' => 'Présentation',
	'contact_retour' => 'Message de confirmation',
	'contact_retour_commentaire' => 'Votre message a bien été envoyé au webmestre du site qui vous répondra dès que possible à cette adresse :',
	'contact_retour_explication' => 'Vous pouvez personnaliser le message qui sera affiché à l’internaute après l’envoi du formulaire. Votre message sera suivi de l’adresse de l’internaute.',
	'contact_sup' => 'Ajout d’un champ supplémentaire',
	'contact_sup1' => 'Champ texte supplémentaire 1',
	'contact_sup2' => 'Champ texte supplémentaire 2',
	'contact_texte_accueil' => 'Texte à afficher en haut de page',
	'contact_texte_accueil_explication' => 'à la place de<br>
                    "Pour contacter le référent technique,veuillez remplir tous les champs de ce formulaire."<br>
                    (Pour ne rien afficher, entrez juste une espace)',
	'contenu_site' => 'Ce site compte : ',
	'copyright' => 'Tous droits réservés',

	// D
	'dans_site' => 'dans ce site',
	'deplier_replier' => 'Déplier et replier',
	'deplier_replier_explication' => 'En cochant "Oui" le bloc sera replié et un bouton permettra de le déplier et de le replier à nouveau.',
	'derniers_articles_bis' => 'Derniers articles bis',
	'derniers_articles_syndiques' => 'Derniers articles syndiqués',
	'derniers_articles_ter' => 'Derniers articles ter',
	'destinataire' => 'Destinataire',
	'details' => 'Avoir plus de détails',
	'doc_a_decouvrir' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article180" title="Voir la documentation">A découvrir</a>',
	'doc_acces_direct' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24" title="Voir la documentation">Accès direct</a>',
	'doc_actus' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article130" title="Voir la documentation">Actus</a>',
	'doc_annonce' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article174" title="Voir la documentation">Documentation</a>',
	'doc_annonce_defil' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article179" title="Voir la documentation">Documentation</a>',
	'doc_annuaire_auteurs' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article201" title="Voir la documentation">Annuaire auteurs et autrices</a>',
	'doc_articles_plus_vus' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article78" title="Voir la documentation">Articles les plus vus</a>',
	'doc_articles_rubrique' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article279" title="Voir la documentation">Articles de rubrique</a>',
	'doc_choix_blocs' => '
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article174" title="Voir la documentation">Annonce</a> : l’article le plus récent avec le mot-clé "annonce" dans un encart.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article179" title="Voir la documentation">Annonces défilantes</a> : tous les articles avec le mot-clé "annonce-defilant" défilent dans un encart.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article5" title="Voir la documentation">A la une</a> : Une boite avec des onglets pour les derniers articles du site, le plan du site, un article, les articles d’une rubrique...',
	'doc_derniers_art_syndic' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article291" title="Voir la documentation">Derniers articles syndiqués</a>',
	'doc_derniers_articles' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="Voir la documentation">Documentation</a>',
	'doc_derniers_articles2' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="Voir la documentation">Derniers articles</a>',
	'doc_derniers_comments' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article178" title="Voir la documentation">Derniers commentaires</a>',
	'doc_edito' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23" title="Voir la documentation">Edito</a>',
	'doc_evenements_a_venir' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article77" title="Voir la documentation">Evènements à venir</a>',
	'doc_identification' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article27" title="Voir la documentation">Identification</a>',
	'doc_meme_rubrique' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article28" title="Voir la documentation">Dans la même rubrique</a>',
	'doc_menuv1' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article102" title="Voir la documentation">Menu vertical dépliant</a>',
	'doc_menuv2' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article112" title="Voir la documentation">Menu vertical déroulant à droite</a>',
	'doc_mini_calendrier' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article81" title="Voir la documentation">Mini calendrier</a>',
	'doc_mot_cles_associes' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article131" title="Voir la documentation">Mots-clés associés</a>',
	'doc_navigation_mots_cles' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article312" title="Voir la documentation">Navigation par mots-clés</a>',
	'doc_noisettes' => '
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24" title="Voir la documentation">Accès direct</a> : article avec le mot-clé "acces-direct" pour des liens internes. Le titre du bloc sera celui de l’article.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article130" title="Voir la documentation">Actus</a> : les articles avec le mot-clé "actus" défilent.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article201" title="Voir la documentation">Annuaire auteurs et autrices</a> : la liste des auteurs et autrices et leur statut.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article279" title="Voir la documentation">Articles de rubrique</a> : affiche les articles de la rubrique avec le mot-clé "articles-de-rubrique".
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article78" title="Voir la documentation">Articles les plus vus</a> : affichage des articles les plus visités.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article200" title="Voir la documentation">Article libre 1 à 5</a> : article avec mot-clé "article-libreN" (N = 1 à 5) pour ce que vous voulez. Le titre du bloc sera celui de l’article.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article180" title="Voir la documentation">A découvrir</a> : un choix d’articles du site ou bien de la rubrique en cours et de ses sous-rubriques.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article191" title="Voir la documentation">A télécharger</a> : liste les documents de la rubrique (pas de ses articles) ou de l’article selon le contexte.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article190" title="Voir la documentation">Bloc à personnaliser</a> : articles avec le mot-clé "special". Le paramétrage de ce bloc se fait dans "Paramétrage des blocs latéraux -> Titres et contenus".
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article28" title="Voir la documentation">Dans la même rubrique</a> : derniers articles de la même rubrique que l’article en cours.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="Voir la documentation">Derniers articles</a> : derniers articles du site ou de la rubrique en cours et de ses sous-rubriques.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article291" title="Voir la documentation">Derniers articles syndiqués</a> : les derniers articles des sites syndiqués avec indication de leur site source.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article178" title="Voir la documentation">Derniers commentaires</a> : les derniers commentaires d’articles de tout le site.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23" title="Voir la documentation">Edito</a> : article avec mot-clé "edito". Le titre du bloc sera celui de l’article.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article77" title="Voir la documentation">Evènements à venir</a> : liste les évènements à venir.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article27" title="Voir la documentation">Identification</a> : pour s’identifier, donc.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article81" title="Voir la documentation">Mini calendrier</a> : utilise le plugin "Calendrier Mini" activé automatiquement avec Escal.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article131" title="Voir la documentation">Mots-clés associés</a> : navigation par mots-clés, si l’article en a au moins un.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article312" title="Voir la documentation">Navigation par mots-clés</a> : menu de navigation via les mots-clés d’un groupe.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article144" title="Voir la documentation">Photos au hasard</a> : les images des articles avec le mot-clé "photo-une" défilent.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article192" title="Voir la documentation">Rainette</a> : nécessite le plugin "Rainette". Infos sur la météo d’une ville au choix.<br>La configuration de ce bloc se fait dans la partie "Des plugins dans Escal".
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article198" title="Voir la documentation">Recherche multi-critères</a> : recherche par groupes de mots-clés et par mots-clés.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article181" title="Voir la documentation">Sites favoris</a> : les vignettes des sites référencés dans le secteur où on se trouve avec le mot-clé "favori" défilent.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article126" title="Voir la documentation">Statistiques</a> : tout un choix de statistiques sur le site.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article82" title="Voir la documentation">Sur le web</a> : le nom et les derniers articles des sites syndiqués classés par site source.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article197" title="Voir la documentation">Vidéos</a> : affiche les vidéos au format flv ou mp4 des articles avec le mot-clé "video-une".
		',
	'doc_noisettes2' => 'Certains blocs ne sont disponibles que sur certaines pages.',
	'doc_personnalise' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article190" title="Voir la documentation">Bloc à personnaliser</a>',
	'doc_photos_hasard' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article144" title="Voir la documentation">Photos au hasard</a>',
	'doc_rainette' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article192" title="Voir la documentation">Rainette</a>',
	'doc_recherche_multi' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article198" title="Voir la documentation">Recherche multi-critères</a>',
	'doc_site_escal' => '<a class="doc-escal spip_out" href="http://escal.edu.ac-lyon.fr" title="Aller sur le site d’ESCAL">Documentation : site d’ESCAL</a>',
	'doc_sites_favoris' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article181" title="Voir la documentation">Sites favoris</a>',
	'doc_stats' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article126" title="Voir la documentation">Statistiques</a>',
	'doc_sur_web' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article82" title="Voir la documentation">Sur le web</a>',
	'doc_telecharger_art_rub' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article191" title="Voir la documentation">A télécharger</a>',
	'doc_videos' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article197" title="Voir la documentation">Vidéos</a>',
	'document_trouve' => 'document trouvé',
	'documentation' => 'Documentation',
	'documentation_voir' => 'Voir la documentation',
	'documents' => 'Documents',
	'documents_trouves' => 'documents trouvés',
	'droite' => 'A droite',

	// E
	'edito' => 'Édito',
	'elements' => 'Éléments',
	'envoi_mail_message' => 'Message : ',
	'envoi_mail_motif' => 'Motif : ',
	'envoi_mail_nom' => 'Nom : ',
	'envoi_mail_prenom' => 'Prénom : ',
	'envoyer_message' => 'Envoyer un message à ',
	'erreur401' => 'Erreur 401',
	'erreur401_message' => 'Vous n’avez pas les autorisations suffisantes pour accéder à la page ou au document demandé... <br>
						Veuillez contacter le webmestre du site pour y accéder
						en utilisant la page [Contact|Page contact->@url@] de ce site.',
	'erreur404' => 'Erreur 404',
	'erreur404_message' => 'La page que vous demandez n’existe pas ou plus sur le site.
                    Si vous pensez qu’il s’agit d’une erreur dans le site,
                    vous pouvez signaler le problème au responsable du site
                    en utilisant la page [Contact|Page contact->@url@] de ce site.',
	'escal' => 'Escal',
	'evenement_associe' => 'Événement associé :',
	'evenements_ajouter' => 'Ajouter un évènement<br>(Accès réservé)',
	'evenements_associes' => 'Événements associés :',
	'evenements_non' => 'Pas d’évènements à venir',
	'evenements_venir' => 'Évènements à venir',

	// F
	'facebook_bouton' => 'Aspect du bouton',
	'facebook_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/Modeles-Facebook" title="Documentation">plugin "Modèles Facebook"</a>, le bouton "J’aime" apparaîtra dans le pied de page.
            <br><br>N’oubliez pas de configurer ce plugin avec l’adresse URL de votre page ou de votre profil Facebook.',
	'facebook_partager' => 'Bouton "Partager"',
	'facebook_plugin' => 'Modèles Facebook',
	'favicon' => 'Favicon',
	'favicon_choix1' => 'à partir du fichier favicon.ico.<br>Par défaut, celui d’Escal ou le vôtre, mis dans le dossier /squelettes (à la racine).',
	'favicon_choix2' => 'à partir du logo de votre site.<br>Il sera automatiquement transformé (taille, format) par Escal.',
	'favicon_choix3' => 'à partir d’une image de votre choix (gif, png, jpg ...) mise dans le dossier /squelettes.',
	'favicon_choix_fichier' => 'Le nom et l’extension du fichier sont à préciser ci-dessous.',
	'fleche' => 'flèche',
	'fois' => 'fois',
	'fonds_couleurs' => '<strong>Couleurs</strong> : on peut indiquer
	   <ul>
        <li>une couleur en code hexadécimal → exemple : #0000FF </li>
        <li>une couleur nommée → exemple : grey </li>
        <li>une couleur en code RGB → exemple : rgb(24,125,255) </li>
        <li>une couleur en code RGB avec transparence → exemple : rgba(24,125,255,0.5) </li>
        <li>ou toute autre code couleur : HSL, HSB, CMJN ... </li>
        <li>ou "transparent" pour supprimer la couleur</li>
        </ul>',
	'fonds_noisettes' => 'Les fonds et les textes des blocs',
	'fonds_noisettes_annonce' => 'Les annonces en une<br>Le surlignage des mots recherchés',
	'fonds_noisettes_apercu' => 'Aperçu de votre texte',
	'fonds_noisettes_articles' => 'Les articles<br>Le forum des articles<br>L’agenda<br>',
	'fonds_noisettes_bandeau' => 'Le bandeau',
	'fonds_noisettes_boutons' => 'Les boutons d’identification et de recherche<br>
		Le formulaire de contact<br>
		Les lignes paires des tableaux<br>
		Le portfolio<br>
		Le post-scriptum<br>
		Le forum du site, ...',
	'fonds_noisettes_contenu' => 'Le contenu des pages',
	'fonds_noisettes_contenu_transparent' => 'On peut aussi écrire "transparent", ce qui peut être sympa avec une image de fond.',
	'fonds_noisettes_derniers_articles' => 'Liste des derniers articles en une',
	'fonds_noisettes_fond' => 'Couleur du fond :',
	'fonds_noisettes_lien' => 'Couleur des liens :',
	'fonds_noisettes_lien_survol' => 'Couleur des liens survolés :',
	'fonds_noisettes_menuh' => 'Le menu horizontal<br>
		Les liens survolés du menu vertical déroulant à droite<br>
		L’encart de titre pour les rubriques et les articles<br>
		Le plan<br>
		Les en-têtes des forums',
	'fonds_noisettes_onglets_actifs' => 'Les onglets à la une si actifs',
	'fonds_noisettes_onglets_inactifs' => 'Les onglets à la une si inactifs',
	'fonds_noisettes_pied' => 'Le pied de page',
	'fonds_noisettes_rubriques' => 'La rubrique active du menu horizontal<br>
		Les lignes impaires des tableaux<br>
		La prévisualisation des forum, ...',
	'fonds_noisettes_survol' => 'Le survol des entrées du menu, des articles du bloc à la une, du fil d’Ariane, ...',
	'fonds_noisettes_texte' => 'Couleur du texte :',
	'fonds_noisettes_textes' => 'Le texte des blocs latéraux',
	'fonds_noisettes_titres' => 'Le titre et les bords des blocs latéraux<br>Le menu vertical dépliant<br>L’en-tête des tableaux, ...',
	'fonds_site' => 'Le fond du site',
	'fonds_site_aucune_image' => 'Aucune image de fond',
	'fonds_site_couleur' => 'Couleur de fond du site',
	'fonds_site_couleur_defaut' => '(par défaut : #DFDFDF)',
	'fonds_site_explication1' => 'Vos fonds doivent être placés dans <strong>/squelettes/images/fonds</strong> <br>
              Toutes les images .jpg .png .webp et .gif de ce répertoire seront listées. <br>
              Vous aurez même un aperçu de votre fond en direct !',
	'fonds_site_explication2' => 'Et si votre hébergeur ne permet pas de lister les fichiers présents,
                vous pouvez indiquer ici le chemin de votre fichier image.<br>
                exemple : <strong>images/fonds/fond.jpg</strong><br>
                si votre fichier <strong>fond.jpg</strong> est dans <strong>squelettes/images/fonds</strong>',
	'fonds_site_explication3' => 'Par défaut, l’image de fond est répétée horizontalement et/ou verticalement,
                ce qui est bien pour un fond dégradé par exemple.
                Mais si vous avez une image qui ne doit pas se répéter, cocher "oui" en dessous
                et votre image s’adaptera à l’écran de l’internaute et restera fixe. <br><br>
                - Taille conseillée pour la plupart des écrans : 2000px par 1300px<br>
                - Le poids de l’image doit être optimisé pour ne pas ralentir considérablement le chargement<br>
                Voir <a class="spip_out" href="http://www.alsacreations.com/astuce/lire/1216-arriere-plan-background-extensible.html" title="Plus d’infos">Un arrière-plan extensible intelligent</a>',
	'fonds_site_image' => 'Image de fond',
	'fonds_site_imagefondunique' => 'Adapter le fond à l’écran',
	'fonds_textes' => 'Autres textes',
	'fonds_textes_alerte' => 'Les alertes de la page contact',
	'fonds_textes_comment1' => 'en cas de champ non rempli par exemple',
	'fonds_textes_comment2' => 'identification, recherche, newsletter, inscription ...',
	'fonds_textes_couleur' => 'Les zones de saisie de SPIP',
	'fonds_textes_explication' => 'Il vous reste à choisir vos couleurs pour quelques textes spécifiques...',
	'fonds_textes_input' => 'Les zones de saisie de SPIP',
	'fonds_textes_liens' => 'Les liens',
	'fonds_textes_liens_survol' => 'Les liens survolés',
	'fonds_textes_liens_vus' => 'Les liens visités',
	'fonds_textes_texte' => 'Aperçu',
	'form_pet_envoi_mail_confirmation' => 'Un courrier électronique de confirmation vient d’être envoyé au webmestre du site afin de valider votre inscription. ',
	'form_recherche' => 'Formulaire de recherche',
	'form_recherche_item1' => 'Dans la barre d’outils',
	'form_recherche_item2' => 'Colonne de gauche',
	'form_recherche_item3' => 'Colonne de droite',
	'form_recherche_item4' => 'Nulle part',
	'form_recherche_item5' => 'Dans la barre de menu horizontal',
	'form_recherche_place' => 'Place du formulaire',
	'format' => 'Format :',
	'forum' => 'Forum',
	'forum_site_avant' => 'Avant de commencer',
	'forum_site_derniers' => 'Derniers<br>messages',
	'forum_site_reponses' => 'Réponses',
	'forum_site_sujets' => 'Sujets',
	'forum_trouve' => 'message de forum trouvé',
	'forums' => 'Messages des forums',
	'forums_trouves' => 'messages de forum trouvés',

	// G
	'galleria_caracteres' => 'Taille des caractères du titre de l’image',
	'galleria_couleur_encart' => 'Couleur de fond de l’encart d’information',
	'galleria_couleur_gallerie' => 'Couleur de fond de la galerie',
	'galleria_couleur_texte' => 'Couleur du texte de l’encart d’information',
	'galleria_explication' => 'Si vous avez activé <a class="spip_out" href="http://contrib.spip.net/Galleria-fr" title="Documentation">le plugin "Galleria"</a>, vous pouvez le paramétrer ici.',
	'galleria_plugin' => 'Galleria',
	'gauche' => 'A gauche',
	'gros_jour' => 'Plus grosse journée :',
	'groupe_affichage' => 'Groupe de mots-clés techniques utilisés dans le plugin Escal',
	'groupe_agenda_couleur' => 'Groupe de mots-clés pour la couleur des événements de l’agenda dans le plugin Escal',

	// H
	'haut' => 'En haut',
	'haut_page' => 'Haut de page',
	'hebergeur' => 'Hébergeur :',

	// I
	'identification' => 'Identification',
	'indentation' => 'Indentation de la première ligne des paragraphes (en px)',
	'info_lien_message' => '<strong>Attention ! </strong>Si votre message contient un lien, il devra être validé par le webmestre. Inutile de le poster à nouveau ;-)',
	'infos' => 'Infos',

	// L
	'layout' => 'Mise en page',
	'layout_choix' => 'Choix de la mise en page (layout)',
	'layout_documentation' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article1" title="Voir la documentation"><strong>Documentation</strong></a>',
	'layout_explication' => 'Attention à vos choix pour les largeurs : notez bien, par exemple, qu’un texte avec des lignes trop longues devient très difficile à lire',
	'layout_fixe' => '<strong>Layout fixe</strong><br>par défaut : layout de 1200px et colonnes de 250px',
	'layout_fixe_largeur' => 'Largeur du layout (en px)',
	'layout_fixe_si' => '<strong>layout fixe</strong> ',
	'layout_fluide' => '<strong>Layout fluide</strong><br>s’adaptant à la largeur de l’écran (recommandé) ',
	'layout_fluide_largeur' => 'Largeur maximum du layout (en px)',
	'layout_fluide_si' => '<strong>layout fluide</strong> ',
	'layout_largeur' => 'Réglage des largeurs',
	'layout_largeur_colonnes' => 'Largeur des colonnes latérales fixes (en px)',
	'layout_mixte' => '<strong>Layout mixte</strong><br>partie centrale fluide et colonnes latérales fixes ',
	'layout_mixte_si' => '<strong>layout mixte</strong> ',
	'le' => 'le',
	'licence_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/Une-licence-pour-un-article" title="Documentation">plugin "Licence"</a>, vous n’avez rien d’autre à faire, l’icône correspondant à la licence que vous avez choisi pour un article s’affichera automatiquement en haut de l’article.',
	'lien_agenda' => '► voir en pleine page',
	'lien_agenda_title' => 'Afficher l’agenda en pleine page',
	'lien_rapide_pied' => 'Aller au pied de page',
	'liens_sociaux_espace' => 'Afficher les liens sociaux dans la barre d’outils',
	'liens_sociaux_explication' => 'Si vous avez activé le <a class="spip_out" href="https://contrib.spip.net/Liens-sociaux" title="Documentation">plugin "Liens sociaux"</a>, vous pouvez le paramétrer ici.',
	'liens_sociaux_pied' => 'Afficher les liens sociaux dans le pied de page',
	'liens_sociaux_plugin' => 'Liens sociaux',
	'liens_sociaux_texte' => 'Texte éventuel à afficher : ',
	'lire_article' => 'Lire l’article ...',
	'lire_suite' => 'Lire la suite ...',
	'lire_tout' => 'Lire tout l’article ',

	// M
	'mentions' => 'Mentions légales',
	'mentions_donnees' => 'Données personnelles et cookies',
	'mentions_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/Mentions-Legales" title="Documentation">plugin "Mentions légales"</a>, vous pouvez faire apparaître le lien vers la page dédiée dans le pied de page du site.',
	'mentions_texte' => '
<!-- Texte à modifier en complétant les données_à_remplacer :
###nom_du_directeur_de_publication###
###adresse_postale### (2 occurrences)
###nom_de_l_hébergeur###
###lien_vers_le_site_de_l_hébergeur###
###courriel_de_l_hébergeur###
 -->

L’affichage des mentions légales est une obligation aussi bien pour les sites internet professionnels que pour les sites personnels, en vertu de la loi du 21 juin 2004 pour la confiance dans l’économie numérique (LCEN). En vertu de cette loi, voici les coordonnées de l’éditeur et du prestataire qui accueille le site @url_du_site@

<hr>

{{{Propriétaire / Éditeur / Directeur de publication}}}

@url_du_site@ est un site web édité par {{@nom_du_site@}} et publié sous la direction de ###nom_du_directeur_de_publication###. L’ensemble du site @url_du_site@, pages et contenus, est la propriété de {{@nom_du_site@}}.
Contact administratif du site @url_du_site@ :
###adresse_postale###
 [->@courriel_de_contact@]

{{{Hébergeur}}}

@url_du_site@ est hébergé par ###nom_de_l_hébergeur###.
Pour plus d’informations, reportez-vous au site [->###lien_vers_le_site_de_l_hébergeur###]
Pour les contacter, utilisez l’adresse courriel [->###courriel_de_l_hébergeur###]

{{@nom_du_site@}} décline toute responsabilité quant aux éventuelles interruptions du site @url_du_site@ et de ses services.

{{{Concepteur / Créateur}}}

L’ensemble du site {{@nom_du_site@}} : conception, charte graphique et applications, a été créé et développé par {{@nom_du_site@}} avec [SPIP->https://www.spip.net] sous habillage [Escal->https://escal.edu.ac-lyon.fr].
Pour les contacter, utilisez l’adresse courriel :  [->@courriel_de_contact@]

{{{Fonctionnement du site}}}

Les pages et le contenu de ce site sont générés par le logiciel libre SPIP, distribué sous licence GNU / GPL (Licence Publique Générale). Vous pouvez l’utiliser librement pour réaliser votre propre site web. Pour plus d’informations, reportez-vous au site [spip.net->https://www.spip.net].

SPIP, Système de Publication pour l’Internet
Copyright © 2001-2018, Arnaud Martin, Antoine Pitrou, Philippe Rivière et Emmanuel Saint-James.

{{{Contenus et droits de reproduction}}}

En application des articles L. 111-1 et L. 123-1 du Code de la Propriété Intellectuelle, l’ensemble des contenus de ce site (textes, images, vidéos et tout média en général), sauf mention contraire explicite, est protégé par le droit d’auteur. La reproduction, même partielle, des contenus des pages de ce site sans accord préalable de @url_du_site@ est strictement interdite.

{{{Acceptation de fait}}}

En utilisant @url_du_site@, l’internaute prend note et accepte les conditions ici énumérées

{{{Abus}}}

En application de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, pour signaler un contenu litigieux ou si vous êtes victime d’une utilisation frauduleuse du site @url_du_site@, merci de contacter l’administrateur du site à l’adresse courriel :  [->@courriel_de_contact@]

{{{Collecte et traitement de données personnelles}}}

Les informations éventuellement recueillies par @url_du_site@ ne sont jamais communiquées à des tiers. Sauf cas particulier, ces informations proviennent de l’enregistrement volontaire d’une adresse courriel fournie par l’internaute, lui permettant de recevoir une documentation ou une newsletter, de demander son inscription dans le comité rédactionnel du site ou de se renseigner sur un point quelconque.
En application de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, l’internaute dispose d’un droit d’accès, de rectification et de suppression des informations personnelles le concernant stockées par @url_du_site@, qu’il peut exercer à tout moment auprès de l’administrateur de @url_du_site@. Pour une demande de ce type, veuillez adresser un courrier à
###adresse_postale###
ou contacter l’adresse courriel :  [->@courriel_de_contact@]

{{{Cookies}}}

Certaines informations nominatives et certains marqueurs témoins peuvent être enregistrées par @url_du_site@ sur l’ordinateur personnel de l’internaute sans expression de la volonté de ce dernier ("cookies" ou "applets JAVA").
Sauf indication contraire explicite, ces procédés techniques ne sont pas indispensables au bon fonctionnement de @url_du_site@ et leur utilisation peut bien entendu être désactivée dans le navigateur utilisé par l’internaute sans que sa navigation en soit affectée.
Aucune de ces informations n’est conservée par le site après déconnexion de l’internaute.
Il est utile de préciser ici que les accès aux différents espaces privés du site peuvent nécessiter l’acceptation par l’internaute du dépôt d’un cookie sur son ordinateur personnel pour une question de sécurité.

{{{Limitation de responsabilité}}}

Le site @url_du_site@ comporte des informations mises à disposition par des sociétés externes ou des liens hypertextes vers d’autres sites ou d’autres sources externes qui n’ont pas été développés par {{@nom_du_site@}}.
Les comportements des sites cibles, éventuellement malveillants, ne sauraient être rattachés à la responsabilité de {{@nom_du_site@}}.
Plus généralement, le contenu mis à disposition sur ce site est fourni à titre informatif.
Il appartient à l’internaute d’utiliser ces informations avec discernement.
La responsabilité de {{@nom_du_site@}} ne saurait être engagée de fait quant aux informations, opinions et recommandations formulées par des tiers.
Dans la mesure où @url_du_site@ ne peut contrôler ces sites ou sources externes, {{@nom_du_site@}} ne peut être tenu pour responsable de la mise à disposition de ces sites ou sources externes et ne peut supporter aucune responsabilité quant aux contenus, publicités, produits, services ou tout autre matériel disponible sur ou à partir de ces sites ou sources externes.
De plus, {{@nom_du_site@}} ne pourra être tenu responsable de tous dommages ou pertes avérés ou allégués consécutifs ou en relation avec l’utilisation ou le fait d’avoir fait confiance au contenu, à des biens ou des services disponibles sur ces sites ou sources externes.
	',
	'menu' => 'Au menu',
	'menu_horizontal' => 'Menu horizontal',
	'menu_horizontal_accueil' => 'Pour le lien vers l’accueil',
	'menu_horizontal_affichage' => 'Affichage du menu horizontal',
	'menu_horizontal_choix' => 'Choix du menu horizontal',
	'menu_horizontal_choix_bloc' => 'Méga menu déroulant',
	'menu_horizontal_choix_liste' => 'Listes déroulantes',
	'menu_horizontal_fixer' => 'Fixer le menu en haut de page lors du scrolling vers le bas',
	'menu_horizontal_hauteur' => 'Hauteur des logos de rubrique (en px)',
	'menu_horizontal_hauteur_explication' => 'Veillez à ce que vos logos aient une hauteur au moins égale à la valeur choisie',
	'menu_horizontal_logo' => 'Affichage du logo de la rubrique s’il existe',
	'menu_horizontal_logo_accueil' => 'Affichage du logo',
	'menu_horizontal_notice' => '<strong>Attention</strong> : ne pas mettre ces 2 valeurs à "non" pour ne pas vous retrouver avec des entrées de menu vides',
	'menu_horizontal_secteur' => 'Pour les rubriques de premier niveau (= secteur)',
	'menu_horizontal_secteur_explication' => '→ s’applique aussi au menu sur mobile',
	'menu_horizontal_titre' => 'Affichage du titre de la rubrique (si au moins un article publié dans celle-ci)',
	'menu_horizontal_titre_accueil' => 'Affichage du mot "Accueil"',
	'menu_vertical_depliant' => 'Menu vertical dépliant',
	'menu_vertical_deroulant' => 'Menu vertical déroulant à droite',
	'metadonnees' => 'Métadonnées',
	'metadonnees_explication' => '<ul>Si vous avez activé le plugin <a class="spip_out" href="https://contrib.spip.net/Metadonnees-Photo" title="Documentation">Metadonnées</a> vous pouvez choisir d’afficher
			<li>les métadonnées de vos photos</li>
			<li>ou bien leur titre</li>
			<li>ou bien les 2</li>
			</ul>',
	'metadonnees_titre' => 'Titre et Métadonnées',
	'mini_calendrier' => 'Mini calendrier',
	'mot_acces_direct' => 'pour choisir l’article qui sera affiché dans le bloc "Accès direct"',
	'mot_accueil' => 'pour choisir un article affiché en onglet de la page d’accueil',
	'mot_actus' => 'pour choisir les articles qui seront affichés dans le bloc "Actus" ',
	'mot_agenda' => 'pour choisir les articles ou la ou les rubriques dont les articles seront affichés dans l’agenda',
	'mot_annonce' => 'pour choisir l’article dont le texte sera affiché dans le bloc "Annonce" de la page d’accueil',
	'mot_annonce_defilant' => 'pour choisir les articles dont le texte sera affiché dans le bloc "Annonces défilantes" de la page d’accueil',
	'mot_annuaire' => 'pour choisir l’article qui sera utilisé par la page annuaire.html',
	'mot_archive' => 'pour choisir la rubrique dont un article pris au hasard sera affiché dans l’onglet "Article archive" de la page d’accueil',
	'mot_article_libre1' => 'pour choisir l’article dont le contenu sera affiché dans le bloc "Article libre 1"',
	'mot_article_libre2' => 'pour choisir l’article dont le contenu sera affiché dans le bloc "Article libre 2"',
	'mot_article_libre3' => 'pour choisir l’article dont le contenu sera affiché dans le bloc "Article libre 3"',
	'mot_article_libre4' => 'pour choisir l’article dont le contenu sera affiché dans le bloc "Article libre 4"',
	'mot_article_libre5' => 'pour choisir l’article dont le contenu sera affiché dans le bloc "Article libre 5"',
	'mot_article_sans_date' => 'pour supprimer l’affichage des dates de publication et de modification d’un article',
	'mot_articles_rubrique' => 'pour choisir la rubrique dont les articles seront affichés dans le bloc "Articles de rubrique"',
	'mot_chrono' => 'pour afficher les articles d’une rubrique dans les menus en ordre antéchronologique, comportement non transmis aux rubriques-filles',
	'mot_citations' => 'pour choisir l’article qui servira de réservoir pour les citations dans le pied de page',
	'mot_edito' => 'pour choisir l’article qui sera affiché dans le bloc "Edito" ',
	'mot_favori' => 'pour choisir les sites dont les vignettes seront affichées dans le bloc "Sites favoris" ',
	'mot_forum' => 'pour choisir le secteur qui sera utilisé pour le forum du site',
	'mot_invisible' => 'pour cacher une rubrique et ses sous-rubriques de tous les menus, du plan du site et des derniers articles',
	'mot_mentions' => 'pour choisir l’article qui sera affiché en tant que mentions légales',
	'mot_mon_article' => 'pour choisir un article qui sera affiché dans un onglet du bloc central de la page d’accueil',
	'mot_mon_article2' => 'pour choisir un deuxième article qui sera affiché dans un onglet du bloc central de la page d’accueil',
	'mot_mon_article3' => 'pour choisir un troisième article qui sera affiché dans un onglet du bloc central de la page d’accueil',
	'mot_pas_decouvrir' => 'pour choisir les rubriques et les articles à exclure de l’affichage dans le bloc "A découvrir" si on choisit "dans tout le site"',
	'mot_pas_menu' => 'pour ne pas afficher une rubrique ou un article dans le menu horizontal',
	'mot_pas_menu_vertical' => 'pour ne pas afficher une rubrique ou un article dans les menus verticaux',
	'mot_pas_plan' => 'pour ne pas afficher une rubrique (et ses articles) ou des articles dans le bloc "Plan du site" de la page d’accueil',
	'mot_pas_une' => 'pour ne pas afficher une rubrique (et ses articles) ou des articles dans les onglets "Derniers articles" de la page d’accueil',
	'mot_photo_une' => 'pour choisir les articles dont les images seront affichées dans le bloc "Quelques images au hasard" ',
	'mot_pleine_page' => 'pour choisir les articles qui seront affichés en pleine page sans aucun bloc latéral',
	'mot_popup' => 'pour choisir l’article qui sera affiché dans la boîte modale à l’ouverture du site (si activé)',
	'mot_rubrique_onglet' => 'pour choisir une rubrique qui sera affichée dans les onglets en page d’accueil',
	'mot_rubrique_onglet2' => 'pour choisir une deuxième rubrique qui sera affichée dans les onglets en page d’accueil',
	'mot_rubrique_onglet3' => 'pour choisir une troisième rubrique qui sera affichée dans les onglets en page d’accueil',
	'mot_rubrique_onglet4' => 'pour choisir une quatrième rubrique qui sera affichée dans les onglets en page d’accueil',
	'mot_rubrique_onglet5' => 'pour choisir une cinquième rubrique qui sera affichée dans les onglets en page d’accueil',
	'mot_site_exclu' => 'pour exclure des sites dans le bloc "Sur le web"',
	'mot_special' => 'pour choisir la rubrique et/ou les articles qui seront affichés dans le bloc à personnaliser',
	'mot_texte2colonnes' => 'Pour afficher un texte d’article en 2 colonnes dans une page article',
	'mot_video_une' => 'pour choisir les articles dont les vidéos seront affichées dans le bloc "Vidéo"',
	'mots_clefs' => 'Les mots-clés',
	'mots_clefs_associes' => 'Mots-clés associés',
	'mots_cles' => 'mots-clés',
	'moyenne_jours' => 'jours :',
	'moyenne_visites' => 'Moyenne depuis',
	'multilinguisme_ariane' => 'Supprimer aussi le secteur dans les fils d’Ariane',
	'multilinguisme_code' => 'Codes des langues',
	'multilinguisme_drapeau' => 'Drapeaux',
	'multilinguisme_explication1' => 'Par défaut, Escal affiche les rubriques de 1er niveau (= secteur) dans les menus.<br>
            Si on veut avoir la même structure dans plusieurs langues, on va donc dupliquer
            tous les secteurs dans toutes les langues. On se retrouve vite avec un grand nombre de secteurs.<br>
            On peut donc préférer créer un secteur par langue mais dans ce cas, seul le secteur de langue apparaît dans le menu.<br>
            Cette option permet donc de faire apparaître les rubriques de deuxième niveau dans les menus à la place des secteurs.
            Option valable pour le menu horizontal mais aussi pour les 2 menus verticaux.',
	'multilinguisme_explication2' => 'Par défaut, ce sont les codes de chaque langue utilisée qui s’affichent mais on peut les remplacer par le nom entier de la langue ou par de petits drapeaux <br>
            Mais attention, l’anglais n’est pas parlé qu’au Royaume-Uni, l’espagnol n’est pas parlé qu’en Espagne, ...
            il est donc conseillé d’utiliser les codes de langue',
	'multilinguisme_niveau' => 'Menus avec rubriques de deuxième niveau',
	'multilinguisme_nom' => 'Noms des langues',

	// N
	'nav_mot_cle' => 'Navigation par mots-clés',
	'noir' => 'noir',
	'noisette_article' => 'Blocs spécifiques à la page article',
	'noisette_commun' => 'Blocs communs à plusieurs pages',
	'noisette_perso' => 'Bloc à personnaliser',
	'noisette_rubrique' => 'Blocs spécifiques à la page rubrique',
	'noisettes_acces_direct_explication' => 'Le titre de ce bloc est donné par le titre de l’article avec le mot-clé "acces-direct". Article dont il faut décliner les traductions sur un site multilingue.',
	'noisettes_actus_enlever' => 'Enlever le bloc si pas d’actus',
	'noisettes_actus_tempo' => 'Temporisation entre chaque actu (en secondes)',
	'noisettes_annuaire_admin' => 'Couleur pour les admins',
	'noisettes_annuaire_icones' => 'Affichage des icônes',
	'noisettes_annuaire_lien' => 'Affichage du lien vers le trombinoscope',
	'noisettes_annuaire_pagination' => 'Nombre de personnes par page de pagination',
	'noisettes_annuaire_redacteur' => 'Couleur pour les rédacteurs et rédactrices',
	'noisettes_annuaire_visiteur' => 'Couleur pour les visiteurs et visiteuses',
	'noisettes_calendrier_ajouter' => 'Lien "Ajouter un événement" si la personne est authentifiée',
	'noisettes_calendrier_couleur' => 'Couleur par défaut des événements',
	'noisettes_calendrier_events' => 'Liste des événement à venir en dessous',
	'noisettes_calendrier_explication2' => 'Ce choix s’appliquera aussi au bloc "Évènements à venir" ',
	'noisettes_calendrier_lien_agenda' => 'Titre avec lien vers l’agenda en pleine page',
	'noisettes_calendrier_pagination' => 'Nombre d’événements par page de pagination',
	'noisettes_calendrier_renvoi' => 'Pour les événements avec utilisation du plugin Agenda, le lien renvoie vers',
	'noisettes_calendrier_renvoi_aucun' => 'aucun lien',
	'noisettes_calendrier_renvoi_evenement' => 'l’évènement',
	'noisettes_decouvrir_hasard' => 'Nombre d’articles pris au hasard',
	'noisettes_decouvrir_moins_vus' => 'Nombre d’articles les moins visités',
	'noisettes_decouvrir_ou' => 'Où prendre les articles ? ',
	'noisettes_decouvrir_plus_vus' => 'Nombre d’articles les plus visités',
	'noisettes_decouvrir_recents' => 'Nombre d’articles les plus récents',
	'noisettes_decouvrir_rubrique' => 'Uniquement dans la rubrique en cours et ses sous-rubriques',
	'noisettes_decouvrir_site' => 'Dans tout le site (on peut exclure des articles ou des rubriques avec le mot-clé "pas-a-decouvrir")',
	'noisettes_edito_explication' => 'Le titre de ce bloc est donné par le titre de l’article avec le mot-clé "edito". Article dont il faut décliner les traductions sur un site multilingue.',
	'noisettes_explication' => '<strong>Titres des blocs</strong><br><br>
              Dans le cas d’un <strong>site multilingue</strong>, si vous voulez changer le titre d’un bloc
              tout en conservant le multilinguisme, vous devez écrire ce titre en suivant cet exemple :<br><br>
              &lt;multi&gt; [fr]Exemple [en]Example [de]Beispiel [es]Ejemplo [it]Esempio &lt;/multi&gt;<br><br>
              En laissant la case vide, Escal utilisera le titre par défaut.',
	'noisettes_forum_colonne' => 'Place de la colonne latérale',
	'noisettes_forum_explication' => 'Une seule colonne latérale car le forum occupe le reste de l’espace',
	'noisettes_identification_explication' => 'Le titre de ce bloc n’est pas modifiable.',
	'noisettes_identification_redacteur' => 'Afficher le formulaire d’inscription de nouveaux <strong>rédacteurs ou rédactrices</strong><br>
			(Il faut avoir coché "Accepter les inscriptions" dans l’espace privé : Configuration - Interactivité)',
	'noisettes_identification_redacteur_label' => 'Rédacteurs ou rédactrices',
	'noisettes_identification_visiteur' => 'Afficher le formulaire d’inscription de nouveaux <strong>visiteurs ou visiteuses</strong><br>
			(Il faut avoir coché "Accepter l’inscription de visiteurs ou visiteuses du site public" dans l’espace privé : Configuration - Interactivité)',
	'noisettes_identification_visiteur_label' => 'Visiteurs ou visiteuses',
	'noisettes_multi_choix' => '<strong>Choix des groupes de mots-clés</strong>',
	'noisettes_multi_groupe1' => 'Premier groupe (obligatoire)',
	'noisettes_multi_groupe2' => 'Deuxième groupe (facultatif)',
	'noisettes_multi_groupe3' => 'Troisième groupe (facultatif)',
	'noisettes_multi_groupe4' => 'Quatrième groupe (facultatif)',
	'noisettes_multi_groupe5' => 'Cinquième groupe (facultatif)',
	'noisettes_multi_groupe6' => 'Sixième groupe (facultatif)',
	'noisettes_multi_groupe7' => 'Septième groupe (facultatif)',
	'noisettes_multi_groupe8' => 'Huitième groupe (facultatif)',
	'noisettes_multi_groupe9' => 'Neuvième groupe (facultatif)',
	'noisettes_multi_non' => 'Non utilisé',
	'noisettes_nav_groupe' => 'Groupe de mot à utiliser',
	'noisettes_nav_nombre' => 'Nombre maximum d’articles à afficher pour chaque mot-clé',
	'noisettes_perso_choix2' => 'Pour les 2 derniers choix d’affichage : ',
	'noisettes_perso_choix3' => 'Pour les 3 premiers choix d’affichage : ',
	'noisettes_perso_choix4' => 'Pour le quatrième choix d’affichage : ',
	'noisettes_perso_liste1' => 'Liste avec titre (et lien), date de publication et auteur ou autrice',
	'noisettes_perso_liste2' => 'Liste avec titre (et lien) seul',
	'noisettes_perso_liste3' => 'Liste avec titre (et lien) et début du texte',
	'noisettes_perso_liste4' => 'Liste défilante avec titre (et lien) et début du texte',
	'noisettes_perso_tempo' => 'Temporisation entre chaque article (en secondes)',
	'noisettes_photos_lien' => 'Lien vers l’article source de la photo',
	'noisettes_photos_tempo' => 'Temporisation entre chaque image (en secondes)',
	'noisettes_sites_exclus' => 'Exclure les sites avec le mot-clé "favori"',
	'noisettes_sites_exclus_explication' => 'puisque déjà présents dans le bloc "Sites favoris" si vous l’utilisez',
	'noisettes_sites_tempo' => 'Temporisation entre chaque logo de site (en secondes)',
	'noisettes_stats_affiche' => 'Affichage ...',
	'noisettes_stats_articles' => '...nombre d’articles du site',
	'noisettes_stats_auteurs' => '...nombre d’auteurs et d’autrices',
	'noisettes_stats_choix' => 'Choix des statistiques à présenter :',
	'noisettes_stats_comments' => '...nombre de commentaires',
	'noisettes_stats_et' => 'et du...',
	'noisettes_stats_jour' => 'Nombre de visiteurs ou visiteuses du jour',
	'noisettes_stats_libelle' => '...du libellé "Ce site compte"',
	'noisettes_stats_ligne' => 'Nombre de visiteurs ou visiteuses en ligne',
	'noisettes_stats_max' => 'Plus grosse journée',
	'noisettes_stats_mots' => '...nombre de mots-clés',
	'noisettes_stats_moyenne' => 'Moyenne des visites',
	'noisettes_stats_pages' => 'Nombre total de pages visitées',
	'noisettes_stats_periode' => 'sur une période de ... (en jours)',
	'noisettes_stats_rubriques' => '...nombre de rubriques',
	'noisettes_stats_sites' => '...nombre de sites référencés',
	'noisettes_stats_sujets' => 'Nombre de sujets du forum',
	'noisettes_stats_visites' => 'Nombre total de visites',
	'noisettes_sur_web_syndic' => 'Uniquement les sites syndiqués',
	'nombre_actus' => 'Nombre d’actus',
	'nombre_articles' => 'Nombre d’articles',
	'nombre_articles_pagination' => 'Nombre d’articles par page de pagination',
	'nombre_articles_sites' => 'Nombre d’articles par sites',
	'nombre_caracteres' => 'Nombre de caractères affichés pour le texte du/des articles',
	'nombre_colonnes' => 'Nombre de colonnes',
	'nombre_colonnes2' => 'Nombre de colonnes (pour les articles et les sous-rubriques)',
	'nombre_comments' => 'Nombre de commentaires',
	'nombre_comments_pagination' => 'Nombre de commentaires par page de pagination',
	'nombre_images' => 'Nombre d’images',
	'nombre_mots' => 'Nombre de mots-clés à afficher',
	'nombre_sites' => 'Nombre de sites',
	'nombre_sujets_pagination' => 'Nombre de sujets par page de pagination',
	'nuage' => 'Un nuage de mots-clés',

	// O
	'onglets_art_accueil' => 'Article d’accueil',
	'onglets_art_accueil_explication' => 'L’article affiché sera celui qui a le mot-clé "accueil" sinon ce sera le dernier publié.<br>
                    Le titre de l’article s’affiche dans l’onglet.',
	'onglets_art_accueil_forum' => 'Afficher le forum de l’article d’accueil',
	'onglets_art_archive' => 'Article archive',
	'onglets_art_archive_explication' => 'L’article affiché sera un article pris au hasard dans la rubrique ayant le mot-clé "archive" ou dans l’une de ses sous-rubriques.<br>
                    Le titre de l’onglet est "Souvenez-vous !" avec multilinguisme pris en charge dans les langues disponibles avec Escal. ',
	'onglets_art_archive_forum' => 'Afficher le forum de l’article archive',
	'onglets_arts_mot' => 'Articles avec mot-clé',
	'onglets_arts_mot_choix' => 'Choix du mot-clé',
	'onglets_arts_mot_explication1' => 'Le titre de cet onglet sera celui du mot-clé',
	'onglets_arts_mot_explication2' => '<strong> <a href="#articles_mots_cles">→ Paramétrer</a></strong>',
	'onglets_bis_pagin_defaut' => '0 | 10 | 20 | 30',
	'onglets_bis_pagin_page' => '1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | ...',
	'onglets_bis_pagin_pps' => '< 1 | 2 | 3	| 4 | 5 | 6 | >',
	'onglets_bis_pagin_prive' => '0 | 10 | 20 | 30 | Tout afficher',
	'onglets_bis_pagin_ps' => 'page précédente | page suivante',
	'onglets_bis_pagination' => 'Modèle de pagination (essayez-les !)',
	'onglets_choix' => 'Choix des onglets',
	'onglets_cinq' => 'Cinquième onglet',
	'onglets_derniers_article' => 'Paramétrage de chaque article',
	'onglets_derniers_arts_explication1' => '<ul>Ils peuvent s’afficher de 3 façons :
				<li><strong>Derniers articles <a href="#derniers_articles">→ Paramétrer</a><br></strong></li>
				<li><strong>Derniers articles bis <a href="#derniers_articles_bis">→ Paramétrer</a><br></strong></li>
				<li><strong>Derniers articles ter <a href="#derniers_articles_ter">→ Paramétrer</a></strong></li>
                    </ul>',
	'onglets_derniers_arts_explication2' => 'Le titre de cet onglet sera "Les derniers articles" sauf si vous lui donnez un autre titre ci-dessous.',
	'onglets_derniers_arts_explication3' => 'Pour "Derniers articles" et "Derniers articles ter" les articles avec le mot-clé "annonce" ou "annonce-defilant" seront exclus de la liste, ceci pour éviter les doublons, sauf si les blocs "Annonce" et "Annonces défilantes" ne sont pas utilisés. ',
	'onglets_derniers_arts_explication4' => 'Les articles avec le mot-clé "annonce" ou "annonce-defilant" seront exclus de la liste, ceci pour éviter les doublons, sauf si les blocs "Annonce" et "Annonces défilantes" ne sont pas utilisés. ',
	'onglets_derniers_autres' => 'Les autres articles',
	'onglets_derniers_exergue' => 'L’article en exergue',
	'onglets_derniers_presentation' => 'Présentation générale',
	'onglets_deux' => 'Deuxième onglet',
	'onglets_monart' => 'Mon article',
	'onglets_monart2' => 'Mon article 2',
	'onglets_monart2_titre' => 'Titre de l’onglet<br>"Mon article 2"',
	'onglets_monart3' => 'Mon article 3',
	'onglets_monart3_titre' => 'Titre de l’onglet<br>"Mon article 3"',
	'onglets_monart4' => 'Mon article 4',
	'onglets_monart4_titre' => 'Titre de l’onglet<br>"Mon article 4"',
	'onglets_monart5' => 'Mon article 5',
	'onglets_monart5_titre' => 'Titre de l’onglet<br>"Mon article 5"',
	'onglets_monart_explication' => '<strong>Mon article</strong> ou <strong>Mon article 2</strong> ou <strong>Mon article 3</strong> ou <strong>Mon article 4</strong> ou <strong>Mon article 5</strong> sont des articles ayant respectivement les mots-clés "mon-article", "mon-article2", "mon-article3", "mon-article4" et "mon-article5".<br><br>
                    Le titre de l’onglet sera celui de l’article sauf si vous lui donnez un titre différent ci-dessous. Seuls le texte et le PS sont affichés.',
	'onglets_monart_titre' => 'Titre de l’onglet<br>"Mon article"',
	'onglets_nombre_articles' => 'Nombre total d’articles à afficher',
	'onglets_nombre_articles_exergue' => 'Nombre d’articles à afficher, y compris l’article en exergue',
	'onglets_parametrage' => 'Paramétrage des onglets',
	'onglets_placer' => 'Placer les onglets en haut de page au clic sur l’un d’eux',
	'onglets_quatre' => 'Quatrième onglet',
	'onglets_rien' => 'Ne pas afficher cet onglet',
	'onglets_rub2' => 'Rubrique 2',
	'onglets_rub3' => 'Rubrique 3',
	'onglets_rub4' => 'Rubrique 4',
	'onglets_rub5' => 'Rubrique 5',
	'onglets_rub_auteur' => 'afficher l’auteur ou autrice des articles',
	'onglets_rub_descriptif' => 'afficher le descriptif de la rubrique',
	'onglets_rub_explication' => '<strong>Rubrique</strong> ou <strong>Rubrique 2</strong> ou <strong>Rubrique 3</strong> ou <strong>Rubrique 4</strong> ou <strong>Rubrique 5</strong><br>
				permettent d’afficher les articles de la rubrique qui a le mot-clé "RubriqueOnglet" ou "RubriqueOngletN" (N = 2 à 5).<br><br>
                    Le titre de la rubrique est affiché dans l’onglet et les articles de cette rubrique et de ses sous-rubriques sont affichés dessous.',
	'onglets_rub_logo' => 'choisir la taille du logo des articles (en px)',
	'onglets_rub_pagin' => 'définir le pas de pagination pour les articles ',
	'onglets_rub_texte' => 'afficher le texte de la rubrique',
	'onglets_sur_web_explication' => 'Cet onglet affichera la liste des articles syndiqués sur votre site en indiquant le site source et sa date de parution.',
	'onglets_sur_web_explication2' => 'Le titre de l’onglet est "Sur le web" sauf si vous lui donnez un titre différent ci-dessous.',
	'onglets_sur_web_pagin' => 'Pas de pagination pour ces articles',
	'onglets_ter_mot_exergue' => 'Affichage d’un article en exergue',
	'onglets_trois' => 'Troisième onglet',
	'onglets_un' => 'Premier onglet',

	// P
	'page_accueil' => 'La page d’accueil',
	'page_article' => 'La page article',
	'page_auteur' => 'page de l’auteur ou autrice',
	'page_autres' => 'Les autres pages',
	'page_autres_explication' => 'Pour les pages contact, annuaire, auteur, recherche, 404, ...',
	'page_contact' => 'Page contact',
	'page_contact2' => 'Page contact de',
	'page_contact3' => 'La page contact',
	'page_forum' => 'Le forum du site',
	'page_recherche' => 'La page recherche',
	'page_rubrique' => 'La page rubrique',
	'pages' => 'pages',
	'pages_vues' => 'Nombre de pages visitées : ',
	'par_defaut' => 'par défaut : ',
	'photos_hasard' => 'Quelques photos<br>au hasard',
	'photos_hasard2' => 'Photos au hasard',
	'pied' => 'Pied de page',
	'pied_autres_mentions' => 'Autres mentions',
	'pied_citations_activer' => 'Activer les citations',
	'pied_citations_explication' => 'Vous pouvez afficher des citations à gauche du pied de page. <br><br>
        <ul>Ces citations
        <li>doivent se trouver dans un article dédié avec le mot-clé "citations"</li>
        <li>doivent être chacune dans un seul paragraphe</li>
        <li>ne doivent pas dépasser 200 caractères</li>
        <li>doivent être séparées par une ligne vide</li>
        </ul>',
	'pied_citations_titre' => 'Titre à afficher au-dessus',
	'pied_citations_titre_explication' => 'Laisser vide pour ne pas avoir de titre',
	'pied_copyright' => 'Affichage du copyright',
	'pied_hebergeur' => 'Affichage de l’hébergeur',
	'pied_hebergeur_nom' => 'Nom de votre hébergeur',
	'pied_hebergeur_url' => 'URL de votre hébergeur (si vous voulez un lien vers celui-ci)',
	'pied_lien_affichage' => 'Affichage des liens ci-dessus',
	'pied_lien_contact' => 'Lien vers la page <strong>contact</strong>',
	'pied_lien_icone' => 'Sous forme d’icônes',
	'pied_lien_mentions' => 'Lien vers la page des <strong>mentions légales</strong>',
	'pied_lien_mentions_article' => 'Numéro de l’article choisi',
	'pied_lien_mentions_choix' => 'Choix de la page à afficher',
	'pied_lien_mentions_choix1' => 'Vers celle du plugin "SPIP Mentions légales" si vous l’avez activé',
	'pied_lien_mentions_choix2' => 'Vers celles du plugin "Legal EN" si vous l’avez activé : "Mentions légales" et "Données personnelles et cookies" (plugin clairement dédié aux sites de l’Éducation Nationale)',
	'pied_lien_mentions_choix3' => 'Vers un article de votre choix',
	'pied_lien_mentions_choix4' => 'Vers l’article avec le mot-clé "mentions-legales"',
	'pied_lien_plan' => 'Lien vers le <strong>plan du site</strong>',
	'pied_lien_prive' => 'Lien vers l’<strong>espace privé</strong>',
	'pied_lien_rss' => 'Icône avec lien vers la page de <strong>flux RSS</strong>',
	'pied_lien_squelette' => 'Lien vers le <strong>squelette</strong> de la page',
	'pied_lien_texte' => 'Sous forme de textes',
	'pointeur' => 'Pointeurs de souris',
	'pointeur_defaut' => 'Pointeur par défaut',
	'pointeur_explication' => 'Vous pouvez changer ici l’image des pointeurs.
				<ul>Ces images doivent :
                    <li>être au format .cur ou .ani</li>
                    <li>ne pas dépasser 32x32 px</li>
                    <li>se trouver dans le dossier /squelettes/images/pointeurs</li>
                    </ul>',
	'pointeur_general' => 'Pointeur général',
	'pointeur_liens' => 'Pointeur pour les liens',
	'police_luciole' => 'Luciole (adaptée aux personnes malvoyantes)',
	'police_readme' => 'Ces fontes sont distribuées gratuitement sous Licence publique Creative Commons Attribution 4.0 International :
					https://creativecommons.org/licenses/by/4.0/legalcode.fr
					<br><br>
					Luciole © Laurent Bourcellier & Jonathan Perez',
	'police_verdana' => 'Verdana, Arial, Helvetica (par défaut)',
	'popup' => 'Boîte modale',
	'popup2_doc' => 'Le contenu de cette boîte sera le texte du dernier article en date avec le mot-clé "popup" du groupe "affichage".',
	'popup_activer' => 'Activer la boîte modale',
	'popup_doc' => 'Afficher une boîte modale à l’ouverture du site. (À utiliser avec parcimonie)<br>
					<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article448" title="Voir la documentation">Documentation</a>',
	'popup_dureecookie' => 'Durée de validité du cookie (en jours)',
	'popup_largeur' => 'Largeur (en px)',
	'popup_temps' => 'Temps d’affichage (en secondes)',
	'portfolio' => 'Portfolio',
	'poster_message' => 'Poster un nouveau message',
	'premier_visiteur' => 'Vous êtes le premier !',
	'proposer_sujet' => 'Proposer un sujet',
	'publie' => 'Publié le',
	'puce' => 'puce',

	// Q
	'qrcode_explication' => 'Si vous avez activé le plugin <a  class="spip_out" href="https://contrib.spip.net/Qrcode" title="Documentation">plugin "QrCode"</a>, le QrCode de l’URL de vos articles s’affichera en bas de la version imprimable des articles',
	'qrcode_plugin' => 'QrCode',

	// R
	'rainette' => 'Rainette',
	'rainette_code' => 'Code de la ville :',
	'rainette_explication' => 'Si vous avez activé et paramétré (choix du service et clé d’inscription) le <a  class="spip_out" href="https://contrib.spip.net/Rainette-v3-une-evolution-majeure" title="Documentation">plugin "Rainette"</a>, entrez ci-dessous le code de la ville qui vous intéresse ou son nom ou ses coordonnées GPS.<br>
            Pour connaître le code à rentrer, plus d’explications sur <a  class="spip_out" href="https://contrib.spip.net/Rainette-v3-une-evolution-majeure#L-indication-du-lieu" title="Documentation">la documentation du plugin</a>',
	'rainette_service' => 'Choix du service météo',
	'rappel_discussion' => 'Rappel de la discussion',
	'reagir_sujet' => 'Réagir au sujet',
	'recherche' => 'Recherche',
	'recherche_articles_non' => 'Aucun article trouvé pour cette recherche',
	'recherche_multi' => 'Recherche multi-critères',
	'recherche_multi_resultat' => 'Résultat de la recherche :',
	'recherche_non' => 'Aucun résultat pour cette recherche',
	'recherche_resultat' => 'Résultat de la recherche pour ',
	'recherche_rubriques_non' => 'Aucune rubrique trouvée pour cette recherche',
	'rechercher' => 'Rechercher ...',
	'repondre_message' => 'Répondre à ce message',
	'resultat_articles' => 'Les articles',
	'resultat_messages' => 'Les messages des forums',
	'resultat_ordre_articles' => 'Classer les articles par',
	'resultat_ordre_forums' => 'Classer les messages par',
	'resultat_ordre_rubriques' => 'Classer les rubriques par',
	'resultat_ordre_sites' => 'Classer les sites par',
	'resultat_rubriques' => 'Les rubriques',
	'resultat_sites' => 'Les sites référencés',
	'resultats_recherche' => 'Eléments à afficher dans les résultats de la recherche :',
	'retour_forum' => 'Retour au forum',
	'rubrique_articles' => 'Les articles de cette rubrique',
	'rubrique_cachee_descriptif' => 'Rubrique pour les articles spéciaux qui n’apparaîtront ni dans le menu, ni dans le plan du site, ni dans les derniers articles',
	'rubrique_cachee_titre' => 'Rubrique cachée',
	'rubrique_contenu' => 'Cette rubrique contient',
	'rubrique_date' => 'Affichage de la date de publication et de la date de modification des articles',
	'rubrique_la' => 'La rubrique',
	'rubrique_pagin_bas' => 'Afficher la pagination en bas',
	'rubrique_pagin_haut' => 'Afficher la pagination en haut',
	'rubrique_rss' => 'Affichage du logo RSS avec lien vers la page du flux',
	'rubrique_rss2' => 'Affichage du logo RSS avec lien vers la page du flux pour les sous-rubriques',
	'rubrique_site_reference' => 'Site référencé dans cette rubrique',
	'rubrique_sites_references' => 'Les sites référencés dans cette rubrique',
	'rubrique_sous_rubriques' => 'Les sous-rubriques de cette rubrique',
	'rubrique_taille_logo' => 'Taille max du logo des rubriques (en px)',
	'rubrique_trouvee' => 'rubrique trouvée',
	'rubriques_trouvees' => 'rubriques trouvées',

	// S
	'shoutbox_explication' => 'Si vous avez activé le <a class="spip_out" href="https://contrib.spip.net/Shoutbox-pour-SPIP" title="Documentation">plugin "Shoutbox"</a>, vous pouvez paramétrer :',
	'shoutbox_hauteur' => 'la hauteur du bloc des messages (en px)',
	'shoutbox_nombre' => 'le nombre de messages à afficher',
	'shoutbox_plugin' => 'Shoutbox',
	'signalement_explication' => 'Si vous avez activé le <a class="spip_out" href="http://www.mediaspip.net/documentation/les-plugins-utilises-par-mediaspip/signal-alert-on-the-validity-of/article/signalement-alerter-sur-la" title="Documentation">plugin "Signalement"</a>, vous n’avez rien d’autre à faire.
		Le bouton de signalement <img src="" style="vertical-align:middle;" alt="" /> apparaitra automatiquement en haut de chaque article.',
	'signalement_plugin' => 'Signalement',
	'site_realise_avec' => 'Réalisé sous',
	'site_reference' => 'site référencé',
	'sites' => 'Sites référencés',
	'sites_favoris' => 'Sites favoris',
	'sites_references' => 'sites référencés',
	'socialtags_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/Social-tags" title="Documentation">plugin "Social tags"</a>,
            Escal vous propose 3 choix de sélecteurs que vous pouvez inscrire dans la configuration du plugin Social tags :
            <ul>
            <li><strong>#socialtags-outils</strong> pour afficher les icônes dans la barre d’outils d’Escal</li>
            <li><strong>#socialtags-article</strong> pour les afficher à côté de l’icône "Imprimer cet article" dans vos articles</li>
            <li><strong>#contenu</strong> pour les afficher en bas de chaque article</li>
            </ul>
            <br>Mais ce ne sont que 3 possibilités parmi plein d’autres.',
	'socialtags_plugin' => 'Social tags',
	'sous_rubrique' => 'sous_rubrique',
	'sous_rubriques' => 'sous_rubriques',
	'spip400_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/SPIP-400-Gestion-des-erreurs-HTTP" title="Documentation">plugin "SPIP 400"</a>, vous n’avez rien d’autre à faire, la page 404 du plugin prendra automatiquement la place de celle d’Escal.',
	'spip400_plugin' => 'Spip 400',
	'spipdf_explication' => 'Si vous avez activé le <a class="spip_out" href="http://contrib.spip.net/spiPDF-generer-des-contenus-sur-mesure-en-PDF" title="Documentation">plugin "spiPDF"</a>, vous n’avez rien d’autre à faire, l’icône permettant d’enregistrer ou d’imprimer un article au format .pdf s’affichera automatiquement dans les pages "article".   <br><br>
            Pensez à télécharger l’archive de la librairie et à la décompresser dans un dossier /lib/mpdf à la racine de votre spip (Voir la documentation).',
	'spipdf_plugin' => 'SpiPDF',
	'suggerer_reponse' => 'Avant de continuer, avez-vous pensé à consulter les pages suivantes ?
	<br>Elles contiennent peut-être la réponse que vous cherchez.',
	'sujets' => 'sujets',
	'sur_forum' => 'Sur le forum c’est :',

	// T
	'taille_augmenter' => 'Augmenter la taille des caractères',
	'taille_diminuer' => 'Diminuer la taille des caractères',
	'taille_fichier' => 'Taille du fichier :',
	'taille_icones_pied' => 'Taille des icônes (en px)',
	'taille_logo' => 'Taille max du logo (en px)',
	'taille_logo_site' => 'Taille max du logo des sites (en px)',
	'taille_outils_article' => 'Taille des icônes de la barre d’outils (en px)',
	'taille_police' => 'Taille de la police',
	'taille_police_explication' => '
				Pour augmenter ou diminuer la taille des caractères de tout le site (définies en rem)<br>
				Exemples : <br>
				62.5% → 1rem = 10px<br>
				75% → 1rem = 12px<br>
				87.5% → 1rem = 14px<br>
				100% → 1rem = 16px<br>
				',
	'taille_pourcent' => 'Taille (en %)',
	'telechargement' => 'Fichier à télécharger :',
	'telechargements' => 'Fichiers à télécharger :',
	'texte_accueil' => 'Vous êtes sur la page de configuration du squelette Escal.<br><br>
	Cette page va vous permettre de personnaliser votre site,
	du choix des blocs au choix des couleurs, en passant par l’affichage ou non de nombreux éléments.<br><br>
	A vous de jouer !',
	'texte_coupe' => 'On coupe le texte à (nombre de caractères)',
	'texte_sous_image' => 'Titre et descriptif',
	'texte_sous_image_non' => 'A côté du logo',
	'texte_sous_image_oui' => 'Sous le logo',
	'title_contact' => 'Contacter le référent technique',
	'title_escal' => 'Lien vers le site officiel d’Escal',
	'title_espace_redacteurs' => 'L’espace privé du site',
	'title_mentions' => 'Mentions légales du site',
	'titre' => 'Titre',
	'titre_contenu' => 'Titres et contenus',
	'titre_coupe' => 'Nombre de caractères affichés pour le titre des articles',
	'titre_noisette' => 'Titre du bloc',
	'total_visites' => 'Nombre total de visites : ',
	'traductions_article' => 'traductions de cet article :',
	'transparent' => 'Fond transparent',

	// U
	'une' => 'A la une',
	'une_bloc' => 'Le bloc "A la une"',
	'une_mots_cles' => 'Articles avec mots-clés',

	// V
	'version_actuelle' => 'Version actuelle :',
	'version_maj' => 'Version disponible :',
	'version_ok' => 'Vous avez la dernière version disponible',
	'video' => 'Vidéo',
	'videos' => 'Vidéos',
	'videos_explication' => 'Pour afficher vos vidéos ici, vous devez <br/>
	        - activer le <a class="spip_out" href="https://contrib.spip.net/Plugin-Video-s" title="Documentation">plugin "Vidéo(s)"</a> <br/>
	        - associer le mot-clé "video-une" aux articles contenant vos vidéos',
	'visites' => 'Visites',
	'visites_jour' => 'visites par jour',
	'visiteur' => 'visiteur ou visiteuse',
	'visiteurs' => 'visiteurs ou visiteuses',
	'vu' => 'vu',

	// W
	'webmestre' => 'Webmestre'
);
