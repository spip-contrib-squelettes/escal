<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/escal?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_decouvrir' => 'Обнаружить',
	'a_telecharger' => 'Скачать',
	'acces_direct' => 'Прямой доступ',
	'accessibilite' => 'Доступность',
	'accessibilite_explication' => 'Отображение кнопки настройки специальных возможностей над баннером',
	'accessibilite_logo' => 'Выбор цвета логотипа',
	'accessibilite_police' => 'Выбор шрифта сайта',
	'actus' => 'Новости',
	'adresse_non' => 'Автор не сообщил свой e-mail',
	'affichage_auteur_articles' => 'Показывать имена авторов статей',
	'affichage_chapeau' => 'показывать шапку',
	'affichage_choix' => 'Выбор отображения',
	'affichage_date_modif' => 'Показывать дату обновления',
	'affichage_date_pub' => 'Показывать дату публикации',
	'affichage_date_pub_modif' => 'Показывать дату публикации и дату изменения',
	'affichage_date_pub_ou_modif' => 'Отображение даты <br>(изменения, в противном случае публикации)',
	'affichage_debut' => 'Показывать начало текста',
	'affichage_descriptif' => 'Показывать краткое описание ',
	'affichage_form_reponse' => 'Форма для комментария под статьей',
	'affichage_image' => 'Показать первое изображение',
	'affichage_logo_site' => 'Показать логотип сайта, если он есть',
	'affichage_mots_cles' => '<strong>Показывать ключевые слова, связанные со статьями</strong>',
	'affichage_mots_cles_article' => 'Для страницы статьи',
	'affichage_mots_cles_rubrique' => 'Для страницы темы',
	'affichage_mots_cles_une' => 'Для домашней страницы',
	'affichage_nom_auteur' => 'Показывать имя автора',
	'affichage_nombre_comments' => 'Показать количество комментариев',
	'affichage_ordre' => 'Порядок отображения',
	'affichage_ordre_date' => 'Хронологический порядок',
	'affichage_ordre_dateinv' => 'Обратный хронологический порядок',
	'affichage_ordre_datemodif' => 'По дате последнего изменения',
	'affichage_ordre_hasard' => 'Случайно',
	'affichage_ordre_num' => 'По номеру в заголовке',
	'affichage_ordre_pertinence' => 'Релевантность',
	'affichage_rubrique' => 'Показать родительскую рубрику',
	'affichage_soustitre' => 'Показать подзаголовок',
	'affichage_surtitre' => 'Показать надзаголовок',
	'affichage_video' => 'Замените все эти элементы видео, если в статье есть хотя бы один.',
	'affichage_visites_inter' => 'Разделитель посещений и популярности',
	'affichage_visites_popularite' => 'Показать количество посещений и популярность',
	'affiche_articles' => 'С отображением статьи',
	'agenda' => 'Расписание',
	'aide' => 'Ошибка, техническая проблема, предложение... <a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?page=forumSite-rubrique&lang=fr " title="Форум">форум</a> создан для этого',
	'alerte_javascript' => 'Эта страница не может правильно работать без JavaScript. Please reactivate it.',
	'alt_telechargements' => 'Загрузки',
	'annonce' => 'Объявление',
	'annonce_afficher' => 'Показать блок «Объявление»',
	'annonce_defil' => 'Прокручивающаяся реклама',
	'annonce_defil_afficher' => 'Показать блок “Annonces défilantes” («Прокрутка рекламы»)',
	'annonce_defil_hauteur' => 'Высота блока (в пикселях)',
	'annonce_defil_nombre' => 'Количество объявлений для отображения',
	'annonce_defil_nombre_affiche' => 'Показать количество объявлений',
	'annonce_defil_tempo' => 'Задержка между объявлениями (в секундах)',
	'annonce_defil_tempo_explication' => 'Для применения нового тайм-аута требуется очистка кэша.',
	'annonce_explication' => '<ul>Выберите даты показа статьи в объявления:
		<li>дата начала показа = дата последнего изменения</li>
		<li>дата окончания показа = дата предыдущей публикации</li>
		</ul>',
	'annonces' => 'Объявления',
	'annuaire' => 'Каталог',
	'annuaire_auteurs' => 'Каталог авторов',
	'annuaire_invitation' => 'Вы тоже используете ESCAL?<br>Тогда добавьте свой сайт на эту страницу.',
	'arrondis' => 'Скругление',
	'arrondis_blocs_lateraux' => 'Боковые блоки',
	'arrondis_calendrier' => 'Календарь (предстоящие события)',
	'arrondis_centre_sommaire' => 'Центральный блок сводной страницы',
	'arrondis_explication1' => 'Для одинакового закругления четырех углов просто укажите желаемое значение закругления. Пример: 10px<br>Для незакругленных углов введите значение 0<br>',
	'arrondis_explication2' => 'Для углов с разным закруглением указать значения в порядке  :',
	'arrondis_explication3' => 'Чтобы запустить тесты и сгенерировать соответствующий код: <a class="spip_out" href="http://www.cssmatic.com/border-radius">CSSmatic</a>
		Для более продвинутого использования эллиптических фигур: <a class ="spip_out" href="http://www.alsacreations.com/astuce/lire/979-ovale-forme-elliptique-css3-sans-image.html">Alsacréations</a>',
	'arrondis_identification_recherche' => 'Идентификация и поиск',
	'arrondis_menus' => 'Меню',
	'arrondis_onglets' => 'Вкладки',
	'article_acces_direct_descriptif' => 'Эта статья появится в блоке «Прямой доступ» (“Accès direct”), если вы его активируете',
	'article_acces_direct_texte' => 'Здесь вы можете отобразить содержание статьи с ключевым словом «acces-direct».<br><br>
		Заголовок блока будет таким же, как и заголовок статьи.<br><br>
		<a class=" spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24">Дополнительная информация</a>',
	'article_archive' => 'Помни!',
	'article_dernier' => 'Последняя опубликованная статья:',
	'article_edito_descriptif' => 'Эта статья появится в блоке “Edito”, если вы его активируете',
	'article_edito_texte' => 'Здесь вы можете отобразить содержание статьи с ключевым словом "edito".<br>
		Заголовок блока будет таким же, как и у статьи.<br>
		Дополнительная информация в <a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23">этой статье</a>',
	'article_edito_titre' => 'Edito («редакционная статья»)',
	'article_forum' => 'Форум статьи',
	'article_imprimer' => 'Печать',
	'article_libre1' => 'Свободная статья 1',
	'article_libre2' => 'Article libre 2',
	'article_libre3' => 'Article libre 3',
	'article_libre4' => 'Article libre 4',
	'article_libre5' => 'Article libre 5',
	'article_licence' => 'Лицензия',
	'article_logo' => 'Максимальный размер логотипа статьи (в пикселях)',
	'article_mise_en_ligne' => 'Статья опубликована ',
	'article_modifie' => 'последние изменения',
	'article_trouve' => 'найдена статья',
	'article_une' => 'Статья для первой страницы',
	'articlepdf_afficher' => 'Показать значок для рубрик',
	'articlepdf_explication' => 'Если вы активировали плагин <a class="spip_out" href="http://contrib.spip.net/Article-PDF,2226" title="Documentation">"PDF Article"</a>, вам больше ничего не нужно делать. Значок, позволяющий сохранить или распечатать статью в формате .pdf, будет автоматически отображаться на страницах «статьи».<br>Он также может отображаться на страницах «рубрик».',
	'articlepdf_plugin' => 'ArticlePDF',
	'articles_associes' => 'См. связанные статьи ',
	'articles_associes_mot' => 'Статьи/документы, связанные с ключом',
	'articles_auteur' => 'Статьи этого автора:',
	'articles_derniers' => 'Новые статьи',
	'articles_largeur_image' => 'Максимальная ширина изображения (в пикселях)',
	'articles_largeur_images' => 'Максимальная ширина изображений (в пикселях) ',
	'articles_largeur_images_classique' => 'Для классических статей',
	'articles_largeur_images_pleinepage' => 'Для полностраничных статей (статей с ключевым словом «полностраничные» -"pleinepage"), без боковых блоков',
	'articles_logo' => 'Максимальный размер логотипа статьи (в пикселях)',
	'articles_plus_vus' => 'Самые просматриваемые статьи',
	'articles_portfolio' => 'Показать портфолио (дублировать с боковым блоком «Скачать»)',
	'articles_portfolio_descriptif' => 'Отобразить описание документа, если оно существует',
	'articles_portfolio_explication' => 'Дублировать с блоком «Скачать»',
	'articles_premier_message' => 'Показывать только первое сообщение каждой ветки, ответы можно развернуть',
	'articles_reponses_forum' => 'Ответы с форумов статей',
	'articles_rubrique' => 'Статьи в рубрике',
	'articles_site' => 'Посмотрите статьи этого сайта',
	'articles_trouves' => 'найдено статей',
	'aucun' => 'Нет',
	'aujourdhui' => 'Сегодня: ',
	'auteurs' => 'авторы',
	'avec' => 'с',
	'avec_le_squelette' => 'с шаблоном',

	// B
	'bandeau' => 'Баннер',
	'bandeau_choix_explication' => 'Если изображение вашего баннера не занимает всю ширину, вы можете выбрать его положение.<br>
		Однако будьте осторожны с «краевыми эффектами», если вы отображаете название сайта, его слоган или его описание.',
	'bandeau_choix_item1' => '<strong>Вариант 1</strong>: отображение логотипа Escal (по умолчанию)',
	'bandeau_choix_item2' => '<strong>Вариант 2</strong>: отображение логотипа сайта.',
	'bandeau_choix_item3' => '<strong>Вариант 3</strong>: показ персонализированного баннера.',
	'bandeau_choix_item4' => '<strong>Вариант 4</strong>: нет изображения',
	'bandeau_choix_option' => 'Выбор варианта изображения',
	'bandeau_choix_position' => 'Положение изображения баннера',
	'bandeau_option3' => 'Для варианта 3',
	'bandeau_option3_aucune' => 'Нет заменяющего изображения',
	'bandeau_option3_explication1' => 'Ваше изображение должно быть размещено в папке <strong>/squelettes/images/bandeau</strong><br>.
		Это изображение будет «адаптивным».<br>
		Все изображения .jpg, .png .webp и .gif в этом каталоге будут перечислены<br>
		Вы даже увидите свой живой баннер! ',
	'bandeau_option3_explication2' => 'А если ваш хостинг не позволяет вам перечислить имеющиеся файлы,
		вы можете указать здесь путь к вашему файлу изображения.<br>
		Пример: <strong>images/bandeau/bandeau.jpg</strong>, если ваш файл <strong>bandeau.jpg</strong> находится в <strong>squelettes/images/bandeau</strong>',
	'bandeau_option3_explication3' => 'Если в вашей папке <strong>/squelettes/images/bandeau</strong> находится изображение с именем <strong>rubriqueXX.jpg</strong>, где XX — номер рубрики, то эта рубрика, её подрубрики и их статьи автоматически отобразят этот баннер.
		Остальные будут отображать баннер, определенный выше.',
	'bandeau_option3_explication3titre' => 'Баннер по сектору?<br><br>',
	'bandeau_option3_explication4' => 'Если ваша папка /squelettes/images/bandeau содержит изображения с именем
		<ul>
		<li>hiver.jpg или hiver.png - "зима"</li>
		<li>printemps.jpg или printemps.png  - "весна" </li>
		<li>été.jpg или été.png  - "лето"</li>
		<li>automne.jpg или automne.png - "осень"</li>
		</ul>
		эти изображения будут автоматически отображаться во течение соответствующем сезоне.',
	'bandeau_option3_explication4titre' => 'Баннер по сезону?<br><br>',
	'bandeau_texte' => 'Выбор текстов',
	'bandeau_texte_descriptif' => '<strong>Описание</strong><br> сайта',
	'bandeau_texte_nom' => '<strong>Название</strong><br> этого сайта',
	'bandeau_texte_slogan' => '<strong>Слоган</strong><br> этого сайта',
	'bandeau_texte_taille' => 'Размер символа (в пикселях)',
	'barre_outils_dessous' => 'Под баннером',
	'barre_outils_dessus' => 'Над баннером',
	'barre_outils_explication' => 'Эта опция автоматически отключается на экранах шириной менее 640 пикселей.',
	'barre_outils_fixer' => 'Если вы решили разместить панель инструментов над баннером, хотите ли вы закрепить ее вверху страницы?',
	'barre_outils_identification' => 'Отображение простой идентификационной формы',
	'barre_outils_place' => 'Место панели инструментов (простая форма идентификации, языковое меню, форма поиска, кнопка-ссылка на форум сайта)',
	'bas' => 'Вниз',
	'bienvenue' => 'Приветствуем, ',
	'blanc' => 'белый',
	'bloc_choix' => 'Выбор блоков',
	'bloc_cinq' => 'Пятый блок',
	'bloc_deux' => 'Второй блок',
	'bloc_dix' => 'Десятый блок',
	'bloc_huit' => 'Восьмой блок',
	'bloc_neuf' => 'Девятый блок',
	'bloc_parametrage' => 'Параметры блоков',
	'bloc_quatre' => 'Четвертый блок',
	'bloc_sept' => 'Седьмой блок',
	'bloc_six' => 'Шестой блок',
	'bloc_trois' => 'Третий блок',
	'bloc_un' => 'Первый блок',
	'blocs_disponibles' => 'Доступные блоки',
	'bord_pages' => 'Боковые края страниц',
	'bord_pages_couleur' => 'Цвет линии или тени',
	'bord_pages_decalage' => 'Ширина обводки или смещение тени (в пикселях)',
	'bord_pages_force' => 'Сила градиента (в пикселях)',
	'bord_pages_label' => 'Дать преимущество',
	'bord_pages_nb' => 'Укажите 0, чтобы линия была без тени.',
	'bords' => 'Границы',
	'bords1' => 'Пункты горизонтального меню',
	'bords2' => 'Страницы статей и рубрик, форумы статей, страница контактов, каталог',
	'bords3' => 'Таблицы',
	'bords5' => 'Элементы горизонтального меню при наведении, идентификация, поиск, событие календаря',
	'bords6' => 'Форум сайта',
	'bords_arrondis' => 'Границы и скругления',

	// C
	'cadres' => 'Рамки и картинки',
	'cadres_bords' => 'Края блоков присутствуют',
	'cadres_images' => 'Ширина изображения',
	'cadres_images_largeur' => 'Максимальная ширина изображений (в пикселях) для статей, используемых в определенных боковых блоках: Прямой доступ (Accès direct), Edito, Случайные фото, Бесплатные статьи с 1 по 5. ',
	'calendrier' => 'Календарь',
	'centre' => 'В центре',
	'cfg_page_accueil' => 'Главная',
	'cfg_page_arrondis' => 'Углы',
	'cfg_page_article' => 'Статьи',
	'cfg_page_article_lateral' => 'Статьи',
	'cfg_page_article_principal' => 'Статьи',
	'cfg_page_articlepdf' => 'Article PDF',
	'cfg_page_autres_principal' => 'Другие страницы',
	'cfg_page_bandeau' => 'Баннер',
	'cfg_page_blocs_lateraux' => 'Боковые блоки',
	'cfg_page_bords' => 'Границы и скругления',
	'cfg_page_choix_blocs' => 'Выбор боковых блоков',
	'cfg_page_colonne_principale' => 'Основной столбец',
	'cfg_page_contact_principal' => 'Страница контактов',
	'cfg_page_deplier_replier' => 'Развернуть и свернуть',
	'cfg_page_elements' => 'Элементы',
	'cfg_page_facebook' => 'Facebook',
	'cfg_page_fonds' => 'Фоны',
	'cfg_page_forumsite' => 'Форум',
	'cfg_page_forumsite_lateral' => 'Форум',
	'cfg_page_forumsite_principal' => 'Форум',
	'cfg_page_galleria' => 'Галерея',
	'cfg_page_generalites' => 'Общее',
	'cfg_page_layout' => 'Макет',
	'cfg_page_licence' => 'Лицензия',
	'cfg_page_liens_sociaux' => 'Социальные связи',
	'cfg_page_mentions' => 'Юридические упоминания',
	'cfg_page_menuh' => 'Меню',
	'cfg_page_meta' => 'META-теги',
	'cfg_page_multilinguisme' => 'Многоязычие',
	'cfg_page_pages_noisettes' => 'Другие страницы',
	'cfg_page_parametrage_blocs' => 'Параметры боковых блоков',
	'cfg_page_pied' => 'Подвал (footer)',
	'cfg_page_plugins' => 'Плагины в Эскале',
	'cfg_page_popup' => 'Модальное окно',
	'cfg_page_qrcode' => 'QR код',
	'cfg_page_rainette' => 'Rainette (погода)',
	'cfg_page_rubrique' => 'Рубрики',
	'cfg_page_rubrique_lateral' => 'Рубрики',
	'cfg_page_rubrique_principal' => 'Рубрики',
	'cfg_page_shoutbox' => 'Рупор',
	'cfg_page_signalement' => 'Отчеты (Signalement)',
	'cfg_page_socialtags' => 'Социальные теги',
	'cfg_page_sommaire_colonnes' => 'Выбор боковых блоков',
	'cfg_page_sommaire_lateral' => 'Главная страница',
	'cfg_page_sommaire_noisettes' => 'Настройки боковых блоков',
	'cfg_page_sommaire_principal' => 'Главная страница',
	'cfg_page_spip400' => 'Spip 400',
	'cfg_page_spipdf' => 'spiPDF',
	'cfg_page_style' => 'Другие стили',
	'cfg_page_textes' => 'Тексты',
	'chercher_parmi_les_signataires' => 'Поиск среди подписантов',
	'choix_article' => 'Список статей',
	'choix_article_choix' => 'Выбор статей',
	'choix_article_choix_explication' => 'Номера статей через запятую',
	'choix_article_colonne_explication' => 'Введите число от 1 до 3',
	'choix_article_fond' => 'Фон статьи',
	'choix_article_fond_explication' => 'либо цвет (имя, шестнадцатеричный код или код RGB)
		<br>или градиент (пример: линейный градиент(#DAE6F6, #336699))
		<br>или изображение (пример: url("squelettes/images/mon_image.jpg") без повтора).',
	'choix_article_taillelogo' => 'Максимальная ширина логотипа статьи (в пикселях)',
	'choix_article_taillelogo_explication' => '80 по умолчанию',
	'choix_blocs_lateraux' => 'Выбор боковых блоков',
	'choix_deux' => 'Два',
	'choix_groupe1' => 'Первая группа ключевых слов',
	'choix_groupe2' => 'Вторая группа ключевых слов',
	'choix_groupe3' => 'Третья группа ключевых слов',
	'choix_trois' => 'Три',
	'choix_une' => 'Один',
	'citations' => 'Цитаты',
	'clic_suite' => 'Нажмите, чтобы прочитать больше',
	'collegues' => 'Все коллеги',
	'colonne_extra' => 'Колонка «Дополнительно» (по умолчанию справа)',
	'colonne_laterale' => 'Боковая колонка',
	'colonne_nav' => 'Колонка «Навигация» (по умолчанию слева)',
	'commentaires' => 'комментарии',
	'configurer_escal' => 'Настроить ЭСКАЛЬ',
	'contact_activer_champ' => 'Активировать поле',
	'contact_alerte_entete' => 'Введенный вами текст содержит ошибки!',
	'contact_alerte_mail' => 'Ваш адрес email неправильный.',
	'contact_alerte_message' => 'Вы не написали сообщение! По рассеянности?',
	'contact_bienvenue' => 'Для связи со службой технической поддержки <br>заполните, пожалуйста,  все поля формы.',
	'contact_cases' => 'Флажки (“checkbox”)',
	'contact_cases_explication' => '(если вы отметите да, не забудьте заполнить хотя бы одну метку, иначе выдаст ошибку)',
	'contact_champ_mail' => 'Поле адреса электронной почты отправителя',
	'contact_destinataire' => 'Адрес получателя (по умолчанию веб-мастера)<br>
		Чтобы ввести несколько адресов, разделите их запятой <br>
		Пример: tata@yoyo.fr, titi@paris.fr, toto@blague.fr',
	'contact_envoyer' => 'Отправить',
	'contact_expediteur' => 'Текстовое поле для адреса электронной почты',
	'contact_explication' => 'Все поля этой формы допускают многоязычность с помощью тега <multi><br>пример: <multi> [fr]Exemple [en]Example [ru]Пример </multi>
		<br>Чтобы изменить текст другого элемента страницы контактов, используйте языковые файлы.<br>
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/ spip/spip .php?article152">Подробнее</a>',
	'contact_libelle' => 'Метка',
	'contact_libelle1' => 'Box label 1',
	'contact_libelle2' => 'Box label 2',
	'contact_libelle3' => 'Box label 3',
	'contact_libelle4' => 'Box label 4',
	'contact_libelle5' => 'Box label 5',
	'contact_libelle_explication' => 'Если оставить метку пустой, соответствующий элемент не будет отображаться.',
	'contact_libelle_gen' => 'Общая метка (без апострофа и кавычек)',
	'contact_ligne' => 'В сети',
	'contact_liste' => 'В списке',
	'contact_mail' => 'Ваш адрес email:',
	'contact_message' => 'Ваше сообщение:',
	'contact_motif' => 'Тема вашего сообщения:',
	'contact_motif1' => 'Информация',
	'contact_motif2' => 'Регистрация',
	'contact_motif3' => 'Техническая проблема',
	'contact_motif4' => 'Ваше мнение о сайте',
	'contact_motif5' => 'Другое',
	'contact_motif_message' => 'Тема сообщения (переключатели)',
	'contact_nom' => 'Ваша фамилия:',
	'contact_obli' => 'Обязательное поле',
	'contact_prenom' => 'Ваше имя:',
	'contact_presentation' => 'Презентация',
	'contact_retour' => 'Подтверждающее сообщение',
	'contact_retour_commentaire' => 'Ваше сообщение успешно отправлено веб-мастеру сайта, и он ответит вам в ближайшее время по этому адресу:',
	'contact_retour_explication' => 'Вы можете настроить сообщение, которое будет отображаться пользователю после отправки формы. За вашим сообщением будет следовать адрес пользователя.',
	'contact_sup' => 'Добавляем дополнительное поле',
	'contact_sup1' => 'Дополнительное текстовое поле 1',
	'contact_sup2' => 'Дополнительное текстовое поле 2',
	'contact_texte_accueil' => 'Текст, который будет отображаться вверху страницы',
	'contact_texte_accueil_explication' => 'вместо<br>
		«Чтобы связаться с технической поддержкой, пожалуйста, заполните все поля этой формы».<br>
		(Чтобы ничего не отображать, просто введите пробел)',
	'contenu_site' => 'Этот сайт содержит: ',
	'copyright' => 'Все права защищены',

	// D
	'dans_site' => 'на этом сайте',
	'deplier_replier' => 'Развернуть и свернуть',
	'deplier_replier_explication' => 'При выборе «Да» блок будет свернут, и кнопка позволит вам развернуть его и снова свернуть.',
	'derniers_articles_bis' => 'Новые статьи 2',
	'derniers_articles_syndiques' => 'Новые импортированные статьи',
	'derniers_articles_ter' => 'Новые статьи 3',
	'destinataire' => 'Получатель',
	'details' => 'Есть более подробная информация',
	'doc_a_decouvrir' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article180" title="См. документацию">Чтобы узнать</a>',
	'doc_acces_direct' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article24&lang=fr" title="Voir la documentation">Accès direct</a>',
	'doc_actus' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article130" title="См. документацию">Новости (“Actus”)</a>',
	'doc_annonce' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article174" title="См. документацию">Документация</a>',
	'doc_annonce_defil' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article179&lang=fr" title="См. документацию">Документация</a>',
	'doc_annuaire_auteurs' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article201&lang=fr" title="См. документацию">Каталог авторов</a>',
	'doc_articles_plus_vus' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article78&lang=fr" title="Voir la documentation">Самые читаемые статьи (“Articles les plus vus”)</a>',
	'doc_articles_rubrique' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article279" title="См. документацию">Статьи рубрики</a>',
	'doc_choix_blocs' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article174" title="Смотреть документацию">Объявление</a>: новая статья с ключевым словом “annonce” на вкладке.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article179" title="См. документацию">Прокручивающаяся реклама</a>: все статьи с ключевым словом “annonce-defilant” будут прокручиваться на вкладке.
<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article5" title="См. документацию">Главная (“A la une”)</a>: вкладки для новых статей на сайте, карта сайта, отдельная статья, статьи из рубрики...',
	'doc_derniers_art_syndic' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article291" title="См. документацию">Последние синдицированные статьи</a>',
	'doc_derniers_articles' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article26&lang=fr" title="См. документацию">Derniers articles</a>',
	'doc_derniers_articles2' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="См. документацию">Новые статьи</a>',
	'doc_derniers_comments' => '	<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article178" title="См. документацию">Последние комментарии</a>',
	'doc_edito' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article23&lang=fr" title="См. документацию">Редакционная статья (“Edito”)</a>',
	'doc_evenements_a_venir' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article77&lang=fr" title="Voir la documentation">Предстоящие события</a>',
	'doc_identification' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article27&lang=fr" title="Voir la documentation">Identification</a>',
	'doc_meme_rubrique' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article28&lang=fr" title="Voir la documentation">Dans la même rubrique</a>',
	'doc_menuv1' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article102&lang=fr" title="Voir la documentation">Menu vertical dépliant</a>',
	'doc_menuv2' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article112&lang=fr" title="Voir la documentation">Menu vertical déroulant à droite</a>',
	'doc_mini_calendrier' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article81&lang=fr" title="Voir la documentation">Mini calendrier</a>',
	'doc_mot_cles_associes' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article131&lang=fr" title="Voir la documentation">Mots-clés associés</a>',
	'doc_navigation_mots_cles' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article312&lang=fr" title="Voir la documentation">Navigation par mots-clés</a>',
	'doc_noisettes' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article24" title="См. документацию">Прямой доступ</a>: статья с ключевым словом «access-direct» для внутренних ссылок. Заголовок будет соответствовать статье.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article130" title="См. документацию">Actus</a>: статьи с ключевым словом «новости» (“actus”).
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article201" title="См. документацию">Каталог авторов</a>: список авторов и их статус.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article279" title="См. документацию">Статьи рубрики</a>: отображает статьи рубрики с ключевым словом «articles-de-rubrique».
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article78" title="См. документацию">Самые читаемые статьи</a>: показывает самые посещаемые статьи.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article200" title="См. документацию">Бесплатные статьи с 1 по 5</a>: статью с ключевым словом «article-libreN» (N = от 1 до 5) для того, что вы хотите. Заголовок будет соответствовать статье.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article180" title="См. документацию">Чтобы узнать</a>: выбор статьи сайта или текущей рубрики и её подрубрик.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article191" title="См. документацию">Для загрузки</a>: список документов рубрики (не статей!) или статьи в зависимости от контекста.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article190" title="См. документацию">Блок для настройки</a>: статьи с ключевым словом “special”.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article28" title="См. документацию">В этой рубрике</a>: новые  статьи из той же рубрики, что и текущая статья.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article26" title="См. документацию">Новые статьи</a>: Новые статьи этого сайта или рубрики и её подрубрик.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article291" title="См. документацию">Новые синдицированные статьи</a>: самые последние статьи с синдицированных сайтов с указанием сайта-источника.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article178" title="См. документацию">Новые комментарии</a>: последние комментарии статьи со всего сайта.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article23" title="См. документацию">Редактировать</a>: статья со словом- клавиша «редактировать». Заголовок будет соответствовать статье.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article77" title="См. документацию">Предстоящие события</a>: список предстоящих событий.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article81" title="См. документацию">Мини-календарь</a>: использует плагин «Мини-календарь», который автоматически активируется с помощью Escal.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article131" title="См. документацию">Связанные ключевые слова</a>: навигация по ключевым словам, если к статье прикреплено хотя бы одно.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article312" title="См. документацию">Просмотр по ключевым словам</a>: меню навигации через группы ключевых слова.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article192" title="См. документацию">Погода</a>: требуется плагин “Rainette” - информация о погоде в выбранном вами городе.<br>Настройка этого блока осуществляется в разделе «Плагины в Escal».
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article198" title="См. документацию">Многокритериальный поиск</a>: поиск по группам ключевых слов и по ключевым словам.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article181" title="См. документацию">Любимые сайты</a>:
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article126" title="См. документацию">Статистика</a>: вся статистика сайта.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article82" title="См. документацию">В сети</a>: имя и последние статьи с синдицированных сайтов, перечисленных по исходному сайту.
		<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article197" title="См. документацию">Видео</a>: отображает видео FLV или mp4 из статей с ключевым словом «video-une».',
	'doc_noisettes2' => 'Некоторые блоки доступны только на определенных страницах.',
	'doc_personnalise' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article190" title="См. документацию">Блок для настройки</a>',
	'doc_photos_hasard' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article144&lang=fr" title="Voir la documentation">Случайные фото</a>',
	'doc_rainette' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article192&lang=fr" title="Voir la documentation">Погода</a>',
	'doc_recherche_multi' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article198&lang=fr" title="Voir la documentation">Многокритериальный поиск</a>',
	'doc_site_escal' => '<a class="doc-escal" href="http://escal.ac-lyon.fr" title="Aller sur le site d\'ESCAL">Документация: шаблон ESCAL.</a>',
	'doc_sites_favoris' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article181&lang=fr" title="A télécharger">Любимые сайты</a>',
	'doc_stats' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article126&lang=fr" title="См. документацию">Статистика</a>',
	'doc_sur_web' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article82&lang=fr" title="См. документацию">В сети</a>',
	'doc_telecharger_art_rub' => '<a class="spip_out" href="http://escal.ac-lyon.fr/spip/spip.php?article191&lang=fr" title="См. документацию">Скачать</a>',
	'doc_videos' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article197" title="См. документацию">Видео</a>',
	'document_trouve' => 'документ найден',
	'documentation' => 'Документация',
	'documentation_voir' => 'См. документацию',
	'documents' => 'Документы',
	'documents_trouves' => 'документы найдены',
	'droite' => 'Справа',

	// E
	'edito' => 'Редакционная статья (“Édito”)',
	'elements' => 'Элементы',
	'envoi_mail_message' => 'Сообщение: ',
	'envoi_mail_motif' => 'Тема: ',
	'envoi_mail_nom' => 'Фамилия: ',
	'envoi_mail_prenom' => 'Имя: ',
	'envoyer_message' => 'Отправить сообщение для ',
	'erreur401' => 'Ошибка 401',
	'erreur401_message' => 'У вас недостаточно прав для доступа к запрошенной странице или документу... <br>
		Для доступа к нему обратитесь к веб-мастеру сайта,
		используя страницу [Контакт|Page contact->@url@] этого сайта.',
	'erreur404' => 'Ошибка 404',
	'erreur404_message' => 'Страница, которую вы ищете, либо перемещена, либо отсутствует.
		Если случившееся кажется вам ошибкой, напишите об этом веб-мастеру с помощью  [страницы обратной связи |Contact page ->@url@] этого сайта.',
	'escal' => 'Escal',
	'evenement_associe' => 'Связанное событие:',
	'evenements_ajouter' => 'Добавить мероприятие<br>(Доступ ограничен)',
	'evenements_associes' => 'Сопутствующие события:',
	'evenements_non' => 'Не ожидается событий',
	'evenements_venir' => 'Планируемые мероприятия',

	// F
	'facebook_bouton' => 'Внешний вид кнопки',
	'facebook_explication' => 'Если вы активировали плагин <a class="spip_out" href="http://contrib.spip.net/Modeles-Facebook" title="Documentation">"Модели Facebook"</a>, надпись "Мне нравится" появится в нижнем колонтитуле.
		<br>Не забудьте настроить этот плагин, указав URL-адрес вашей страницы или профиля Facebook.',
	'facebook_partager' => 'Кнопка «Поделиться»',
	'facebook_plugin' => 'Шаблоны Facebook',
	'favicon' => 'Favicon',
	'favicon_choix1' => 'из файла favicon.ico.<br>По умолчанию, Escal’s или ваш, поместить в папку /squelettes(в корне). ',
	'favicon_choix2' => 'из логотипа вашего сайта.<br>Escal его автоматически преобразует (размер, формат).',
	'favicon_choix3' => 'из изображения по вашему выбору (gif, png, jpg...), помещенного его в папку /squelettes.',
	'favicon_choix_fichier' => 'Ниже необходимо указать имя и расширение файла.',
	'fleche' => 'стрелка',
	'fois' => 'раз',
	'fonds_couleurs' => '<strong>Цвета</strong>: вы можете указать
		<ul>
		<li>цвет в шестнадцатеричном коде → пример: #0000FF </li>
		<li>именованный цвет → пример: grey </li>
		<li>a цвет в коде RGB → пример: rgb(24,125,255) </li>
		<li>цвет в коде RGB с прозрачностью → пример: rgba(24,125,255,0.5) </li>
		<li>или любой другой цветовой код: HSL, HSB , CMYK ... </li>
		<li>или «прозрачный» для удаления цвета</li>
		</ul>',
	'fonds_noisettes' => 'Фоны и тексты в блоках',
	'fonds_noisettes_annonce' => 'Объявления в одном<br>Выделение искомых слов',
	'fonds_noisettes_apercu' => 'Предварительный просмотр текста',
	'fonds_noisettes_articles' => 'Статьи<br>Форум статей<br>Повестка дня<br>',
	'fonds_noisettes_bandeau' => 'Баннер',
	'fonds_noisettes_boutons' => 'Кнопки идентификации и поиска<br>
		Форма обратной связи<br>
		Четные строки таблиц<br>
		Портфолио<br>
		Постскриптум<br>
		Форум сайта,...',
	'fonds_noisettes_contenu' => 'Содержимое страницы',
	'fonds_noisettes_contenu_transparent' => 'Вы также можете написать «прозрачный», что будет удобно с фоновым изображением.',
	'fonds_noisettes_derniers_articles' => 'Список последних статей на Главной',
	'fonds_noisettes_fond' => 'Фон:',
	'fonds_noisettes_lien' => 'Цвет ссылки:',
	'fonds_noisettes_lien_survol' => 'Цвет ссылки при наведении: ',
	'fonds_noisettes_menuh' => 'Горизонтальное меню<br>
		Ссылки при наведении в вертикальном выпадающем меню справа<br>
		Вставка заголовка для рубрик и статей<br>
		План<br>
		Шапки форума',
	'fonds_noisettes_onglets_actifs' => 'Активная вкладка главной страницы',
	'fonds_noisettes_onglets_inactifs' => 'Избранные вкладки, если они неактивны',
	'fonds_noisettes_pied' => 'Футер',
	'fonds_noisettes_rubriques' => 'Активный пункт горизонтального меню<br>
		Нечетные строки таблиц<br>
		Превью форумов,...',
	'fonds_noisettes_survol' => 'Пункты меню при наведении, избранные элементы блоков, хлебные крошки...',
	'fonds_noisettes_texte' => 'Цвет текста:',
	'fonds_noisettes_textes' => 'Текст боковых блоков',
	'fonds_noisettes_titres' => 'Заголовок и края боковых блоков<br>Вертикальное всплывающее меню<br>Заголовок таблиц,...',
	'fonds_site' => 'Фон для сайта',
	'fonds_site_aucune_image' => 'Нет фонового изображения',
	'fonds_site_couleur' => 'Цвет фона сайта',
	'fonds_site_couleur_defaut' => '(по умолчанию: #DFDFDF)',
	'fonds_site_explication1' => 'Ваши фоны должны быть помещены в <strong>/squelettes/images/fonds</strong> <br>
		Все изображения .jpg, .png .webp и .gif в этом каталоге будут перечислены. <br>
		Вы даже сможете предварительно просмотреть свой живой фон!',
	'fonds_site_explication2' => 'А если ваш хостинг не позволяет вам перечислить имеющиеся файлы,
		вы можете указать здесь путь к вашему файлу изображения.<br>
		Пример: <strong>images/fonds/fond.jpg</strong>, если ваш файл <strong>fond.jpg</strong> находится в <strong>squelettes/images/fonds</strong>',
	'fonds_site_explication3' => 'По умолчанию фоновое изображение повторяется по горизонтали и/или вертикали,
		что хорошо, например, для градиентного фона.
		Но если у вас есть изображение, которое не должно повторяться, отметьте «да» ниже,
		и ваше изображение адаптируется к экрану пользователя и останется зафиксированным. <br><br>
		– Рекомендуемый размер для большинства экранов: 2000 на 1300 пикселей.<br>
		– Изображения должно быть оптимизировано, чтобы не замедлять загрузку.<br>
		См. <a class="spip_out" href="http://www.alsacreations.com/astuce/lire/1216-arriere-plan-extensible-background.html" title="Подробнее">Расширяемый фон</a>',
	'fonds_site_image' => 'Фоновое изображение',
	'fonds_site_imagefondunique' => 'Подогнать фон под экран',
	'fonds_textes' => 'Другие тексты',
	'fonds_textes_alerte' => 'Оповещения на странице контактов',
	'fonds_textes_comment1' => '(в случае пустого поля, например)',
	'fonds_textes_comment2' => 'идентификация, вход, информация, регистрация...',
	'fonds_textes_couleur' => 'Области ввода SPIP',
	'fonds_textes_explication' => 'Вам все равно придется выбирать цвета для некоторых конкретных текстов...',
	'fonds_textes_input' => 'Области ввода SPIP',
	'fonds_textes_liens' => 'Ссылки',
	'fonds_textes_liens_survol' => 'Ссылки при наведении',
	'fonds_textes_liens_vus' => 'Посещенные ссылки',
	'fonds_textes_texte' => 'Предварительный просмотр',
	'form_pet_envoi_mail_confirmation' => 'Письмо-подтверждение (e-mail) было отправлено веб-мастеру сайта для подтверждения вашей регистрации. ',
	'form_recherche' => 'Форма поиска',
	'form_recherche_item1' => 'На панели инструментов',
	'form_recherche_item2' => 'Левый столбец',
	'form_recherche_item3' => 'Правый столбец',
	'form_recherche_item4' => 'Нигде',
	'form_recherche_item5' => 'В горизонтальной строке меню',
	'form_recherche_place' => 'Место формы',
	'format' => 'Тип файла:',
	'forum' => 'Форум',
	'forum_site_avant' => 'Прежде, чем начать',
	'forum_site_derniers' => 'Последние<br>сообщения',
	'forum_site_reponses' => 'Ответы',
	'forum_site_sujets' => 'темы',
	'forum_trouve' => 'найдено сообщение форумa',
	'forums' => 'Сообщения форумов',
	'forums_trouves' => 'найдены сообщения форумa',

	// G
	'galleria_caracteres' => 'Размер шрифта заголовка изображения',
	'galleria_couleur_encart' => 'Цвет фона информационного окна',
	'galleria_couleur_gallerie' => 'Цвет фона галереи',
	'galleria_couleur_texte' => 'Цвет текста информационного поля',
	'galleria_explication' => 'Если вы активировали <a class="spip_out" href="http://contrib.spip.net/Galleria-fr" title="Documentation">плагин "Galleria"</a>, вы можете настроить его здесь.',
	'galleria_plugin' => 'Галерея',
	'gauche' => 'Слева',
	'gros_jour' => 'Самый важный день:',
	'groupe_affichage' => 'Группа технических ключевых слов, используемых в плагине Escal.',
	'groupe_agenda_couleur' => 'Группа ключевых слов для цвета событий календаря в плагине Escal',

	// H
	'haut' => 'Вверх',
	'haut_page' => 'В начало страницы  ⇑',
	'hebergeur' => 'Хост:',

	// I
	'identification' => 'Идентификация',
	'indentation' => 'Отступ первой строки абзацев (в пикселях)',
	'info_lien_message' => '<strong>Внимание! </strong>Если ваше сообщение содержит ссылку, она должна быть проверена веб-мастером. Не надо пытатья это еще раз публиковать ;-)',
	'infos' => 'Информация',

	// L
	'layout' => 'Макет',
	'layout_choix' => 'Выбор планировки',
	'layout_documentation' => '<a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article1" title="См. документацию"><strong>Документация</strong></a>',
	'layout_explication' => 'Будьте осторожны с выбором ширины: обратите внимание, что текст со слишком длинными строками становится трудным для чтения.',
	'layout_fixe' => '<strong>Фиксированный макет</strong><br>по умолчанию: макет 1200 пикселей и столбцы 250 пикселей.',
	'layout_fixe_largeur' => 'Ширина макета (в пикселях)',
	'layout_fixe_si' => '<strong>фиксированный макет</strong>',
	'layout_fluide' => '<strong>Гибкий макет</strong><br>адаптируется к ширине экрана (рекомендуется)',
	'layout_fluide_largeur' => 'Максимальная ширина макета (в пикселях)',
	'layout_fluide_si' => '<strong>текучий макет</strong>',
	'layout_largeur' => 'Регулировка ширины',
	'layout_largeur_colonnes' => 'Ширина фиксированных боковых колонок (в пикселях)',
	'layout_mixte' => '<strong>Смешанная компоновка</strong><br> гибкая центральная часть и фиксированные боковые колонки',
	'layout_mixte_si' => '<strong>смешанный макет</strong>',
	'le' => ' ',
	'licence_explication' => 'Если вы активировали плагин <a class="spip_out" href="http://contrib.spip.net/Une-licence-pour-un-article" title="Documentation">"License"</a>, вам больше нечего делать, вверху статьи автоматически появится значок, соответствующий лицензии, которую вы выбрали для статьи.',
	'lien_agenda' => '► посмотреть полную страницу',
	'lien_agenda_title' => 'Отобразить календарь на всю страницу',
	'lien_rapide_pied' => 'Перейти в футер',
	'liens_sociaux_espace' => 'Показывать социальные ссылки на панели инструментов',
	'liens_sociaux_explication' => 'Если вы активировали <a class="spip_out" href="https://contrib.spip.net/Liens-sociaux" title="Documentation">плагин «Социальные ссылки»</a> ("Liens sociaux"), вы можете настроить его здесь.',
	'liens_sociaux_pied' => 'Показывать социальные ссылки в футере',
	'liens_sociaux_plugin' => 'Социальные связи',
	'liens_sociaux_texte' => 'Возможный текст для отображения:',
	'lire_article' => 'Читать статью...',
	'lire_suite' => 'Подробнее...',
	'lire_tout' => 'Прочитать всю статью',

	// M
	'mentions' => 'Юридические уведомления',
	'mentions_donnees' => 'Персональные данные и файлы cookie',
	'mentions_explication' => 'Если вы активировали <a class="spip_out" href="http://contrib.spip.net/Mentions-Legales" title="Documentation">плагин «Юридические уведомления»</a>, вы можете отобразить ссылку на специальную страницу в нижнем колонтитуле сайта.',
	'mentions_texte' => '<!-- Текст, который необходимо изменить, заполнив data_to_replace:
		###name_of_the_director_of_publication###
		###postal_address### (2 экземпляра)
		###name_of_host###
		###link_to_the_website_of_the_host###
		###email_of_the_host# # #
		-->

		Отображение юридических уведомлений является обязательством как для профессиональных, так и для личных веб-сайтов в соответствии с законом от 21 июня 2004 г. о доверии в цифровой экономике (LCEN). Согласно этому закону здесь указаны контактные данные издателя и поставщика услуг, размещающего сайт @url_du_site@

		<hr>

		{{{Владелец/Издатель/Директор издания}}}

		@url_du_site@ — это веб-сайт, редактируемый {{@nom_du_site@}} и публикуемый под руководством ###nom_du_directeur_de_publication###. Весь сайт @url_du_site@, его страницы и контент являются собственностью {{@nom_du_site@}}.
		Административный контакт для сайта @url_du_site@:
		###postal_address###
		[->@courriel_de_contact@]

		{{{Host}}}

		@url_du_site@ размещается на хосте ###name_of_the_host###.
		Для получения дополнительной информации посетите сайт [->###lien_vers_le_site_de_l_hébergeur###]
		Чтобы связаться с ними, используйте адрес электронной почты [->###courriel_de_l_hébergeur###]

		{{@nom_du_site@}} отказывается от какой-либо ответственности за любые перебои в работе сайта @url_du_site@ и его сервисов.

		{{{Дизайнер/Создатель}}}

		Весь сайт {{@nom_du_site@}}: дизайн, графический устав и приложения был создан и разработан {{@nom_du_site@}} с помощью [SPIP->https://www.spip.net] под скином [Escal-> https://escal.edu.ac-lyon.fr].
		Чтобы связаться с ними, используйте адрес электронной почты: [->@courriel_de_contact@]

		{{{Functionnement du site}}}

		Страницы и контент этого сайта созданы с помощью бесплатного программного обеспечения SPIP, распространяемого по лицензии GNU/GPL (Общая лицензия). Общественный). Вы можете свободно использовать его для создания собственного веб-сайта. Для получения дополнительной информации посетите сайт [spip.net->https://www.spip.net].

		SPIP, Система публикаций в Интернете.
		Авторские права © 2001–2018, Арно Мартен, Антуан Питру, Филипп Ривьер и Эммануэль Сен-Джеймс.

		{{{Содержание и права на воспроизведение}}}

		В соответствии со статьями L. 111-1 и L. 123-1 Кодекса интеллектуальной собственности, весь контент на этом сайте (тексты, изображения, видео и любые медиафайлы в целом), если иное прямо не указано иное, если не указано иное, защищено авторским правом. Воспроизведение, даже частичное, содержимого страниц данного сайта без предварительного согласия @url_du_site@ строго запрещено.

		{{{Признание факта}}}

		Используя @url_du_site@, пользователь Интернета принимает к сведению и принимает перечисленные здесь условия

		{{{Злоупотребление}}}

		В соответствии с Законом № 2004-575 от 21 июня 2004 г. о доверии в цифровой экономике, чтобы сообщить о спорном содержании или если вы стали жертвой мошеннического использования сайта @url_du_site@, свяжитесь с администратором сайта по адресу: адрес электронной почты: [->@courriel_de_contact@]

		{{{Сбор и обработка персональных данных}}}

		Любая информация, собранная @url_du_site@, никогда не передается третьим лицам. За исключением особых случаев, эта информация поступает в результате добровольной регистрации адреса электронной почты, предоставленного пользователем Интернета, что позволяет ему получать документацию или информационный бюллетень, запрашивать регистрацию в редакции сайта или спрашивать о каком-либо пункте.
		В соответствии с законом № 78-17 от 6 января 1978 г. об обработке данных, файлах и свободах пользователь Интернета имеет право на доступ, исправление и удаление личной информации, касающейся его, хранящейся на @url_du_site@, которую он можете заниматься в любое время у администратора @url_du_site@. Для запроса этого типа отправьте письмо по адресу
		###address_postale###
		или свяжитесь по адресу электронной почты: [->@courriel_de_contact@]

		{{{Cookies}}}

		Определенная личная информация и определенные маркеры cookie могут быть записаны @url_du_site@ на персональном компьютере пользователя без выражения его пожеланий («cookies» или «JAVA-апплеты»).
		Если явно не указано иное, эти технические процессы не являются необходимыми для правильного функционирования @url_du_site@, и их использование, конечно, может быть деактивировано в браузере, используемом пользователем Интернета, без ущерба для его просмотра.
		Никакая эта информация не сохраняется на сайте после отключения пользователя от Интернета.
		Здесь полезно указать, что для доступа к различным частным областям сайта может потребоваться согласие пользователя Интернета на хранение файла cookie на его персональном компьютере в целях безопасности.

		{{{Ограничение ответственности}}}

		Сайт @url_du_site@ содержит информацию, предоставленную сторонними компаниями, или гипертекстовые ссылки на другие сайты или другие внешние источники, которые не были разработаны {{@nom_du_site@}}.
		Поведение целевых сайтов, возможно, вредоносное, не может быть привлечено к ответственности {{@nom_du_site@}}.
		В более общем смысле, контент, представленный на этом сайте, предназначен для информационных целей.
		Пользователь Интернета должен использовать эту информацию с пониманием.
		{{@nom_du_site@}} не несет ответственности за информацию, мнения и рекомендации третьих лиц.
		Поскольку @url_du_site@ не может контролировать эти сайты или внешние источники, {{@nom_du_site@}} не может нести ответственность за предоставление этих сайтов или внешних источников и не может нести никакой ответственности за контент, рекламу, продукты, услуги или другие материалы. доступны на таких сайтах или из внешних источников или с них.
		Кроме того, {{@nom_du_site@}} не может нести ответственность за любой доказанный или предполагаемый ущерб или убытки, возникшие в результате или в связи с использованием или фактом доверия к контенту, товарам или услугам, доступным на этих внешних сайтах или источниках.',
	'menu' => 'В меню',
	'menu_horizontal' => 'Горизонтальное меню',
	'menu_horizontal_accueil' => 'Для ссылки на Главную страницу',
	'menu_horizontal_affichage' => 'Показать меню горизонтально',
	'menu_horizontal_choix' => 'Выбор горизонтального меню',
	'menu_horizontal_choix_bloc' => 'Мега раскрывающееся меню',
	'menu_horizontal_choix_liste' => 'Раскрывающиеся списки',
	'menu_horizontal_fixer' => 'Исправить меню вверху страницы при прокрутке вниз',
	'menu_horizontal_hauteur' => 'Высота логотипов рубрик (в пикселях)',
	'menu_horizontal_hauteur_explication' => 'Убедитесь, что высота ваших логотипов как минимум равна выбранному значению.',
	'menu_horizontal_logo' => 'Отображение логотипа рубрики, если он существует',
	'menu_horizontal_logo_accueil' => 'Отображение логотипа',
	'menu_horizontal_notice' => '<strong>Внимание</strong>: не устанавливайте для обоих значений «нет», чтобы не получить пустые записи меню.',
	'menu_horizontal_secteur' => 'Для заголовков первого уровня (= сектор)',
	'menu_horizontal_secteur_explication' => '→ также применимо к меню для мобильных',
	'menu_horizontal_titre' => 'Показ названия рубрики (если в ней опубликована хотя бы одна статья)',
	'menu_horizontal_titre_accueil' => 'Отображение слова «Главная»',
	'menu_vertical_depliant' => 'Раскрывающееся вертикальное меню',
	'menu_vertical_deroulant' => 'Вертикальное выпадающее меню справа',
	'metadonnees' => 'Метаданные',
	'metadonnees_explication' => '<ul>Если вы активировали плагин <a class="spip_out" href="https://contrib.spip.net/Metadonnees-Photo" title="Documentation">Метаданные</a>, вы можете выбрать отображение
		<li>метаданных ваших фотографий</li>
		<li>или их названий</li>
		<li>или и то, и другое</li>
		</ul>',
	'metadonnees_titre' => 'Название и метаданные',
	'mini_calendrier' => 'Мини-календарь',
	'mot_acces_direct' => 'выбрать статью, которая будет отображаться в блоке «Прямой доступ» (“Accès direct”)',
	'mot_accueil' => 'выбрать статью для вкладки Главной страницы',
	'mot_actus' => 'выбрать статьи, которые будут отображаться в блоке «Новости» (“Actus”)',
	'mot_agenda' => 'выбрать статьи или рубрику(и), статьи которых будут отображаться в повестке дня',
	'mot_annonce' => 'выбрать статью, текст которой будет отображаться в блоке «Анонс» (“Annonce”) Главной страницы',
	'mot_annonce_defilant' => 'выбрать статьи, текст которых будет отображаться в блоке «Прокрутка» (“Annonces défilantes”) Главной страницы',
	'mot_annuaire' => 'чтобы выбрать статью, которая будет использоваться на странице annuaire.html',
	'mot_archive' => 'выбрать рубрику, из которой будет отображаться случайная статья на вкладке «Архивные статьи» главной страницы',
	'mot_article_libre1' => 'выбрать статью, содержание которой будет отображаться в блоке "Article libre 1"',
	'mot_article_libre2' => 'выбрать статью, содержание которой будет отображаться в блоке "Article libre 2"',
	'mot_article_libre3' => 'выбрать статью, содержание которой будет отображаться в блоке "Article libre 3"',
	'mot_article_libre4' => 'выбрать статью, содержание которой будет отображаться в блоке "Article libre 4"',
	'mot_article_libre5' => 'выбрать статью, содержание которой будет отображаться в блоке "Article libre 5"',
	'mot_article_sans_date' => 'скрыть отображение дат публикации и изменения статьи',
	'mot_articles_rubrique' => 'выбрать рубрику, статьи которой будут отображаться в блоке «Статьи рубрики»',
	'mot_chrono' => 'отображать статьи рубрики в меню в обратном хронологическом порядке, поведение не передается дочерним рубрикам',
	'mot_citations' => 'pour choisir l’article qui servira de réservoir pour les citations dans le pied de page',
	'mot_edito' => 'pour choisir l’article qui sera affiché dans le bloc "Edito" ',
	'mot_favori' => 'pour choisir les sites dont les vignettes seront affichées dans le bloc "Sites favoris" ',
	'mot_forum' => 'pour choisir le secteur qui sera utilisé pour le forum du site',
	'mot_invisible' => 'скрыть рубрику и её подрубрики из всех меню, карты сайта и последних статей',
	'mot_mentions' => 'выбрать статью, которая будет отображаться в качестве юридических уведомлений',
	'mot_mon_article' => 'выбрать статью, которая будет отображаться во вкладке центрального блока Главной страницы',
	'mot_mon_article2' => 'выбрать вторую статью, которая будет отображаться во вкладке центрального блока Главной страницы',
	'mot_mon_article3' => 'выбрать третью статью, которая будет отображаться во вкладке центрального блока Главной страницы',
	'mot_pas_decouvrir' => 'выбрать рубрики и статьи, которые следует исключить из отображения в блоке «Обнаружить» ("A decouvrir"), если вы выбрали «по всему сайту» ',
	'mot_pas_menu' => 'не отображать рубрику или статью в горизонтальном меню',
	'mot_pas_menu_vertical' => 'не отображать рубрику или статью в вертикальных меню',
	'mot_pas_plan' => 'не отображать рубрику (и её статьи) или статью в блоке «Карта сайта» Главной страницы',
	'mot_pas_une' => 'pour ne pas afficher une rubrique (et ses articles) ou des articles dans les onglets "Derniers articles" de la page d’accueil',
	'mot_photo_une' => 'pour choisir les articles dont les images seront affichées dans le bloc "Quelques images au hasard" ',
	'mot_pleine_page' => 'выбрать статьи, которые будут отображаться на всю страницу без бокового блока',
	'mot_popup' => 'выбор статьи, которая будет отображаться в модальном окне при открытии сайта (если активировано)',
	'mot_rubrique_onglet' => 'выбрать рубрику, которая будет отображаться во вкладке на Главной странице',
	'mot_rubrique_onglet2' => 'выбрать вторую рубрику, которая будет отображаться во вкладке на Главной странице',
	'mot_rubrique_onglet3' => 'выбрать третью рубрику, которая будет отображаться во вкладке на главной странице',
	'mot_rubrique_onglet4' => 'выбрать четвертую рубрику, которая будет отображаться во вкладке на главной странице',
	'mot_rubrique_onglet5' => 'выбрать пятую рубрику, которая будет отображаться во вкладке на главной странице',
	'mot_site_exclu' => 'исключить сайты из блока «В сети»',
	'mot_special' => 'выбрать рубрику и/или статьи, которые будут отображаться в персонализируемом блоке',
	'mot_texte2colonnes' => 'Pour afficher un texte d’article en 2 colonnes dans une page article',
	'mot_video_une' => 'выбрать статьи, видео которых будут отображаться в блоке «Видео»',
	'mots_clefs' => 'Ключи',
	'mots_clefs_associes' => 'Связанные ключи',
	'mots_cles' => 'ключи',
	'moyenne_jours' => 'дни:',
	'moyenne_visites' => 'В среднем',
	'multilinguisme_ariane' => 'Также удалить сектор в "Хлебных крошках"',
	'multilinguisme_code' => 'Коды языков',
	'multilinguisme_drapeau' => 'Флаги',
	'multilinguisme_explication1' => 'По умолчанию Escal отображает в отдельном меню заголовки 1-го уровня (= сектора).<br>
Если мы хотим иметь одинаковую структуру на нескольких языках, придется дублировать все сектора на всех языках. Получится большое количество секторов.<br>
Но можно выделить по одному сектору для каждого языка и в меню отображать только языковой сектор.<br>
Выбор этой опции позволяет отображать в меню заголовки второго уровня вместо секторов. Опция действительна для горизонтального меню, а также для двух вертикальных меню.',
	'multilinguisme_explication2' => 'По умолчанию отображаются коды каждого используемого языка, но их можно заменить полным названием языка или маленькими флажками <br> Но будьте осторожны: на английском языке говорят не только в Великобритании, а на испанском говорят не только в Испании, ... поэтому желательно использовать коды языков',
	'multilinguisme_niveau' => 'Меню с рубриками второго уровня',
	'multilinguisme_nom' => 'Названия языков',

	// N
	'nav_mot_cle' => '	Просмотр по ключевым словам',
	'noir' => 'черный',
	'noisette_article' => 'Блоки, специфичные для страницы статьи',
	'noisette_commun' => 'Блоки, общие для нескольких страниц',
	'noisette_perso' => 'Блок личных настроек',
	'noisette_rubrique' => 'Блоки, специфичные для страницы рубрики',
	'noisettes_acces_direct_explication' => 'Название этого блока задано названием статьи с ключевым словом «прямой доступ» ("acces-direct"). Её переводы должны быть доступны на всех языках многоязычного сайта.',
	'noisettes_actus_enlever' => 'Скрыть блок, если нет новостей',
	'noisettes_actus_tempo' => 'Задержка между каждым действием (в секундах)',
	'noisettes_annuaire_admin' => 'Цвет для администраторов',
	'noisettes_annuaire_icones' => 'Показывать значки',
	'noisettes_annuaire_lien' => 'Отображение ссылки на тромбиноскоп (trombinoscope)',
	'noisettes_annuaire_pagination' => 'Количество авторов на странице пагинации',
	'noisettes_annuaire_redacteur' => 'Цвет для редакторов',
	'noisettes_annuaire_visiteur' => 'Цвет для посетителей',
	'noisettes_calendrier_ajouter' => 'Ссылка «Добавить событие», если человек прошел аутентификацию',
	'noisettes_calendrier_couleur' => 'Цвет события по умолчанию',
	'noisettes_calendrier_events' => 'Список предстоящих событий ниже',
	'noisettes_calendrier_explication2' => 'Этот выбор также будет распространяться на блок «Предстоящие события».',
	'noisettes_calendrier_lien_agenda' => 'Название со ссылкой на полностраничную повестку дня',
	'noisettes_calendrier_pagination' => 'Количество статей или событий на странице пагинации',
	'noisettes_calendrier_renvoi' => 'PДля мероприятий, использующих плагин Agenda, ссылка относится к',
	'noisettes_calendrier_renvoi_aucun' => 'нет ссылки',
	'noisettes_calendrier_renvoi_evenement' => 'событие',
	'noisettes_decouvrir_hasard' => 'Количество случайных статей',
	'noisettes_decouvrir_moins_vus' => 'Количество наименее посещаемых статей',
	'noisettes_decouvrir_ou' => 'Откуда брать статьи?',
	'noisettes_decouvrir_plus_vus' => 'Количество наиболее посещаемых статей',
	'noisettes_decouvrir_recents' => 'Количество последних статей',
	'noisettes_decouvrir_rubrique' => 'Только в текущей рубрике и её подрубриках',
	'noisettes_decouvrir_site' => 'По всему сайту (вы можете исключить статьи или рубрики с ключевым словом «не для обнаружения» - “pas-a-decouvrir”)',
	'noisettes_edito_explication' => 'Заголовок этого блока задается названием статьи с ключевым словом «edito». На многоязычном сайте у неё должны быть переводы на все языки.',
	'noisettes_explication' => '<strong>Заголовки блоков</strong><br><br>
               В случае <strong>многоязычного сайта</strong>, если вы хотите изменить заголовок блока сохраняя многоязычие, вам следует записать этот заголовок по следующему примеру:<br><br>
               &lt;multi&gt; [fr]Exemple [en]Example [ru]Пример [es]Ejemplo [it]Esempio &lt;/multi&gt;<br><br>
               Если оставить поле пустым, Escal будет использовать заголовок по умолчанию.',
	'noisettes_forum_colonne' => 'Place de la colonne latérale',
	'noisettes_forum_explication' => 'Только одна боковая колонка, потому что форум занимает все остальное пространство.',
	'noisettes_identification_explication' => 'Заголовок этого блока не может быть изменен.',
	'noisettes_identification_redacteur' => 'Отображение формы регистрации для новых <strong>редакторов</strong><br>
		(у вас должен быть установлен флажок «Принимать регистрацию» в админ-зоне: Настройки — Интерактивность).',
	'noisettes_identification_redacteur_label' => 'Редакторы',
	'noisettes_identification_visiteur' => 'Отображение формы регистрации для новых <strong>посетителей</strong><br>
		(у вас должен быть установлен флажок «Включить регистрацию посетителей на сайте» в админ-зоне: Настройки – Регистрация, рассылки)',
	'noisettes_identification_visiteur_label' => 'посетителей',
	'noisettes_multi_choix' => '<strong>Выбор групп ключевых слов</strong>',
	'noisettes_multi_groupe1' => 'Первая группа (обязательно)',
	'noisettes_multi_groupe2' => 'Вторая группа (по желанию)',
	'noisettes_multi_groupe3' => 'Третья группа (по желанию)',
	'noisettes_multi_groupe4' => 'Четвертая группа (по желанию)',
	'noisettes_multi_groupe5' => 'Пятая группа (по желанию)',
	'noisettes_multi_groupe6' => 'Шестая группа (по желанию)',
	'noisettes_multi_groupe7' => 'Седьмая группа (по желанию)',
	'noisettes_multi_groupe8' => 'Восьмая группа (по желанию)',
	'noisettes_multi_groupe9' => 'Девятая группа (по желанию)',
	'noisettes_multi_non' => 'Не используется',
	'noisettes_nav_groupe' => 'Группа ключевых слов, которые нужно использовать',
	'noisettes_nav_nombre' => 'Максимальное количество статей для отображения по каждому ключевому слову',
	'noisettes_perso_choix2' => 'Для последних двух вариантов отображения:',
	'noisettes_perso_choix3' => 'Для первых трех вариантов отображения:',
	'noisettes_perso_choix4' => 'Для четвертого варианта отображения:',
	'noisettes_perso_liste1' => 'Список с названием (и ссылкой), датой публикации и автором.',
	'noisettes_perso_liste2' => 'Список только с заголовком (и ссылкой)',
	'noisettes_perso_liste3' => 'Список с заголовком (и ссылкой) и началом текста',
	'noisettes_perso_liste4' => 'Прокручиваемый список с заголовком (и ссылкой) и началом текста',
	'noisettes_perso_tempo' => 'Задержка между статьями (в секундах)',
	'noisettes_photos_lien' => 'Ссылка на статью, источник фото',
	'noisettes_photos_tempo' => 'Задержка между изображениями (в секундах)',
	'noisettes_sites_exclus' => 'Исключить сайты с ключевым словом «избранное» ("favori")',
	'noisettes_sites_exclus_explication' => 'так как уже присутствует в блоке «Избранные сайты» ("Sites favoris"), если вы им пользуетесь',
	'noisettes_sites_tempo' => 'Задержка между логотипами сайтов (в секундах)',
	'noisettes_stats_affiche' => 'Отображать ...',
	'noisettes_stats_articles' => '...количество статей на сайте',
	'noisettes_stats_auteurs' => '...количество авторов',
	'noisettes_stats_choix' => 'Выбор статистики для представления:',
	'noisettes_stats_comments' => '...количество комментариев',
	'noisettes_stats_et' => 'и...',
	'noisettes_stats_jour' => 'Количество посетителей в день',
	'noisettes_stats_libelle' => '...ярлык «Этот сайт важен»',
	'noisettes_stats_ligne' => 'Количество онлайн-посетителей',
	'noisettes_stats_max' => 'Самый большой день',
	'noisettes_stats_mots' => '...количество ключевых слов',
	'noisettes_stats_moyenne' => 'Среднее число посещений',
	'noisettes_stats_pages' => 'Общее количество посещенных страниц',
	'noisettes_stats_periode' => 'в течение ... (в днях)',
	'noisettes_stats_rubriques' => '...количество рубрик',
	'noisettes_stats_sites' => '...количество ссылочных сайтов',
	'noisettes_stats_sujets' => 'Количество тем форума',
	'noisettes_stats_visites' => 'Общее количество посещений',
	'noisettes_sur_web_syndic' => 'Только синдицированные сайты',
	'nombre_actus' => 'Количество новостей',
	'nombre_articles' => 'Количество статей',
	'nombre_articles_pagination' => 'Количество статей на странице пагинации',
	'nombre_articles_sites' => 'Количество статей на сайте',
	'nombre_caracteres' => 'Количество символов, отображаемых в тексте статьи/статей',
	'nombre_colonnes' => 'Число столбцов',
	'nombre_colonnes2' => 'Количество столбцов (для статей и подзаголовков)',
	'nombre_comments' => 'Количество комментариев',
	'nombre_comments_pagination' => 'Количество комментариев на странице пагинации',
	'nombre_images' => 'Количество изображений',
	'nombre_mots' => 'Количество ключевых слов для отображения',
	'nombre_sites' => 'Количество сайтов',
	'nombre_sujets_pagination' => 'Количество тем на странице пагинации',
	'nuage' => 'Облако ключевых слов',

	// O
	'onglets_art_accueil' => 'Главная',
	'onglets_art_accueil_explication' => 'Отображается статья с ключевым словом «accueil», в противном случае она будет опубликована последней.<br> Название статьи отображается на вкладке.',
	'onglets_art_accueil_forum' => 'Показать главную статью форума',
	'onglets_art_archive' => 'Архивная статья',
	'onglets_art_archive_explication' => 'Отображаемая статья будет случайной статьей в рубрике с ключевым словом "архив" ("archive") или в одной из её подрубрик.<br> Заголовок вкладки - "Помни!" с поддержкой многоязычия на языках, доступных в Escal.',
	'onglets_art_archive_forum' => 'Посмотреть архив статей форума',
	'onglets_arts_mot' => 'Статьи с ключевым словом',
	'onglets_arts_mot_choix' => 'Выбор ключевого слова',
	'onglets_arts_mot_explication1' => 'Заголовок этой вкладки будет соответствовать ключевому слову',
	'onglets_arts_mot_explication2' => '<strong> <a href="#articles_keywords">→ Настроить</a></strong>',
	'onglets_bis_pagin_defaut' => '0 | 10 | 20 | 30',
	'onglets_bis_pagin_page' => '1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | ...',
	'onglets_bis_pagin_pps' => '< 1 | 2 | 3 | 4 | 5 | 6 | >',
	'onglets_bis_pagin_prive' => '0 | 10 | 20 | 30 | Показать все',
	'onglets_bis_pagin_ps' => 'предыдущая страница | следующая Страница',
	'onglets_bis_pagination' => 'Шаблон нумерации страниц (попробуйте!)',
	'onglets_choix' => 'Выбор вкладок',
	'onglets_cinq' => 'Пятая вкладка',
	'onglets_derniers_article' => 'Настройки каждой статьи',
	'onglets_derniers_arts_explication1' => '<ul>Их можно отобразить тремя способами:
		<li><strong>Новые статьи <a href="#last_articles">→ Настроить</a><br></strong></li>
		<li><strong>Новые статьи 2 <a href="#last_articles_bis">→ Настроить</a><br></strong></li>
		<li><strong>Новые статьи 3 <a href="#last_articles_ter">→ Настроить </a></strong></li>
		</ul>',
	'onglets_derniers_arts_explication2' => 'Заголовком этой вкладки будет «Новые статьи», если вы не дадите ей другое название ниже.',
	'onglets_derniers_arts_explication3' => 'Для "Derniers articles" («Последние статьи») и "Derniers articles ter" («Последние статьи из...») из списка будут исключены статьи с ключевым словом "annonce" или "annonce-defilant" во избежание дублирования, если только не установлены блоки "Annonce" («Объявления») и "Annonces défilantes" («Прокрутка рекламы»). ',
	'onglets_derniers_arts_explication4' => 'Статьи с ключевым словом "annonce" («Объявление») или "annonce-defilant" («Прокручивающаяся реклама») будут исключены из списка во избежание дублирования, если не используются блоки "Annonce" и "Annonces défilantes". ',
	'onglets_derniers_autres' => 'Другие статьи',
	'onglets_derniers_exergue' => 'Избранная статья',
	'onglets_derniers_presentation' => 'Общая презентация',
	'onglets_deux' => 'Вторая вкладка',
	'onglets_monart' => 'Моя статья',
	'onglets_monart2' => 'Моя статья 2',
	'onglets_monart2_titre' => 'Заголовок вкладки<br>"Моя статья 2"',
	'onglets_monart3' => 'Моя статья 3',
	'onglets_monart3_titre' => 'Заголовок вкладки <br>"Моя статья 3"',
	'onglets_monart4' => 'Моя статья 4',
	'onglets_monart4_titre' => 'Заголовок вкладки<br>"Моя статья 4"',
	'onglets_monart5' => 'Моя статья 5',
	'onglets_monart5_titre' => 'Заголовок вкладки<br>"Моя статья 5""',
	'onglets_monart_explication' => '<strong>Моя статья</strong> или <strong>Моя статья 2</strong> или <strong>Моя статья 3</strong> или <strong>Моя статья 4</strong> или <strong>Моя статья 5</strong> – это статьи с ключевыми словами "mon-article", "mon-article2", "mon-article3", "mon-article4" и "mon-article5".<br><br>Заголовком вкладки будет название статьи, если вы не дадите ей другое название ниже. Отображаются только текст и PS.',
	'onglets_monart_titre' => 'Заголовок вкладки <br>"Моя статья"',
	'onglets_nombre_articles' => 'Общее количество статей для отображения',
	'onglets_nombre_articles_exergue' => 'Количество статей для отображения, включая избранную статью',
	'onglets_parametrage' => 'Настройки вкладки',
	'onglets_placer' => 'Размещайте вкладки вверху страницы при нажатии на одну из них.',
	'onglets_quatre' => 'Четвертая вкладка',
	'onglets_rien' => 'Не показывать эту вкладку',
	'onglets_rub2' => 'Рубрика 2',
	'onglets_rub3' => 'Рубрика 3',
	'onglets_rub4' => 'Рубрика 4',
	'onglets_rub5' => 'Рубрика 5',
	'onglets_rub_auteur' => 'показать автора статьи',
	'onglets_rub_descriptif' => 'показать описание рубрики',
	'onglets_rub_explication' => '<strong>Рубрика</strong> или <strong>Рубрика 2</strong> или <strong>Рубрика 3</strong> или <strong>Рубрика 4</strong> или <strong>Рубрика 5</strong><br>используются для отображения статей рубрики, помеченной ключевым словом "RubriqueOnglet" или "RubriqueOngletN" (N = от 2 до 5).<br><br>На вкладке отображается название рубрики, а внизу — статьи этой рубрики и ее подрубрик.',
	'onglets_rub_logo' => 'выберите размер логотипа статьи (в пикселях)',
	'onglets_rub_pagin' => 'определить шаг нумерации страниц для статей',
	'onglets_rub_texte' => 'отображать текст рубрики',
	'onglets_sur_web_explication' => 'На этой вкладке будет отображен список синдицированных статей на вашем сайте с указанием исходного сайта и даты его публикации.',
	'onglets_sur_web_explication2' => 'Название вкладки — «В сети», если вы не дадите ей другое название ниже.',
	'onglets_sur_web_pagin' => 'Для этих статей нет нумерации страниц.',
	'onglets_ter_mot_exergue' => 'Показ избранной статьи',
	'onglets_trois' => 'Третья вкладка',
	'onglets_un' => 'Первая вкладка',

	// P
	'page_accueil' => 'Главная (La page d’accueil)',
	'page_article' => 'Страница статьи',
	'page_auteur' => 'Авторы страниц',
	'page_autres' => 'Другие страницы',
	'page_autres_explication' => 'Страница контактов, каталог, автор, поиск, 404,...',
	'page_contact' => 'Страница обратной связи',
	'page_contact2' => 'Страница обратной связи с',
	'page_contact3' => 'Страница контактов',
	'page_forum' => 'Форум сайта',
	'page_recherche' => 'Страница поиска',
	'page_rubrique' => 'Страница рубрики',
	'pages' => 'страниц',
	'pages_vues' => 'Количество посещенных страниц: ',
	'par_defaut' => 'по умолчанию:',
	'photos_hasard' => 'Несколько случайных<br>фотографий',
	'photos_hasard2' => 'Случайные фото',
	'pied' => 'Футер (нижний колонтитул)',
	'pied_autres_mentions' => 'Другие упоминания',
	'pied_citations_activer' => 'Включить цитаты',
	'pied_citations_explication' => 'Вы можете отображать цитаты слева в футере. <br><br>
		<ul>Эти цитаты
		<li>должны быть в отдельной статье с ключевым словом «citations»</li>
		<li>должны быть каждая в одном абзаце</li>
		<li>не должны превышать 200 символов</li>
		<li>должны быть разделены пустой строкой.</li>
		</ul>',
	'pied_citations_titre' => 'Название, которое будет отображаться выше',
	'pied_citations_titre_explication' => 'Оставьте пустым - не будет названия',
	'pied_copyright' => 'Отображение авторских прав',
	'pied_hebergeur' => 'Отображение хоста',
	'pied_hebergeur_nom' => 'Имя вашего хостера',
	'pied_hebergeur_url' => 'URL вашего хостера (если вам нужна ссылка на него)',
	'pied_lien_affichage' => 'Показать ссылки выше',
	'pied_lien_contact' => 'Ссылка на страницу <strong>контактов</strong>',
	'pied_lien_icone' => 'Как значки',
	'pied_lien_mentions' => 'Ссылка на страницу <strong>юридических уведомлений</strong>.',
	'pied_lien_mentions_article' => 'Номер выбранного элемента',
	'pied_lien_mentions_choix' => 'Выбор страницы для отображения',
	'pied_lien_mentions_choix1' => 'К плагину «Правовые уведомления SPIP» ("SPIP Mentions légales"), если вы его активировали.',
	'pied_lien_mentions_choix2' => 'К плагину «Legal EN», если вы его активировали «Юридические уведомления» и «Личные данные и файлы cookie» (плагин явно предназначен для национальных образовательных сайтов).',
	'pied_lien_mentions_choix3' => 'К статье по вашему выбору',
	'pied_lien_mentions_choix4' => 'К статье с тегом "legal-notice"',
	'pied_lien_plan' => 'Ссылка на <strong>карту сайта</strong>',
	'pied_lien_prive' => 'Ссылка на <strong>админ-зону</strong>',
	'pied_lien_rss' => 'Значок со ссылкой на страницу <strong>RSS-канал</strong>.',
	'pied_lien_squelette' => 'Ссылка на <strong>шаблон</strong> страницы.',
	'pied_lien_texte' => 'В виде текстов',
	'pointeur' => 'Указатели мыши',
	'pointeur_defaut' => 'Указатель по умолчанию',
	'pointeur_explication' => 'Здесь вы можете изменить изображение указателей.
		<ul>Эти изображения должны:
		<li>быть в формате .cur или .ani</li>
		<li>не превышать 32x32 пикселей</li>
		<li>находиться в папке /squelettes/images/pointeurs</li>
		</ul>',
	'pointeur_general' => 'Общий указатель',
	'pointeur_liens' => 'Указатель для ссылок',
	'police_luciole' => 'Luciole (подходит для слабовидящих)',
	'police_readme' => 'Эти шрифты распространяются бесплатно в соответствии с международной публичной лицензией Creative Commons Attribution 4.0: https://creativecommons.org/licenses/by/4.0/legalcode.fr<br><br>
		Luciole © Laurent Bourcellier & Jonathan Perez',
	'police_verdana' => 'Verdana, Arial, Helvetica (по умолчанию)',
	'popup' => 'Модальное окно',
	'popup2_doc' => 'Содержимым этого поля будет текст последней статьи с ключевым словом "popup" в группе "affichage".',
	'popup_activer' => 'Включить модальное окно',
	'popup_doc' => 'Отображение модального окна при открытии сайта. (Используйте экономно)<br>
  <a class="spip_out" href="http://escal.edu.ac-lyon.fr/spip/spip.php?article448" title="См. документацию">Документация</a>',
	'popup_dureecookie' => 'Срок действия cookie (в днях)',
	'popup_largeur' => 'Ширина (в px)',
	'popup_temps' => 'Время отображения (в секундах)',
	'portfolio' => 'Портфолио',
	'poster_message' => 'Разместить новое сообщение',
	'premier_visiteur' => 'Вы - первый!',
	'proposer_sujet' => 'Предложить тему',
	'publie' => 'Опубликовано',
	'puce' => 'пункт списка',

	// Q
	'qrcode_explication' => 'Если вы активируете плагин <strong><a class="spip_out" href="https://contrib.spip.net/Qrcode" title="Documentation">"QrCode"</a></strong>, QR-код, соответствующий URL-адресу вашей статьи, автоматически появится внизу <strong>версии для печати</strong> этой статьи.',
	'qrcode_plugin' => 'QR-код',

	// R
	'rainette' => 'Rainette',
	'rainette_code' => 'Код города:',
	'rainette_explication' => 'Если вы активировали и настроили (выбор службы и регистрационный ключ) <a class="spip_out" href="https://contrib.spip.net/Rainette-v3-une-evolution-majeure" title=" Документация" >плагин «Погода» (“Rainette”)</a>, введите ниже код интересующего вас города или его название или GPS-координаты.<br>
             Как узнать код для входа? Объяснения в <a class="spip_out" href="https://contrib.spip.net/Rainette-v3-une-evolution-majeure#L-indiction-du-lieu" title= "Документация">документации плагина</a>',
	'rainette_service' => 'Выбор метеослужбы',
	'rappel_discussion' => 'Напоминание про обсуждение',
	'reagir_sujet' => 'Реагировать на тему',
	'recherche' => 'Поиск',
	'recherche_articles_non' => 'Не найдены статьи по этому запросу',
	'recherche_multi' => 'Поиск по нескольким<br>критериям',
	'recherche_multi_resultat' => 'Результаты поиска:',
	'recherche_non' => 'Нет результатов по этому запросу',
	'recherche_resultat' => 'Результат поиска для ',
	'recherche_rubriques_non' => 'Не найдены рубрики по этому запросу',
	'rechercher' => 'Поиск...',
	'repondre_message' => 'Ответить на это сообщение',
	'resultat_articles' => 'Статьи',
	'resultat_messages' => 'Сообщения форумов',
	'resultat_ordre_articles' => 'Сортировать статьи по',
	'resultat_ordre_forums' => 'Сортировать сообщения по',
	'resultat_ordre_rubriques' => 'Сортировать рубрики по',
	'resultat_ordre_sites' => 'Сортировать сайты по',
	'resultat_rubriques' => 'Рубрики',
	'resultat_sites' => 'Сайты, на которые ссылаются',
	'resultats_recherche' => 'Элементы для отображения в результатах поиска:',
	'retour_forum' => 'Вернуться на форум',
	'rubrique_articles' => 'Статьи этой рубрики',
	'rubrique_cachee_descriptif' => 'Раздел специальных статей, которые не будут отображаться ни в меню, ни в карте сайта, ни в последних статьях.',
	'rubrique_cachee_titre' => 'Скрытая рубрика',
	'rubrique_contenu' => 'Эта рубрика содержит',
	'rubrique_date' => 'Отображение даты публикации и даты изменения статей.',
	'rubrique_la' => 'Рубрика',
	'rubrique_pagin_bas' => 'Показывать нумерацию страниц внизу',
	'rubrique_pagin_haut' => 'Показывать нумерацию страниц вверху',
	'rubrique_rss' => 'Отображение логотипа RSS со ссылкой на страницу канала.',
	'rubrique_rss2' => 'Отображение логотипа RSS со ссылкой на страницу канала для подрубрик',
	'rubrique_site_reference' => 'Сайт, указанный в этой рубрике',
	'rubrique_sites_references' => 'Реферируемые сайты в этой рубрике',
	'rubrique_sous_rubriques' => 'Подрубрики в этой рубрике',
	'rubrique_taille_logo' => 'Максимальный размер логотипа рубрик (в пикселях)',
	'rubrique_trouvee' => 'найдена рубрика',
	'rubriques_trouvees' => 'рубрик(и) найдено',

	// S
	'shoutbox_explication' => 'Если вы активировали<a class="spip_out" href="https://contrib.spip.net/Shoutbox-pour-SPIP" title="Documentation">плагин "Shoutbox"</a>,
			вы можете настроить:',
	'shoutbox_hauteur' => 'высота блока сообщения (в пикселях)',
	'shoutbox_nombre' => 'количество сообщений для отображения',
	'shoutbox_plugin' => 'Рупор (плагин голосовой почты)',
	'signalement_explication' => 'Если вы активировали <a class="spip_out" href="http://www.mediaspip.net/documentation/les-plugins-utiles-par-mediaspip/signal-alert-on-the-validity-of/article /signalement-alert-on-the" title="Documentation">Плагин "Signalement"</a>, вам больше нечего делать.
Кнопка отчета <img src="" style="vertical-align:middle;" alt="" /> автоматически появится вверху каждой статьи.',
	'signalement_plugin' => 'Составление отчетов (Signalement)',
	'site_realise_avec' => 'Сайт создан на',
	'site_reference' => 'реферируемый сайт',
	'sites' => 'Сайты, на которые ссылаются',
	'sites_favoris' => 'Избранные сайты',
	'sites_references' => 'реферируемые сайты',
	'socialtags_explication' => 'Если вы активировали <a class="spip_out" href="http://contrib.spip.net/Social-tags" title="Documentation">плагин «Социальные теги»</a>,
		Escal предлагает вам 3 варианта выбора. селекторов, которые вы можете прописать в конфигурации плагина Social tags:
		<ul>
		<li><strong>#socialtags-tools</strong> для отображения значков на панели инструментов Escal</li>
		<li><strong>#socialtags-article</strong> для отображения рядом с надписью «Распечатать эту статью» " в ваших статьях.</li>
		<li><strong>#content</strong>, чтобы отображать их внизу каждой статьи.</li>
		</ul>
		<br>Но это только 3 возможности среди множества других.',
	'socialtags_plugin' => 'Социальные теги',
	'sous_rubrique' => 'подрубрика',
	'sous_rubriques' => 'подрубрик(и)',
	'spip400_explication' => 'Если вы активировали плагин <a class="spip_out" href="http://contrib.spip.net/SPIP-400-Gestion-des-erreurs-HTTP" title="Documentation">"SPIP 400"</a> ,
вам больше ничего делать не нужно, страница 404 плагина автоматически заменит страницу Escal.',
	'spip400_plugin' => 'Spip 400',
	'spipdf_explication' => 'Если вы активировали <a class="spip_out" href="http://contrib.spip.net/spiPDF-generer-des-contenus-sur-mesure-en-PDF" title="Documentation">плагин "spiPDF "</a>, больше ничего делать не нужно, на страницах статьи автоматически появится значок, позволяющий сохранить или распечатать статью в формате .pdf. <br>
Не забудьте скачать архив библиотеки и распаковать его в папку /lib/mpdf в корне вашего spip (см. документацию).',
	'spipdf_plugin' => 'SpiPDF',
	'suggerer_reponse' => 'Прежде чем продолжить, просмотрите следующие страницы.
		<br>Они могут содержать ответ, который вы ищете.',
	'sujets' => 'темы',
	'sur_forum' => 'На форуме находится:',

	// T
	'taille_augmenter' => 'Увеличить размер символов',
	'taille_diminuer' => 'Уменьшить размер символов',
	'taille_fichier' => 'Размер файла',
	'taille_icones_pied' => 'Размер значка (в пикселях)',
	'taille_logo' => 'Максимальный размер логотипа (в пикселях)',
	'taille_logo_site' => 'Максимальный размер логотипа сайта (в пикселях)',
	'taille_outils_article' => 'Размер значков на панели инструментов (в пикселях)',
	'taille_police' => 'Размер шрифта:',
	'taille_police_explication' => 'Размер шрифта для всего сайта (определяется в rem)<br>
			Примеры: <br>
			62,5% → 1rem = 10px<br>
			75% → 1rem = 12px<br>
			87,5% → 1rem = 14px<br>
			100% → 1rem=16px<br>',
	'taille_pourcent' => 'Размер (в %)',
	'telechargement' => 'Файл для скачивания:',
	'telechargements' => 'Файлы для скачивания:',
	'texte_accueil' => 'Вы находитесь на странице конфигурации шаблона Escal.<br>
		Эта страница позволит вам настроить ваш сайт, от выбора блоков до выбора цветов, отображая или скрывая множество элементов. <br>
		Решать вам!',
	'texte_coupe' => 'Обрезать текст на (количество символов)',
	'texte_sous_image' => 'Название и описание',
	'texte_sous_image_non' => 'Рядом с логотипом',
	'texte_sous_image_oui' => 'Под логотипом',
	'title_contact' => 'Связаться со службой технической поддержки',
	'title_escal' => 'Ссылка на официальный сайт Escal',
	'title_espace_redacteurs' => 'Личное пространство для авторов и администраторов',
	'title_mentions' => 'Юридические уведомления сайта',
	'titre' => 'Заголовок',
	'titre_contenu' => 'Название и содержание',
	'titre_coupe' => 'Количество символов, отображаемых в заголовках статей',
	'titre_noisette' => 'Заголовок блока',
	'total_visites' => 'Всего посещений:',
	'traductions_article' => 'Перевод этой статьи:',
	'transparent' => 'Прозрачный фон',

	// U
	'une' => 'Избранное (“A la une”)',
	'une_bloc' => 'Блок «Избранное» ("A la une")',
	'une_mots_cles' => 'Статьи с ключевыми словами',

	// V
	'version_actuelle' => 'Текущая версия:',
	'version_maj' => 'Доступная версия:',
	'version_ok' => 'У вас установлена последняя доступная версия',
	'video' => 'Видео',
	'videos' => 'Видео',
	'videos_explication' => 'Чтобы отобразить здесь свои видео, вы должны <br>
		- активировать <a class="spip_out" href="https://contrib.spip.net/Plugin-Video-s" title="Documentation">"Video" плагин</a> <br>
		- свяжите ключевое слово "video-une" со статьями, содержащими ваши видео',
	'visites' => 'Посещения',
	'visites_jour' => 'посещений в день',
	'visiteur' => 'посетитель',
	'visiteurs' => 'посетителей',
	'vu' => 'см.',

	// W
	'webmestre' => 'Веб-мастер'
);
