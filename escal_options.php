<?php

include_spip('inc/config');


// Liste des pages de configuration dans l'ordre de presentation
// Librement inspir� du squelette Sarkaspip
if (!defined('_ESCAL_PAGES_CONFIG')) define('_ESCAL_PAGES_CONFIG',
'accueil
|generalites!layout:popup:elements:bandeau:menuh:multilinguisme:pied
|colonne_principale!sommaire_principal:autres_principal
|blocs_lateraux!choix_blocs:parametrage_blocs
|style!fonds:bords
|plugins!plugins
');


// surlignage des recherches
if (isset($_REQUEST['recherche'])) {
  $_GET['var_recherche'] = $_REQUEST['recherche'];
}
define('_SURLIGNE_RECHERCHE_REFERERS',true);

// Et pour �viter de faire planter GD2 :
if (!defined('_IMG_GD_MAX_PIXELS')) {
 define('_IMG_GD_MAX_PIXELS', 2000000);
}


// proteger le #FORMULAIRE_CONTACT
$GLOBALS['formulaires_no_spam'][] = 'contact';

// multilinguisme

$forcer_lang = true;


